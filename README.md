# SCAR - Smart Contract Abstract Representation
This repository contains the code for the SCAR framework for model-driven development of correct and secure smart contract applications.

## The SCAR Language
The SCAR language is a domain-specific language for specifying smart contracts. Its syntax is defined in the SCApp.g4 ANTLR grammar.
An application consists of a set of contracts. A contract, in turn, consists of a set of typed state variables and a set of functions.
Functions can have _function contracts_, which specify preconditions, postconditions, and effects of the function. The SCAR language also supports _invariants_, which are properties that must hold at all times during the execution of the contract.
Furthermore, SCAR allows the definition of access control policies, and the specification of liveness properties.

## Overview
The `model` package contains the Scala model of the SCAR language.

The `translation` package provides utilities for translating a textfile in the `.scar` format to a Scala model.

The `generate` package contains utilities for generating source code and annotations for language-specific formal verification tools from a SCAR model.

The `analysis` package contains utilities for analyzing whether a given SCAR model conforms to its access control policies and its liveness specification.

## Intended Workflow
1. Write a SCAR model in the `.scar` format.
2. Use the `translation` package to translate the `.scar` file to a Scala model.
3. Use the `analysis` package to check whether the SCAR model conforms to its access control policies and liveness specification.
4. Use the `generate` package to generate source code and annotations for a language-specific formal verification tool (e.g., Solidity and the `solc-verify` tool).
5. Implement the generated functions and verify that your implementation conforms to the generated specification. This shows that the implementation conforms to the model.

## Further reading
A Practical Notion of Liveness in Smart Contract Applications 

[Static Capability-based Security for Smart Contract Applications 
](https://ieeexplore.ieee.org/abstract/document/10237040)