ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion := "3.3.3" // LTS

lazy val root = (project in file("."))
  .settings(
    name := "Scar",
    Compile / unmanagedSourceDirectories += baseDirectory.value / "gen",
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.19",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.19" % "test",
    libraryDependencies += "com.github.scopt" %% "scopt" % "4.1.0",
    libraryDependencies += "com.lihaoyi" %% "upickle" % "4.1.0",
    assembly / mainClass := Some("edu.kit.kastel.formal.scar.main.Main")
  )
enablePlugins(JavaAppPackaging)
// generate fat jar using `assembly` sbt task
