## Preparation

### Build Solc-Verify Docker Image
```shell
git clone -b 0.7 --single-branch https://github.com/SRI-CSL/solidity.git scar-solc-verify  # use 0.7 branch since it was maintained the longest
docker build --platform linux/amd64 -t scar-solc-verify -f scar-solc-verify/docker/Dockerfile-solcverify-local.src scar-solc-verify
```
You can delete the `scar-solc-verify` directory after.

### Generate a fat JAR with all dependencies included
```shell
sbt assembly
```

## Command Line Interface
```
Usage: java -jar scar.jar [align|analyze|integrate] <args>...

Command: align <scar> <skeletonTarget>
Generate a Solidity skeleton from a SCAR specification
  <scar>
  <skeletonTarget>

Command: analyze <scar> <implementation> <errorTarget> [<docker>]
Find the errors in a Solidity implementation against a SCAR specification
  <scar>
  <implementation>
  <errorTarget>
  [<docker>] Path to the docker executable, not necessary if docker is in PATH

Command: integrate <scar> <errorSource> <scarTarget>
Resolve given errors in a Solidity implementation against a SCAR specificationand integrate them into the specification to match the implementation
  <scar>
  <errorSource>
  <scarTarget>
```

## Architecture

### Overview
- File handling is done in `Main`, the `verification` package concerns itself only with lines of text.
- Verification is shorthand for analysis & integration.
- The verification logic is contained in the `verification` package.
- The verification process is stateless.
- All the errors were parsed and modeled, but only modify and condition errors are (currently) relevant.

### Components
- Processor contains the high-level verification logic and calls the other components as needed.
- SolcVerifyWrapper runs Solc-Verify via Docker.
- SpecErrorParser constructs SpecErrors from the Solc-Verify output.
- SpecErrorIntegrator integrates errors into the specification using the visitor pattern.
- AnnotationsUpdater updates the specification annotations in the implementation.
- SpecErrors contains the specification error model.


### Component Diagram
<img src="Components.png" alt="Component Diagram" width="1042"/>

### Visitor Diagram
<img src="Visitor.png" alt="Class Diagram for Visitor" width="769"/>
