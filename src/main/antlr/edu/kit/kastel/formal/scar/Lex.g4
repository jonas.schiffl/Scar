lexer grammar Lex;

APPLICATION : 'application' ;
CONTRACT : 'contract' ;
STATEVARS : 'state' ;
FUNCTIONS : 'functions' ;
INIT: 'init' ;
INVARIANT : 'invariant' ;

CONFIG : 'config' ;

APPINIT : 'appinit' ;

BOOL : 'bool' ;
INT : 'int' ;
UINT : 'uint' ;
STRING : 'string' ;
ACCOUNT : 'account' ;
MAPPING: 'mapping' ;

STRUCT : 'struct' ;
ENUM : 'enum' ;

VAR : 'var' ;
TYPE : 'type' ;
FUN : 'fun' ;
INITPARAMS : 'initparams' ;
PARAMS : 'params' ;
RETURNS : 'returns' ;
PRE : 'pre' ;
POST : 'post' ;

TEMPSPEC : 'tempspec' ;

WEAKUNTIL : 'UW' ;
BOX : 'G' ;
ENABLED : 'enabled' ;
ENABLEDUNTIL : 'enabledU' ;
OWNS : 'owns' ;

CALLS : 'calls' ;
FRAMES : 'frames' ;
MODIFIES : 'modifies' ;
TRANSFERS : 'transfers' ;

ROLES : 'roles' ;
ROLE : 'role' ;

RESULT : '\\result' ;
THIS: '\\this' ;
ANY : '\\any' ;
SELF : '\\self' ;
CALLER : '\\caller' ;
BLOCKNUM : '\\blocknum' ;
SYSTIME : '\\systime' ;
AMT : '\\amt' ;
OLD : '\\old' ;
SEND : '\\send' ;
HASH : '\\hash' ;
SIZE : '\\size' ;
KEYS : '\\keys' ;
VALUES : '\\values' ;
SUM : '\\sum' ;
IN : '\\in' ;

FORALL : '\\forall' ;
EXISTS : '\\exists' ;

BOOL_LIT : 'true' | 'false' ;

INT_LIT : '0' | [1-9][0-9]* ;

STRING_LIT : '"' [a-zA-Z0-9$_]* '"' ;

ID : IdentifierStart IdentifierPart* ;
fragment IdentifierStart : [a-zA-Z$_] ;
fragment IdentifierPart : [a-zA-Z0-9$_] ;

WS : [ \t\r\n\u000C]+ -> channel(HIDDEN) ;

COMMENT : '/*' .*? '*/' -> channel(HIDDEN) ;

LINE_COMMENT : '//' ~[@\r\n]* -> channel(HIDDEN) ;