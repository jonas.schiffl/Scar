grammar SCApp;

import Lex;

scapp : APPLICATION ':' name=ID
        (udts+=userDefType+)?
        roleset?
        liveSpec?
        appInit?
        (contracts+=contract)+ EOF ;

userDefType : structDecl | enumDecl ;

structDecl: STRUCT name=ID '{' (vars+=structVar (',' vars+=structVar)*)? '}';

structVar: name=ID ':' t=typename;

enumDecl: ENUM name=ID '{' consts+=ID (',' consts+=ID)* '}';

roleset : ROLES ':' (roles+=role | accounts+=account)+;

role : ROLE ID (':' capabilities)? ;
account : ACCOUNT ID (':' capabilities)? ;

capabilities : (mods+=modifyCapa | calls+=callCapa | txs+=transferCapa)+;

callCapa : CALLS ':' callees+=qualifiedFun (',' callees+=qualifiedFun)* ;

modifyCapa : MODIFIES ':' locs+=location (',' locs+=location)* ;

transferCapa : TRANSFERS ':' transfers+=transfer (',' transfers+=transfer)* ;

transfer : '(' from=accountSet ',' to=accountSet ',' limitExpr=expr ')';

accountSet : CALLER | ANY | SELF | THIS | ID;

liveSpec : lProp+ ;

lProp : TEMPSPEC ':' expr ;

appInit : APPINIT ':' conList ;

conList : conInit (',' conInit)* ;

conInit : (conName=ID ':' conType=ID '('initparams+=expr (',' initparams+=expr)* ')')+ ;

contract : CONTRACT name=ID ':'
           STATEVARS ':' stateVars+=stateVar*
           (INVARIANT ':' invariants+=expr)*
           (INITPARAMS ':' inparams=params)?
           (INIT ':' initConditions+=expr)*
           FUNCTIONS ':' functions+=function+ ;

stateVar : VAR name=ID ':' typename ;

typename : MAPPING '('fromType=typename '=>' toType=typename ')'
            | arrType=typename'[]'
            | elementaryTypeName
            | userDefTypeName=ID ;

elementaryTypeName : BOOL | STRING | INT | UINT | ACCOUNT ;

function : FUN name=ID ':'
           (PARAMS ':' params)?
           (RETURNS ':' rettype=typename)?
           (PRE ':' preconditions+=expr)*
           (POST ':' postconditions+=expr)*
           capabilities? ;

qualifiedFun : contractName=ID'.' funName=ID ;

location : contractName=ID '.' varName=ID                           #varLocExpr
        | contractName=ID '.' arrMapName=ID '[' arrMapIndExpr ']'   #arrMapLocExpr
        | contractName=ID '.' structName=ID '#' structElemExpr      #structLocExpr ;

arrMapIndExpr : star='*'                            #starExpr
                | fromExpr=expr '..' toExpr=expr    #rangeExpr
                | expr                              #indexExpr;

structElemExpr : star='*' | structElemName=ID ;

params : param (',' param)* ;

param : typename name=ID ;

expr :  arrName=expr '[' index=expr ']'               #accessExpr
        | funName=expr '(' ps=argList ')'             #funCallExpr
        | expr '.' identifier=ID                      #memberAccessExpr
        | expr '#' identifier=ID                      #enumStructExpr
        | '(' expr ')'                                #parExpr
        | '-' expr                                    #unaryMinusExpr
        | l=expr op=('*' | '/' | '%') r=expr          #mulModExpr
        | l=expr op=('+' | '-') r=expr                #addSubExpr
        | l=expr op=('<' | '>' | '<=' | '>=') r=expr  #inEqExpr
        | l=expr op=('=='|'!='|'==='|'!==') r=expr    #eqExpr
        | '!' expr                                    #logicNotExpr
        | l=expr '&&' r=expr                          #landExpr
        | l=expr '||' r=expr                          #lorExpr
        | l=expr '=>' r=expr                          #impExpr
        | quantifier '(' v=ID ':' t=typename ')' ':' matrix=expr  #quantifiedexpr
        | quantifier '(' v=ID IN set=expr ')' ':'    matrix=expr  #setquantifiedexpr
        | SIZE '(' expr ')'                           #sizeExpr
        | VALUES '(' expr ')'                         #valuesExpr
        | KEYS '(' expr ')'                           #keysExpr
        | SUM '(' expr ')'                            #sumExpr
        | HASH '(' expr ')'                           #hashExpr
        | OLD '(' expr ')'                            #oldExpr
        | SEND '('from=expr ',' to=expr ',' amt=expr ')'            #sendExpr
        | l=expr WEAKUNTIL r=expr                     #weakUntilExpr
        | BOX '(' expr ')'                            #boxExpr
        | ENABLED '{' accExpr=expr? '}' '{' paramCond=expr? '}' '{' amtExpr=expr? '}' '(' e=expr ')'       #enabledExpr
        | ENABLEDUNTIL '{' accExpr=expr? '}' '{' paramCond=expr? '}' '{' amtExpr=expr? '}' '(' e=expr ')'  #enabledUntilExpr
        | conName=ID '.' funName=ID '{' accExpr=expr? '}' '{' paramCond=expr? '}' '{' amtExpr=expr? '}'    #txExpr
        | OWNS '('accExpr=expr ',' amtExpr=expr')'    #ownsExpr
        | primaryExpr                                 #primExpr ;

argList : (args+=expr (',' args+=expr)*)? ;

primaryExpr : BOOL_LIT               #boolLitExpr
        | INT_LIT                    #numLitExpr
        | STRING_LIT                 #stringLitExpr
        | THIS                       #thisExpr
        | RESULT                     #resultExpr
        | SELF                       #selfExpr
        | CALLER                     #callerExpr
        | AMT                        #amtExpr
        | BLOCKNUM                   #blocknumExpr
        | SYSTIME                    #systimeExpr
        | id=ID                      #idExpr ;

quantifier : FORALL | EXISTS ;