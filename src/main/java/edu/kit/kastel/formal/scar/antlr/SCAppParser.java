// Generated from /home/jonas/projects/Scar/src/main/antlr/edu/kit/kastel/formal/scar/SCApp.g4 by ANTLR 4.13.2
package edu.kit.kastel.formal.scar.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue", "this-escape"})
public class SCAppParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, APPLICATION=30, CONTRACT=31, 
		STATEVARS=32, FUNCTIONS=33, INIT=34, INVARIANT=35, CONFIG=36, APPINIT=37, 
		BOOL=38, INT=39, UINT=40, STRING=41, ACCOUNT=42, MAPPING=43, STRUCT=44, 
		ENUM=45, VAR=46, TYPE=47, FUN=48, INITPARAMS=49, PARAMS=50, RETURNS=51, 
		PRE=52, POST=53, TEMPSPEC=54, WEAKUNTIL=55, BOX=56, ENABLED=57, ENABLEDUNTIL=58, 
		OWNS=59, CALLS=60, FRAMES=61, MODIFIES=62, TRANSFERS=63, ROLES=64, ROLE=65, 
		RESULT=66, THIS=67, ANY=68, SELF=69, CALLER=70, BLOCKNUM=71, SYSTIME=72, 
		AMT=73, OLD=74, SEND=75, HASH=76, SIZE=77, KEYS=78, VALUES=79, SUM=80, 
		IN=81, FORALL=82, EXISTS=83, BOOL_LIT=84, INT_LIT=85, STRING_LIT=86, ID=87, 
		WS=88, COMMENT=89, LINE_COMMENT=90;
	public static final int
		RULE_scapp = 0, RULE_userDefType = 1, RULE_structDecl = 2, RULE_structVar = 3, 
		RULE_enumDecl = 4, RULE_roleset = 5, RULE_role = 6, RULE_account = 7, 
		RULE_capabilities = 8, RULE_callCapa = 9, RULE_modifyCapa = 10, RULE_transferCapa = 11, 
		RULE_transfer = 12, RULE_accountSet = 13, RULE_liveSpec = 14, RULE_lProp = 15, 
		RULE_appInit = 16, RULE_conList = 17, RULE_conInit = 18, RULE_contract = 19, 
		RULE_stateVar = 20, RULE_typename = 21, RULE_elementaryTypeName = 22, 
		RULE_function = 23, RULE_qualifiedFun = 24, RULE_location = 25, RULE_arrMapIndExpr = 26, 
		RULE_structElemExpr = 27, RULE_params = 28, RULE_param = 29, RULE_expr = 30, 
		RULE_argList = 31, RULE_primaryExpr = 32, RULE_quantifier = 33;
	private static String[] makeRuleNames() {
		return new String[] {
			"scapp", "userDefType", "structDecl", "structVar", "enumDecl", "roleset", 
			"role", "account", "capabilities", "callCapa", "modifyCapa", "transferCapa", 
			"transfer", "accountSet", "liveSpec", "lProp", "appInit", "conList", 
			"conInit", "contract", "stateVar", "typename", "elementaryTypeName", 
			"function", "qualifiedFun", "location", "arrMapIndExpr", "structElemExpr", 
			"params", "param", "expr", "argList", "primaryExpr", "quantifier"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "':'", "'{'", "','", "'}'", "'('", "')'", "'=>'", "'[]'", "'.'", 
			"'['", "']'", "'#'", "'*'", "'..'", "'-'", "'/'", "'%'", "'+'", "'<'", 
			"'>'", "'<='", "'>='", "'=='", "'!='", "'==='", "'!=='", "'!'", "'&&'", 
			"'||'", "'application'", "'contract'", "'state'", "'functions'", "'init'", 
			"'invariant'", "'config'", "'appinit'", "'bool'", "'int'", "'uint'", 
			"'string'", "'account'", "'mapping'", "'struct'", "'enum'", "'var'", 
			"'type'", "'fun'", "'initparams'", "'params'", "'returns'", "'pre'", 
			"'post'", "'tempspec'", "'UW'", "'G'", "'enabled'", "'enabledU'", "'owns'", 
			"'calls'", "'frames'", "'modifies'", "'transfers'", "'roles'", "'role'", 
			"'\\result'", "'\\this'", "'\\any'", "'\\self'", "'\\caller'", "'\\blocknum'", 
			"'\\systime'", "'\\amt'", "'\\old'", "'\\send'", "'\\hash'", "'\\size'", 
			"'\\keys'", "'\\values'", "'\\sum'", "'\\in'", "'\\forall'", "'\\exists'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "APPLICATION", "CONTRACT", "STATEVARS", 
			"FUNCTIONS", "INIT", "INVARIANT", "CONFIG", "APPINIT", "BOOL", "INT", 
			"UINT", "STRING", "ACCOUNT", "MAPPING", "STRUCT", "ENUM", "VAR", "TYPE", 
			"FUN", "INITPARAMS", "PARAMS", "RETURNS", "PRE", "POST", "TEMPSPEC", 
			"WEAKUNTIL", "BOX", "ENABLED", "ENABLEDUNTIL", "OWNS", "CALLS", "FRAMES", 
			"MODIFIES", "TRANSFERS", "ROLES", "ROLE", "RESULT", "THIS", "ANY", "SELF", 
			"CALLER", "BLOCKNUM", "SYSTIME", "AMT", "OLD", "SEND", "HASH", "SIZE", 
			"KEYS", "VALUES", "SUM", "IN", "FORALL", "EXISTS", "BOOL_LIT", "INT_LIT", 
			"STRING_LIT", "ID", "WS", "COMMENT", "LINE_COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SCApp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SCAppParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ScappContext extends ParserRuleContext {
		public Token name;
		public UserDefTypeContext userDefType;
		public List<UserDefTypeContext> udts = new ArrayList<UserDefTypeContext>();
		public ContractContext contract;
		public List<ContractContext> contracts = new ArrayList<ContractContext>();
		public TerminalNode APPLICATION() { return getToken(SCAppParser.APPLICATION, 0); }
		public TerminalNode EOF() { return getToken(SCAppParser.EOF, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public RolesetContext roleset() {
			return getRuleContext(RolesetContext.class,0);
		}
		public LiveSpecContext liveSpec() {
			return getRuleContext(LiveSpecContext.class,0);
		}
		public AppInitContext appInit() {
			return getRuleContext(AppInitContext.class,0);
		}
		public List<ContractContext> contract() {
			return getRuleContexts(ContractContext.class);
		}
		public ContractContext contract(int i) {
			return getRuleContext(ContractContext.class,i);
		}
		public List<UserDefTypeContext> userDefType() {
			return getRuleContexts(UserDefTypeContext.class);
		}
		public UserDefTypeContext userDefType(int i) {
			return getRuleContext(UserDefTypeContext.class,i);
		}
		public ScappContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scapp; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitScapp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ScappContext scapp() throws RecognitionException {
		ScappContext _localctx = new ScappContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_scapp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			match(APPLICATION);
			setState(69);
			match(T__0);
			setState(70);
			((ScappContext)_localctx).name = match(ID);
			setState(76);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STRUCT || _la==ENUM) {
				{
				setState(72); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(71);
					((ScappContext)_localctx).userDefType = userDefType();
					((ScappContext)_localctx).udts.add(((ScappContext)_localctx).userDefType);
					}
					}
					setState(74); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==STRUCT || _la==ENUM );
				}
			}

			setState(79);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ROLES) {
				{
				setState(78);
				roleset();
				}
			}

			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TEMPSPEC) {
				{
				setState(81);
				liveSpec();
				}
			}

			setState(85);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==APPINIT) {
				{
				setState(84);
				appInit();
				}
			}

			setState(88); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(87);
				((ScappContext)_localctx).contract = contract();
				((ScappContext)_localctx).contracts.add(((ScappContext)_localctx).contract);
				}
				}
				setState(90); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CONTRACT );
			setState(92);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class UserDefTypeContext extends ParserRuleContext {
		public StructDeclContext structDecl() {
			return getRuleContext(StructDeclContext.class,0);
		}
		public EnumDeclContext enumDecl() {
			return getRuleContext(EnumDeclContext.class,0);
		}
		public UserDefTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_userDefType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitUserDefType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UserDefTypeContext userDefType() throws RecognitionException {
		UserDefTypeContext _localctx = new UserDefTypeContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_userDefType);
		try {
			setState(96);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRUCT:
				enterOuterAlt(_localctx, 1);
				{
				setState(94);
				structDecl();
				}
				break;
			case ENUM:
				enterOuterAlt(_localctx, 2);
				{
				setState(95);
				enumDecl();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructDeclContext extends ParserRuleContext {
		public Token name;
		public StructVarContext structVar;
		public List<StructVarContext> vars = new ArrayList<StructVarContext>();
		public TerminalNode STRUCT() { return getToken(SCAppParser.STRUCT, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public List<StructVarContext> structVar() {
			return getRuleContexts(StructVarContext.class);
		}
		public StructVarContext structVar(int i) {
			return getRuleContext(StructVarContext.class,i);
		}
		public StructDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structDecl; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStructDecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructDeclContext structDecl() throws RecognitionException {
		StructDeclContext _localctx = new StructDeclContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_structDecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			match(STRUCT);
			setState(99);
			((StructDeclContext)_localctx).name = match(ID);
			setState(100);
			match(T__1);
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(101);
				((StructDeclContext)_localctx).structVar = structVar();
				((StructDeclContext)_localctx).vars.add(((StructDeclContext)_localctx).structVar);
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(102);
					match(T__2);
					setState(103);
					((StructDeclContext)_localctx).structVar = structVar();
					((StructDeclContext)_localctx).vars.add(((StructDeclContext)_localctx).structVar);
					}
					}
					setState(108);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(111);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructVarContext extends ParserRuleContext {
		public Token name;
		public TypenameContext t;
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public TypenameContext typename() {
			return getRuleContext(TypenameContext.class,0);
		}
		public StructVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structVar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStructVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructVarContext structVar() throws RecognitionException {
		StructVarContext _localctx = new StructVarContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_structVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			((StructVarContext)_localctx).name = match(ID);
			setState(114);
			match(T__0);
			setState(115);
			((StructVarContext)_localctx).t = typename(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class EnumDeclContext extends ParserRuleContext {
		public Token name;
		public Token ID;
		public List<Token> consts = new ArrayList<Token>();
		public TerminalNode ENUM() { return getToken(SCAppParser.ENUM, 0); }
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public EnumDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumDecl; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitEnumDecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EnumDeclContext enumDecl() throws RecognitionException {
		EnumDeclContext _localctx = new EnumDeclContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_enumDecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			match(ENUM);
			setState(118);
			((EnumDeclContext)_localctx).name = match(ID);
			setState(119);
			match(T__1);
			setState(120);
			((EnumDeclContext)_localctx).ID = match(ID);
			((EnumDeclContext)_localctx).consts.add(((EnumDeclContext)_localctx).ID);
			setState(125);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(121);
				match(T__2);
				setState(122);
				((EnumDeclContext)_localctx).ID = match(ID);
				((EnumDeclContext)_localctx).consts.add(((EnumDeclContext)_localctx).ID);
				}
				}
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(128);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RolesetContext extends ParserRuleContext {
		public RoleContext role;
		public List<RoleContext> roles = new ArrayList<RoleContext>();
		public AccountContext account;
		public List<AccountContext> accounts = new ArrayList<AccountContext>();
		public TerminalNode ROLES() { return getToken(SCAppParser.ROLES, 0); }
		public List<RoleContext> role() {
			return getRuleContexts(RoleContext.class);
		}
		public RoleContext role(int i) {
			return getRuleContext(RoleContext.class,i);
		}
		public List<AccountContext> account() {
			return getRuleContexts(AccountContext.class);
		}
		public AccountContext account(int i) {
			return getRuleContext(AccountContext.class,i);
		}
		public RolesetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roleset; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitRoleset(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RolesetContext roleset() throws RecognitionException {
		RolesetContext _localctx = new RolesetContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_roleset);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130);
			match(ROLES);
			setState(131);
			match(T__0);
			setState(134); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(134);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ROLE:
					{
					setState(132);
					((RolesetContext)_localctx).role = role();
					((RolesetContext)_localctx).roles.add(((RolesetContext)_localctx).role);
					}
					break;
				case ACCOUNT:
					{
					setState(133);
					((RolesetContext)_localctx).account = account();
					((RolesetContext)_localctx).accounts.add(((RolesetContext)_localctx).account);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(136); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ACCOUNT || _la==ROLE );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RoleContext extends ParserRuleContext {
		public TerminalNode ROLE() { return getToken(SCAppParser.ROLE, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public CapabilitiesContext capabilities() {
			return getRuleContext(CapabilitiesContext.class,0);
		}
		public RoleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_role; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitRole(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RoleContext role() throws RecognitionException {
		RoleContext _localctx = new RoleContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_role);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(138);
			match(ROLE);
			setState(139);
			match(ID);
			setState(142);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(140);
				match(T__0);
				setState(141);
				capabilities();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AccountContext extends ParserRuleContext {
		public TerminalNode ACCOUNT() { return getToken(SCAppParser.ACCOUNT, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public CapabilitiesContext capabilities() {
			return getRuleContext(CapabilitiesContext.class,0);
		}
		public AccountContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_account; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitAccount(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AccountContext account() throws RecognitionException {
		AccountContext _localctx = new AccountContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_account);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			match(ACCOUNT);
			setState(145);
			match(ID);
			setState(148);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(146);
				match(T__0);
				setState(147);
				capabilities();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CapabilitiesContext extends ParserRuleContext {
		public ModifyCapaContext modifyCapa;
		public List<ModifyCapaContext> mods = new ArrayList<ModifyCapaContext>();
		public CallCapaContext callCapa;
		public List<CallCapaContext> calls = new ArrayList<CallCapaContext>();
		public TransferCapaContext transferCapa;
		public List<TransferCapaContext> txs = new ArrayList<TransferCapaContext>();
		public List<ModifyCapaContext> modifyCapa() {
			return getRuleContexts(ModifyCapaContext.class);
		}
		public ModifyCapaContext modifyCapa(int i) {
			return getRuleContext(ModifyCapaContext.class,i);
		}
		public List<CallCapaContext> callCapa() {
			return getRuleContexts(CallCapaContext.class);
		}
		public CallCapaContext callCapa(int i) {
			return getRuleContext(CallCapaContext.class,i);
		}
		public List<TransferCapaContext> transferCapa() {
			return getRuleContexts(TransferCapaContext.class);
		}
		public TransferCapaContext transferCapa(int i) {
			return getRuleContext(TransferCapaContext.class,i);
		}
		public CapabilitiesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_capabilities; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitCapabilities(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CapabilitiesContext capabilities() throws RecognitionException {
		CapabilitiesContext _localctx = new CapabilitiesContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_capabilities);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(153);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case MODIFIES:
					{
					setState(150);
					((CapabilitiesContext)_localctx).modifyCapa = modifyCapa();
					((CapabilitiesContext)_localctx).mods.add(((CapabilitiesContext)_localctx).modifyCapa);
					}
					break;
				case CALLS:
					{
					setState(151);
					((CapabilitiesContext)_localctx).callCapa = callCapa();
					((CapabilitiesContext)_localctx).calls.add(((CapabilitiesContext)_localctx).callCapa);
					}
					break;
				case TRANSFERS:
					{
					setState(152);
					((CapabilitiesContext)_localctx).transferCapa = transferCapa();
					((CapabilitiesContext)_localctx).txs.add(((CapabilitiesContext)_localctx).transferCapa);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(155); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & -3458764513820540928L) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CallCapaContext extends ParserRuleContext {
		public QualifiedFunContext qualifiedFun;
		public List<QualifiedFunContext> callees = new ArrayList<QualifiedFunContext>();
		public TerminalNode CALLS() { return getToken(SCAppParser.CALLS, 0); }
		public List<QualifiedFunContext> qualifiedFun() {
			return getRuleContexts(QualifiedFunContext.class);
		}
		public QualifiedFunContext qualifiedFun(int i) {
			return getRuleContext(QualifiedFunContext.class,i);
		}
		public CallCapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callCapa; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitCallCapa(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallCapaContext callCapa() throws RecognitionException {
		CallCapaContext _localctx = new CallCapaContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_callCapa);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(CALLS);
			setState(158);
			match(T__0);
			setState(159);
			((CallCapaContext)_localctx).qualifiedFun = qualifiedFun();
			((CallCapaContext)_localctx).callees.add(((CallCapaContext)_localctx).qualifiedFun);
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(160);
				match(T__2);
				setState(161);
				((CallCapaContext)_localctx).qualifiedFun = qualifiedFun();
				((CallCapaContext)_localctx).callees.add(((CallCapaContext)_localctx).qualifiedFun);
				}
				}
				setState(166);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ModifyCapaContext extends ParserRuleContext {
		public LocationContext location;
		public List<LocationContext> locs = new ArrayList<LocationContext>();
		public TerminalNode MODIFIES() { return getToken(SCAppParser.MODIFIES, 0); }
		public List<LocationContext> location() {
			return getRuleContexts(LocationContext.class);
		}
		public LocationContext location(int i) {
			return getRuleContext(LocationContext.class,i);
		}
		public ModifyCapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modifyCapa; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitModifyCapa(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModifyCapaContext modifyCapa() throws RecognitionException {
		ModifyCapaContext _localctx = new ModifyCapaContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_modifyCapa);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(167);
			match(MODIFIES);
			setState(168);
			match(T__0);
			setState(169);
			((ModifyCapaContext)_localctx).location = location();
			((ModifyCapaContext)_localctx).locs.add(((ModifyCapaContext)_localctx).location);
			setState(174);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(170);
				match(T__2);
				setState(171);
				((ModifyCapaContext)_localctx).location = location();
				((ModifyCapaContext)_localctx).locs.add(((ModifyCapaContext)_localctx).location);
				}
				}
				setState(176);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TransferCapaContext extends ParserRuleContext {
		public TransferContext transfer;
		public List<TransferContext> transfers = new ArrayList<TransferContext>();
		public TerminalNode TRANSFERS() { return getToken(SCAppParser.TRANSFERS, 0); }
		public List<TransferContext> transfer() {
			return getRuleContexts(TransferContext.class);
		}
		public TransferContext transfer(int i) {
			return getRuleContext(TransferContext.class,i);
		}
		public TransferCapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transferCapa; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitTransferCapa(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TransferCapaContext transferCapa() throws RecognitionException {
		TransferCapaContext _localctx = new TransferCapaContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_transferCapa);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(177);
			match(TRANSFERS);
			setState(178);
			match(T__0);
			setState(179);
			((TransferCapaContext)_localctx).transfer = transfer();
			((TransferCapaContext)_localctx).transfers.add(((TransferCapaContext)_localctx).transfer);
			setState(184);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(180);
				match(T__2);
				setState(181);
				((TransferCapaContext)_localctx).transfer = transfer();
				((TransferCapaContext)_localctx).transfers.add(((TransferCapaContext)_localctx).transfer);
				}
				}
				setState(186);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TransferContext extends ParserRuleContext {
		public AccountSetContext from;
		public AccountSetContext to;
		public ExprContext limitExpr;
		public List<AccountSetContext> accountSet() {
			return getRuleContexts(AccountSetContext.class);
		}
		public AccountSetContext accountSet(int i) {
			return getRuleContext(AccountSetContext.class,i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TransferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transfer; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitTransfer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TransferContext transfer() throws RecognitionException {
		TransferContext _localctx = new TransferContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_transfer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			match(T__4);
			setState(188);
			((TransferContext)_localctx).from = accountSet();
			setState(189);
			match(T__2);
			setState(190);
			((TransferContext)_localctx).to = accountSet();
			setState(191);
			match(T__2);
			setState(192);
			((TransferContext)_localctx).limitExpr = expr(0);
			setState(193);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AccountSetContext extends ParserRuleContext {
		public TerminalNode CALLER() { return getToken(SCAppParser.CALLER, 0); }
		public TerminalNode ANY() { return getToken(SCAppParser.ANY, 0); }
		public TerminalNode SELF() { return getToken(SCAppParser.SELF, 0); }
		public TerminalNode THIS() { return getToken(SCAppParser.THIS, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public AccountSetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_accountSet; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitAccountSet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AccountSetContext accountSet() throws RecognitionException {
		AccountSetContext _localctx = new AccountSetContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_accountSet);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			_la = _input.LA(1);
			if ( !(((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & 1048591L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LiveSpecContext extends ParserRuleContext {
		public List<LPropContext> lProp() {
			return getRuleContexts(LPropContext.class);
		}
		public LPropContext lProp(int i) {
			return getRuleContext(LPropContext.class,i);
		}
		public LiveSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_liveSpec; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitLiveSpec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiveSpecContext liveSpec() throws RecognitionException {
		LiveSpecContext _localctx = new LiveSpecContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_liveSpec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(197);
				lProp();
				}
				}
				setState(200); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TEMPSPEC );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LPropContext extends ParserRuleContext {
		public TerminalNode TEMPSPEC() { return getToken(SCAppParser.TEMPSPEC, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public LPropContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lProp; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitLProp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LPropContext lProp() throws RecognitionException {
		LPropContext _localctx = new LPropContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_lProp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(202);
			match(TEMPSPEC);
			setState(203);
			match(T__0);
			setState(204);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AppInitContext extends ParserRuleContext {
		public TerminalNode APPINIT() { return getToken(SCAppParser.APPINIT, 0); }
		public ConListContext conList() {
			return getRuleContext(ConListContext.class,0);
		}
		public AppInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_appInit; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitAppInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AppInitContext appInit() throws RecognitionException {
		AppInitContext _localctx = new AppInitContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_appInit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			match(APPINIT);
			setState(207);
			match(T__0);
			setState(208);
			conList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ConListContext extends ParserRuleContext {
		public List<ConInitContext> conInit() {
			return getRuleContexts(ConInitContext.class);
		}
		public ConInitContext conInit(int i) {
			return getRuleContext(ConInitContext.class,i);
		}
		public ConListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitConList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConListContext conList() throws RecognitionException {
		ConListContext _localctx = new ConListContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_conList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			conInit();
			setState(215);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(211);
				match(T__2);
				setState(212);
				conInit();
				}
				}
				setState(217);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ConInitContext extends ParserRuleContext {
		public Token conName;
		public Token conType;
		public ExprContext expr;
		public List<ExprContext> initparams = new ArrayList<ExprContext>();
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ConInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conInit; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitConInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConInitContext conInit() throws RecognitionException {
		ConInitContext _localctx = new ConInitContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_conInit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(232); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(218);
				((ConInitContext)_localctx).conName = match(ID);
				setState(219);
				match(T__0);
				setState(220);
				((ConInitContext)_localctx).conType = match(ID);
				setState(221);
				match(T__4);
				setState(222);
				((ConInitContext)_localctx).expr = expr(0);
				((ConInitContext)_localctx).initparams.add(((ConInitContext)_localctx).expr);
				setState(227);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(223);
					match(T__2);
					setState(224);
					((ConInitContext)_localctx).expr = expr(0);
					((ConInitContext)_localctx).initparams.add(((ConInitContext)_localctx).expr);
					}
					}
					setState(229);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(230);
				match(T__5);
				}
				}
				setState(234); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ContractContext extends ParserRuleContext {
		public Token name;
		public StateVarContext stateVar;
		public List<StateVarContext> stateVars = new ArrayList<StateVarContext>();
		public ExprContext expr;
		public List<ExprContext> invariants = new ArrayList<ExprContext>();
		public ParamsContext inparams;
		public List<ExprContext> initConditions = new ArrayList<ExprContext>();
		public FunctionContext function;
		public List<FunctionContext> functions = new ArrayList<FunctionContext>();
		public TerminalNode CONTRACT() { return getToken(SCAppParser.CONTRACT, 0); }
		public TerminalNode STATEVARS() { return getToken(SCAppParser.STATEVARS, 0); }
		public TerminalNode FUNCTIONS() { return getToken(SCAppParser.FUNCTIONS, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public List<TerminalNode> INVARIANT() { return getTokens(SCAppParser.INVARIANT); }
		public TerminalNode INVARIANT(int i) {
			return getToken(SCAppParser.INVARIANT, i);
		}
		public TerminalNode INITPARAMS() { return getToken(SCAppParser.INITPARAMS, 0); }
		public List<TerminalNode> INIT() { return getTokens(SCAppParser.INIT); }
		public TerminalNode INIT(int i) {
			return getToken(SCAppParser.INIT, i);
		}
		public List<StateVarContext> stateVar() {
			return getRuleContexts(StateVarContext.class);
		}
		public StateVarContext stateVar(int i) {
			return getRuleContext(StateVarContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ParamsContext params() {
			return getRuleContext(ParamsContext.class,0);
		}
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public ContractContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contract; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitContract(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ContractContext contract() throws RecognitionException {
		ContractContext _localctx = new ContractContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_contract);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(236);
			match(CONTRACT);
			setState(237);
			((ContractContext)_localctx).name = match(ID);
			setState(238);
			match(T__0);
			setState(239);
			match(STATEVARS);
			setState(240);
			match(T__0);
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==VAR) {
				{
				{
				setState(241);
				((ContractContext)_localctx).stateVar = stateVar();
				((ContractContext)_localctx).stateVars.add(((ContractContext)_localctx).stateVar);
				}
				}
				setState(246);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(252);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INVARIANT) {
				{
				{
				setState(247);
				match(INVARIANT);
				setState(248);
				match(T__0);
				setState(249);
				((ContractContext)_localctx).expr = expr(0);
				((ContractContext)_localctx).invariants.add(((ContractContext)_localctx).expr);
				}
				}
				setState(254);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(258);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INITPARAMS) {
				{
				setState(255);
				match(INITPARAMS);
				setState(256);
				match(T__0);
				setState(257);
				((ContractContext)_localctx).inparams = params();
				}
			}

			setState(265);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INIT) {
				{
				{
				setState(260);
				match(INIT);
				setState(261);
				match(T__0);
				setState(262);
				((ContractContext)_localctx).expr = expr(0);
				((ContractContext)_localctx).initConditions.add(((ContractContext)_localctx).expr);
				}
				}
				setState(267);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(268);
			match(FUNCTIONS);
			setState(269);
			match(T__0);
			setState(271); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(270);
				((ContractContext)_localctx).function = function();
				((ContractContext)_localctx).functions.add(((ContractContext)_localctx).function);
				}
				}
				setState(273); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==FUN );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StateVarContext extends ParserRuleContext {
		public Token name;
		public TerminalNode VAR() { return getToken(SCAppParser.VAR, 0); }
		public TypenameContext typename() {
			return getRuleContext(TypenameContext.class,0);
		}
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public StateVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stateVar; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStateVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StateVarContext stateVar() throws RecognitionException {
		StateVarContext _localctx = new StateVarContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_stateVar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(275);
			match(VAR);
			setState(276);
			((StateVarContext)_localctx).name = match(ID);
			setState(277);
			match(T__0);
			setState(278);
			typename(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TypenameContext extends ParserRuleContext {
		public TypenameContext arrType;
		public TypenameContext fromType;
		public TypenameContext toType;
		public Token userDefTypeName;
		public TerminalNode MAPPING() { return getToken(SCAppParser.MAPPING, 0); }
		public List<TypenameContext> typename() {
			return getRuleContexts(TypenameContext.class);
		}
		public TypenameContext typename(int i) {
			return getRuleContext(TypenameContext.class,i);
		}
		public ElementaryTypeNameContext elementaryTypeName() {
			return getRuleContext(ElementaryTypeNameContext.class,0);
		}
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public TypenameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typename; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitTypename(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypenameContext typename() throws RecognitionException {
		return typename(0);
	}

	private TypenameContext typename(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypenameContext _localctx = new TypenameContext(_ctx, _parentState);
		TypenameContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_typename, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(290);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MAPPING:
				{
				setState(281);
				match(MAPPING);
				setState(282);
				match(T__4);
				setState(283);
				((TypenameContext)_localctx).fromType = typename(0);
				setState(284);
				match(T__6);
				setState(285);
				((TypenameContext)_localctx).toType = typename(0);
				setState(286);
				match(T__5);
				}
				break;
			case BOOL:
			case INT:
			case UINT:
			case STRING:
			case ACCOUNT:
				{
				setState(288);
				elementaryTypeName();
				}
				break;
			case ID:
				{
				setState(289);
				((TypenameContext)_localctx).userDefTypeName = match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(296);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new TypenameContext(_parentctx, _parentState);
					_localctx.arrType = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_typename);
					setState(292);
					if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
					setState(293);
					match(T__7);
					}
					} 
				}
				setState(298);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ElementaryTypeNameContext extends ParserRuleContext {
		public TerminalNode BOOL() { return getToken(SCAppParser.BOOL, 0); }
		public TerminalNode STRING() { return getToken(SCAppParser.STRING, 0); }
		public TerminalNode INT() { return getToken(SCAppParser.INT, 0); }
		public TerminalNode UINT() { return getToken(SCAppParser.UINT, 0); }
		public TerminalNode ACCOUNT() { return getToken(SCAppParser.ACCOUNT, 0); }
		public ElementaryTypeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elementaryTypeName; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitElementaryTypeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElementaryTypeNameContext elementaryTypeName() throws RecognitionException {
		ElementaryTypeNameContext _localctx = new ElementaryTypeNameContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_elementaryTypeName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(299);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 8521215115264L) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FunctionContext extends ParserRuleContext {
		public Token name;
		public TypenameContext rettype;
		public ExprContext expr;
		public List<ExprContext> preconditions = new ArrayList<ExprContext>();
		public List<ExprContext> postconditions = new ArrayList<ExprContext>();
		public TerminalNode FUN() { return getToken(SCAppParser.FUN, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public TerminalNode PARAMS() { return getToken(SCAppParser.PARAMS, 0); }
		public ParamsContext params() {
			return getRuleContext(ParamsContext.class,0);
		}
		public TerminalNode RETURNS() { return getToken(SCAppParser.RETURNS, 0); }
		public List<TerminalNode> PRE() { return getTokens(SCAppParser.PRE); }
		public TerminalNode PRE(int i) {
			return getToken(SCAppParser.PRE, i);
		}
		public List<TerminalNode> POST() { return getTokens(SCAppParser.POST); }
		public TerminalNode POST(int i) {
			return getToken(SCAppParser.POST, i);
		}
		public CapabilitiesContext capabilities() {
			return getRuleContext(CapabilitiesContext.class,0);
		}
		public TypenameContext typename() {
			return getRuleContext(TypenameContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(301);
			match(FUN);
			setState(302);
			((FunctionContext)_localctx).name = match(ID);
			setState(303);
			match(T__0);
			setState(307);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARAMS) {
				{
				setState(304);
				match(PARAMS);
				setState(305);
				match(T__0);
				setState(306);
				params();
				}
			}

			setState(312);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURNS) {
				{
				setState(309);
				match(RETURNS);
				setState(310);
				match(T__0);
				setState(311);
				((FunctionContext)_localctx).rettype = typename(0);
				}
			}

			setState(319);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PRE) {
				{
				{
				setState(314);
				match(PRE);
				setState(315);
				match(T__0);
				setState(316);
				((FunctionContext)_localctx).expr = expr(0);
				((FunctionContext)_localctx).preconditions.add(((FunctionContext)_localctx).expr);
				}
				}
				setState(321);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(327);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==POST) {
				{
				{
				setState(322);
				match(POST);
				setState(323);
				match(T__0);
				setState(324);
				((FunctionContext)_localctx).expr = expr(0);
				((FunctionContext)_localctx).postconditions.add(((FunctionContext)_localctx).expr);
				}
				}
				setState(329);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(331);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & -3458764513820540928L) != 0)) {
				{
				setState(330);
				capabilities();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class QualifiedFunContext extends ParserRuleContext {
		public Token contractName;
		public Token funName;
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public QualifiedFunContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedFun; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitQualifiedFun(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QualifiedFunContext qualifiedFun() throws RecognitionException {
		QualifiedFunContext _localctx = new QualifiedFunContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_qualifiedFun);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(333);
			((QualifiedFunContext)_localctx).contractName = match(ID);
			setState(334);
			match(T__8);
			setState(335);
			((QualifiedFunContext)_localctx).funName = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LocationContext extends ParserRuleContext {
		public LocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_location; }
	 
		public LocationContext() { }
		public void copyFrom(LocationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ArrMapLocExprContext extends LocationContext {
		public Token contractName;
		public Token arrMapName;
		public ArrMapIndExprContext arrMapIndExpr() {
			return getRuleContext(ArrMapIndExprContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public ArrMapLocExprContext(LocationContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitArrMapLocExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StructLocExprContext extends LocationContext {
		public Token contractName;
		public Token structName;
		public StructElemExprContext structElemExpr() {
			return getRuleContext(StructElemExprContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public StructLocExprContext(LocationContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStructLocExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VarLocExprContext extends LocationContext {
		public Token contractName;
		public Token varName;
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public VarLocExprContext(LocationContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitVarLocExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocationContext location() throws RecognitionException {
		LocationContext _localctx = new LocationContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_location);
		try {
			setState(352);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				_localctx = new VarLocExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(337);
				((VarLocExprContext)_localctx).contractName = match(ID);
				setState(338);
				match(T__8);
				setState(339);
				((VarLocExprContext)_localctx).varName = match(ID);
				}
				break;
			case 2:
				_localctx = new ArrMapLocExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(340);
				((ArrMapLocExprContext)_localctx).contractName = match(ID);
				setState(341);
				match(T__8);
				setState(342);
				((ArrMapLocExprContext)_localctx).arrMapName = match(ID);
				setState(343);
				match(T__9);
				setState(344);
				arrMapIndExpr();
				setState(345);
				match(T__10);
				}
				break;
			case 3:
				_localctx = new StructLocExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(347);
				((StructLocExprContext)_localctx).contractName = match(ID);
				setState(348);
				match(T__8);
				setState(349);
				((StructLocExprContext)_localctx).structName = match(ID);
				setState(350);
				match(T__11);
				setState(351);
				structElemExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ArrMapIndExprContext extends ParserRuleContext {
		public ArrMapIndExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrMapIndExpr; }
	 
		public ArrMapIndExprContext() { }
		public void copyFrom(ArrMapIndExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StarExprContext extends ArrMapIndExprContext {
		public Token star;
		public StarExprContext(ArrMapIndExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStarExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IndexExprContext extends ArrMapIndExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public IndexExprContext(ArrMapIndExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitIndexExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class RangeExprContext extends ArrMapIndExprContext {
		public ExprContext fromExpr;
		public ExprContext toExpr;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RangeExprContext(ArrMapIndExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitRangeExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrMapIndExprContext arrMapIndExpr() throws RecognitionException {
		ArrMapIndExprContext _localctx = new ArrMapIndExprContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_arrMapIndExpr);
		try {
			setState(360);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				_localctx = new StarExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(354);
				((StarExprContext)_localctx).star = match(T__12);
				}
				break;
			case 2:
				_localctx = new RangeExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(355);
				((RangeExprContext)_localctx).fromExpr = expr(0);
				setState(356);
				match(T__13);
				setState(357);
				((RangeExprContext)_localctx).toExpr = expr(0);
				}
				break;
			case 3:
				_localctx = new IndexExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(359);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructElemExprContext extends ParserRuleContext {
		public Token star;
		public Token structElemName;
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public StructElemExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structElemExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStructElemExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructElemExprContext structElemExpr() throws RecognitionException {
		StructElemExprContext _localctx = new StructElemExprContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_structElemExpr);
		try {
			setState(364);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__12:
				enterOuterAlt(_localctx, 1);
				{
				setState(362);
				((StructElemExprContext)_localctx).star = match(T__12);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(363);
				((StructElemExprContext)_localctx).structElemName = match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ParamsContext extends ParserRuleContext {
		public List<ParamContext> param() {
			return getRuleContexts(ParamContext.class);
		}
		public ParamContext param(int i) {
			return getRuleContext(ParamContext.class,i);
		}
		public ParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_params; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamsContext params() throws RecognitionException {
		ParamsContext _localctx = new ParamsContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_params);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(366);
			param();
			setState(371);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(367);
				match(T__2);
				setState(368);
				param();
				}
				}
				setState(373);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ParamContext extends ParserRuleContext {
		public Token name;
		public TypenameContext typename() {
			return getRuleContext(TypenameContext.class,0);
		}
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public ParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitParam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamContext param() throws RecognitionException {
		ParamContext _localctx = new ParamContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_param);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(374);
			typename(0);
			setState(375);
			((ParamContext)_localctx).name = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LogicNotExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public LogicNotExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitLogicNotExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SendExprContext extends ExprContext {
		public ExprContext from;
		public ExprContext to;
		public ExprContext amt;
		public TerminalNode SEND() { return getToken(SCAppParser.SEND, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public SendExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitSendExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class OwnsExprContext extends ExprContext {
		public ExprContext accExpr;
		public ExprContext amtExpr;
		public TerminalNode OWNS() { return getToken(SCAppParser.OWNS, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public OwnsExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitOwnsExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ParExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitParExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EnabledExprContext extends ExprContext {
		public ExprContext accExpr;
		public ExprContext paramCond;
		public ExprContext amtExpr;
		public ExprContext e;
		public TerminalNode ENABLED() { return getToken(SCAppParser.ENABLED, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EnabledExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitEnabledExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SetquantifiedexprContext extends ExprContext {
		public Token v;
		public ExprContext set;
		public ExprContext matrix;
		public QuantifierContext quantifier() {
			return getRuleContext(QuantifierContext.class,0);
		}
		public TerminalNode IN() { return getToken(SCAppParser.IN, 0); }
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public SetquantifiedexprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitSetquantifiedexpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LandExprContext extends ExprContext {
		public ExprContext l;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LandExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitLandExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class UnaryMinusExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public UnaryMinusExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitUnaryMinusExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SumExprContext extends ExprContext {
		public TerminalNode SUM() { return getToken(SCAppParser.SUM, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public SumExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitSumExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LorExprContext extends ExprContext {
		public ExprContext l;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LorExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitLorExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ImpExprContext extends ExprContext {
		public ExprContext l;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ImpExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitImpExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MemberAccessExprContext extends ExprContext {
		public Token identifier;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public MemberAccessExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitMemberAccessExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EnabledUntilExprContext extends ExprContext {
		public ExprContext accExpr;
		public ExprContext paramCond;
		public ExprContext amtExpr;
		public ExprContext e;
		public TerminalNode ENABLEDUNTIL() { return getToken(SCAppParser.ENABLEDUNTIL, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EnabledUntilExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitEnabledUntilExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class AccessExprContext extends ExprContext {
		public ExprContext arrName;
		public ExprContext index;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AccessExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitAccessExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SizeExprContext extends ExprContext {
		public TerminalNode SIZE() { return getToken(SCAppParser.SIZE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public SizeExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitSizeExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PrimExprContext extends ExprContext {
		public PrimaryExprContext primaryExpr() {
			return getRuleContext(PrimaryExprContext.class,0);
		}
		public PrimExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitPrimExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class AddSubExprContext extends ExprContext {
		public ExprContext l;
		public Token op;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AddSubExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitAddSubExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MulModExprContext extends ExprContext {
		public ExprContext l;
		public Token op;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public MulModExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitMulModExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class InEqExprContext extends ExprContext {
		public ExprContext l;
		public Token op;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public InEqExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitInEqExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BoxExprContext extends ExprContext {
		public TerminalNode BOX() { return getToken(SCAppParser.BOX, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public BoxExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitBoxExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EnumStructExprContext extends ExprContext {
		public Token identifier;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public EnumStructExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitEnumStructExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class KeysExprContext extends ExprContext {
		public TerminalNode KEYS() { return getToken(SCAppParser.KEYS, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public KeysExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitKeysExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class HashExprContext extends ExprContext {
		public TerminalNode HASH() { return getToken(SCAppParser.HASH, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public HashExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitHashExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EqExprContext extends ExprContext {
		public ExprContext l;
		public Token op;
		public ExprContext r;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public EqExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitEqExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FunCallExprContext extends ExprContext {
		public ExprContext funName;
		public ArgListContext ps;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ArgListContext argList() {
			return getRuleContext(ArgListContext.class,0);
		}
		public FunCallExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitFunCallExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TxExprContext extends ExprContext {
		public Token conName;
		public Token funName;
		public ExprContext accExpr;
		public ExprContext paramCond;
		public ExprContext amtExpr;
		public List<TerminalNode> ID() { return getTokens(SCAppParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SCAppParser.ID, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TxExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitTxExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class QuantifiedexprContext extends ExprContext {
		public Token v;
		public TypenameContext t;
		public ExprContext matrix;
		public QuantifierContext quantifier() {
			return getRuleContext(QuantifierContext.class,0);
		}
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public TypenameContext typename() {
			return getRuleContext(TypenameContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public QuantifiedexprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitQuantifiedexpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ValuesExprContext extends ExprContext {
		public TerminalNode VALUES() { return getToken(SCAppParser.VALUES, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ValuesExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitValuesExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class OldExprContext extends ExprContext {
		public TerminalNode OLD() { return getToken(SCAppParser.OLD, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public OldExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitOldExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class WeakUntilExprContext extends ExprContext {
		public ExprContext l;
		public ExprContext r;
		public TerminalNode WEAKUNTIL() { return getToken(SCAppParser.WEAKUNTIL, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public WeakUntilExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitWeakUntilExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 60;
		enterRecursionRule(_localctx, 60, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(514);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
			case 1:
				{
				_localctx = new ParExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(378);
				match(T__4);
				setState(379);
				expr(0);
				setState(380);
				match(T__5);
				}
				break;
			case 2:
				{
				_localctx = new UnaryMinusExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(382);
				match(T__14);
				setState(383);
				expr(25);
				}
				break;
			case 3:
				{
				_localctx = new LogicNotExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(384);
				match(T__26);
				setState(385);
				expr(20);
				}
				break;
			case 4:
				{
				_localctx = new QuantifiedexprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(386);
				quantifier();
				setState(387);
				match(T__4);
				setState(388);
				((QuantifiedexprContext)_localctx).v = match(ID);
				setState(389);
				match(T__0);
				setState(390);
				((QuantifiedexprContext)_localctx).t = typename(0);
				setState(391);
				match(T__5);
				setState(392);
				match(T__0);
				setState(393);
				((QuantifiedexprContext)_localctx).matrix = expr(16);
				}
				break;
			case 5:
				{
				_localctx = new SetquantifiedexprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(395);
				quantifier();
				setState(396);
				match(T__4);
				setState(397);
				((SetquantifiedexprContext)_localctx).v = match(ID);
				setState(398);
				match(IN);
				setState(399);
				((SetquantifiedexprContext)_localctx).set = expr(0);
				setState(400);
				match(T__5);
				setState(401);
				match(T__0);
				setState(402);
				((SetquantifiedexprContext)_localctx).matrix = expr(15);
				}
				break;
			case 6:
				{
				_localctx = new SizeExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(404);
				match(SIZE);
				setState(405);
				match(T__4);
				setState(406);
				expr(0);
				setState(407);
				match(T__5);
				}
				break;
			case 7:
				{
				_localctx = new ValuesExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(409);
				match(VALUES);
				setState(410);
				match(T__4);
				setState(411);
				expr(0);
				setState(412);
				match(T__5);
				}
				break;
			case 8:
				{
				_localctx = new KeysExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(414);
				match(KEYS);
				setState(415);
				match(T__4);
				setState(416);
				expr(0);
				setState(417);
				match(T__5);
				}
				break;
			case 9:
				{
				_localctx = new SumExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(419);
				match(SUM);
				setState(420);
				match(T__4);
				setState(421);
				expr(0);
				setState(422);
				match(T__5);
				}
				break;
			case 10:
				{
				_localctx = new HashExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(424);
				match(HASH);
				setState(425);
				match(T__4);
				setState(426);
				expr(0);
				setState(427);
				match(T__5);
				}
				break;
			case 11:
				{
				_localctx = new OldExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(429);
				match(OLD);
				setState(430);
				match(T__4);
				setState(431);
				expr(0);
				setState(432);
				match(T__5);
				}
				break;
			case 12:
				{
				_localctx = new SendExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(434);
				match(SEND);
				setState(435);
				match(T__4);
				setState(436);
				((SendExprContext)_localctx).from = expr(0);
				setState(437);
				match(T__2);
				setState(438);
				((SendExprContext)_localctx).to = expr(0);
				setState(439);
				match(T__2);
				setState(440);
				((SendExprContext)_localctx).amt = expr(0);
				setState(441);
				match(T__5);
				}
				break;
			case 13:
				{
				_localctx = new BoxExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(443);
				match(BOX);
				setState(444);
				match(T__4);
				setState(445);
				expr(0);
				setState(446);
				match(T__5);
				}
				break;
			case 14:
				{
				_localctx = new EnabledExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(448);
				match(ENABLED);
				setState(449);
				match(T__1);
				setState(451);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(450);
					((EnabledExprContext)_localctx).accExpr = expr(0);
					}
				}

				setState(453);
				match(T__3);
				setState(454);
				match(T__1);
				setState(456);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(455);
					((EnabledExprContext)_localctx).paramCond = expr(0);
					}
				}

				setState(458);
				match(T__3);
				setState(459);
				match(T__1);
				setState(461);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(460);
					((EnabledExprContext)_localctx).amtExpr = expr(0);
					}
				}

				setState(463);
				match(T__3);
				setState(464);
				match(T__4);
				setState(465);
				((EnabledExprContext)_localctx).e = expr(0);
				setState(466);
				match(T__5);
				}
				break;
			case 15:
				{
				_localctx = new EnabledUntilExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(468);
				match(ENABLEDUNTIL);
				setState(469);
				match(T__1);
				setState(471);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(470);
					((EnabledUntilExprContext)_localctx).accExpr = expr(0);
					}
				}

				setState(473);
				match(T__3);
				setState(474);
				match(T__1);
				setState(476);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(475);
					((EnabledUntilExprContext)_localctx).paramCond = expr(0);
					}
				}

				setState(478);
				match(T__3);
				setState(479);
				match(T__1);
				setState(481);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(480);
					((EnabledUntilExprContext)_localctx).amtExpr = expr(0);
					}
				}

				setState(483);
				match(T__3);
				setState(484);
				match(T__4);
				setState(485);
				((EnabledUntilExprContext)_localctx).e = expr(0);
				setState(486);
				match(T__5);
				}
				break;
			case 16:
				{
				_localctx = new TxExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(488);
				((TxExprContext)_localctx).conName = match(ID);
				setState(489);
				match(T__8);
				setState(490);
				((TxExprContext)_localctx).funName = match(ID);
				setState(491);
				match(T__1);
				setState(493);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(492);
					((TxExprContext)_localctx).accExpr = expr(0);
					}
				}

				setState(495);
				match(T__3);
				setState(496);
				match(T__1);
				setState(498);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(497);
					((TxExprContext)_localctx).paramCond = expr(0);
					}
				}

				setState(500);
				match(T__3);
				setState(501);
				match(T__1);
				setState(503);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
					{
					setState(502);
					((TxExprContext)_localctx).amtExpr = expr(0);
					}
				}

				setState(505);
				match(T__3);
				}
				break;
			case 17:
				{
				_localctx = new OwnsExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(506);
				match(OWNS);
				setState(507);
				match(T__4);
				setState(508);
				((OwnsExprContext)_localctx).accExpr = expr(0);
				setState(509);
				match(T__2);
				setState(510);
				((OwnsExprContext)_localctx).amtExpr = expr(0);
				setState(511);
				match(T__5);
				}
				break;
			case 18:
				{
				_localctx = new PrimExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(513);
				primaryExpr();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(558);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(556);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
					case 1:
						{
						_localctx = new MulModExprContext(new ExprContext(_parentctx, _parentState));
						((MulModExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(516);
						if (!(precpred(_ctx, 24))) throw new FailedPredicateException(this, "precpred(_ctx, 24)");
						setState(517);
						((MulModExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 204800L) != 0)) ) {
							((MulModExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(518);
						((MulModExprContext)_localctx).r = expr(25);
						}
						break;
					case 2:
						{
						_localctx = new AddSubExprContext(new ExprContext(_parentctx, _parentState));
						((AddSubExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(519);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(520);
						((AddSubExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__14 || _la==T__17) ) {
							((AddSubExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(521);
						((AddSubExprContext)_localctx).r = expr(24);
						}
						break;
					case 3:
						{
						_localctx = new InEqExprContext(new ExprContext(_parentctx, _parentState));
						((InEqExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(522);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(523);
						((InEqExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 7864320L) != 0)) ) {
							((InEqExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(524);
						((InEqExprContext)_localctx).r = expr(23);
						}
						break;
					case 4:
						{
						_localctx = new EqExprContext(new ExprContext(_parentctx, _parentState));
						((EqExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(525);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(526);
						((EqExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 125829120L) != 0)) ) {
							((EqExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(527);
						((EqExprContext)_localctx).r = expr(22);
						}
						break;
					case 5:
						{
						_localctx = new LandExprContext(new ExprContext(_parentctx, _parentState));
						((LandExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(528);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(529);
						match(T__27);
						setState(530);
						((LandExprContext)_localctx).r = expr(20);
						}
						break;
					case 6:
						{
						_localctx = new LorExprContext(new ExprContext(_parentctx, _parentState));
						((LorExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(531);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(532);
						match(T__28);
						setState(533);
						((LorExprContext)_localctx).r = expr(19);
						}
						break;
					case 7:
						{
						_localctx = new ImpExprContext(new ExprContext(_parentctx, _parentState));
						((ImpExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(534);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(535);
						match(T__6);
						setState(536);
						((ImpExprContext)_localctx).r = expr(18);
						}
						break;
					case 8:
						{
						_localctx = new WeakUntilExprContext(new ExprContext(_parentctx, _parentState));
						((WeakUntilExprContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(537);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(538);
						match(WEAKUNTIL);
						setState(539);
						((WeakUntilExprContext)_localctx).r = expr(8);
						}
						break;
					case 9:
						{
						_localctx = new AccessExprContext(new ExprContext(_parentctx, _parentState));
						((AccessExprContext)_localctx).arrName = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(540);
						if (!(precpred(_ctx, 30))) throw new FailedPredicateException(this, "precpred(_ctx, 30)");
						setState(541);
						match(T__9);
						setState(542);
						((AccessExprContext)_localctx).index = expr(0);
						setState(543);
						match(T__10);
						}
						break;
					case 10:
						{
						_localctx = new FunCallExprContext(new ExprContext(_parentctx, _parentState));
						((FunCallExprContext)_localctx).funName = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(545);
						if (!(precpred(_ctx, 29))) throw new FailedPredicateException(this, "precpred(_ctx, 29)");
						setState(546);
						match(T__4);
						setState(547);
						((FunCallExprContext)_localctx).ps = argList();
						setState(548);
						match(T__5);
						}
						break;
					case 11:
						{
						_localctx = new MemberAccessExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(550);
						if (!(precpred(_ctx, 28))) throw new FailedPredicateException(this, "precpred(_ctx, 28)");
						setState(551);
						match(T__8);
						setState(552);
						((MemberAccessExprContext)_localctx).identifier = match(ID);
						}
						break;
					case 12:
						{
						_localctx = new EnumStructExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(553);
						if (!(precpred(_ctx, 27))) throw new FailedPredicateException(this, "precpred(_ctx, 27)");
						setState(554);
						match(T__11);
						setState(555);
						((EnumStructExprContext)_localctx).identifier = match(ID);
						}
						break;
					}
					} 
				}
				setState(560);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ArgListContext extends ParserRuleContext {
		public ExprContext expr;
		public List<ExprContext> args = new ArrayList<ExprContext>();
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ArgListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitArgList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgListContext argList() throws RecognitionException {
		ArgListContext _localctx = new ArgListContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_argList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(569);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & 1080863910703169568L) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & 4161531L) != 0)) {
				{
				setState(561);
				((ArgListContext)_localctx).expr = expr(0);
				((ArgListContext)_localctx).args.add(((ArgListContext)_localctx).expr);
				setState(566);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(562);
					match(T__2);
					setState(563);
					((ArgListContext)_localctx).expr = expr(0);
					((ArgListContext)_localctx).args.add(((ArgListContext)_localctx).expr);
					}
					}
					setState(568);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class PrimaryExprContext extends ParserRuleContext {
		public PrimaryExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryExpr; }
	 
		public PrimaryExprContext() { }
		public void copyFrom(PrimaryExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ThisExprContext extends PrimaryExprContext {
		public TerminalNode THIS() { return getToken(SCAppParser.THIS, 0); }
		public ThisExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitThisExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ResultExprContext extends PrimaryExprContext {
		public TerminalNode RESULT() { return getToken(SCAppParser.RESULT, 0); }
		public ResultExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitResultExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BlocknumExprContext extends PrimaryExprContext {
		public TerminalNode BLOCKNUM() { return getToken(SCAppParser.BLOCKNUM, 0); }
		public BlocknumExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitBlocknumExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CallerExprContext extends PrimaryExprContext {
		public TerminalNode CALLER() { return getToken(SCAppParser.CALLER, 0); }
		public CallerExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitCallerExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StringLitExprContext extends PrimaryExprContext {
		public TerminalNode STRING_LIT() { return getToken(SCAppParser.STRING_LIT, 0); }
		public StringLitExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitStringLitExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SelfExprContext extends PrimaryExprContext {
		public TerminalNode SELF() { return getToken(SCAppParser.SELF, 0); }
		public SelfExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitSelfExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class SystimeExprContext extends PrimaryExprContext {
		public TerminalNode SYSTIME() { return getToken(SCAppParser.SYSTIME, 0); }
		public SystimeExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitSystimeExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NumLitExprContext extends PrimaryExprContext {
		public TerminalNode INT_LIT() { return getToken(SCAppParser.INT_LIT, 0); }
		public NumLitExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitNumLitExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BoolLitExprContext extends PrimaryExprContext {
		public TerminalNode BOOL_LIT() { return getToken(SCAppParser.BOOL_LIT, 0); }
		public BoolLitExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitBoolLitExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class AmtExprContext extends PrimaryExprContext {
		public TerminalNode AMT() { return getToken(SCAppParser.AMT, 0); }
		public AmtExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitAmtExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IdExprContext extends PrimaryExprContext {
		public Token id;
		public TerminalNode ID() { return getToken(SCAppParser.ID, 0); }
		public IdExprContext(PrimaryExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryExprContext primaryExpr() throws RecognitionException {
		PrimaryExprContext _localctx = new PrimaryExprContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_primaryExpr);
		try {
			setState(582);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BOOL_LIT:
				_localctx = new BoolLitExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(571);
				match(BOOL_LIT);
				}
				break;
			case INT_LIT:
				_localctx = new NumLitExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(572);
				match(INT_LIT);
				}
				break;
			case STRING_LIT:
				_localctx = new StringLitExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(573);
				match(STRING_LIT);
				}
				break;
			case THIS:
				_localctx = new ThisExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(574);
				match(THIS);
				}
				break;
			case RESULT:
				_localctx = new ResultExprContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(575);
				match(RESULT);
				}
				break;
			case SELF:
				_localctx = new SelfExprContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(576);
				match(SELF);
				}
				break;
			case CALLER:
				_localctx = new CallerExprContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(577);
				match(CALLER);
				}
				break;
			case AMT:
				_localctx = new AmtExprContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(578);
				match(AMT);
				}
				break;
			case BLOCKNUM:
				_localctx = new BlocknumExprContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(579);
				match(BLOCKNUM);
				}
				break;
			case SYSTIME:
				_localctx = new SystimeExprContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(580);
				match(SYSTIME);
				}
				break;
			case ID:
				_localctx = new IdExprContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(581);
				((IdExprContext)_localctx).id = match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class QuantifierContext extends ParserRuleContext {
		public TerminalNode FORALL() { return getToken(SCAppParser.FORALL, 0); }
		public TerminalNode EXISTS() { return getToken(SCAppParser.EXISTS, 0); }
		public QuantifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quantifier; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SCAppVisitor ) return ((SCAppVisitor<? extends T>)visitor).visitQuantifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QuantifierContext quantifier() throws RecognitionException {
		QuantifierContext _localctx = new QuantifierContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_quantifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(584);
			_la = _input.LA(1);
			if ( !(_la==FORALL || _la==EXISTS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 21:
			return typename_sempred((TypenameContext)_localctx, predIndex);
		case 30:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean typename_sempred(TypenameContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 24);
		case 2:
			return precpred(_ctx, 23);
		case 3:
			return precpred(_ctx, 22);
		case 4:
			return precpred(_ctx, 21);
		case 5:
			return precpred(_ctx, 19);
		case 6:
			return precpred(_ctx, 18);
		case 7:
			return precpred(_ctx, 17);
		case 8:
			return precpred(_ctx, 7);
		case 9:
			return precpred(_ctx, 30);
		case 10:
			return precpred(_ctx, 29);
		case 11:
			return precpred(_ctx, 28);
		case 12:
			return precpred(_ctx, 27);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001Z\u024b\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007\u000f"+
		"\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007\u0012"+
		"\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007\u0015"+
		"\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007\u0018"+
		"\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007\u001b"+
		"\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007\u001e"+
		"\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0001\u0000\u0001"+
		"\u0000\u0001\u0000\u0001\u0000\u0004\u0000I\b\u0000\u000b\u0000\f\u0000"+
		"J\u0003\u0000M\b\u0000\u0001\u0000\u0003\u0000P\b\u0000\u0001\u0000\u0003"+
		"\u0000S\b\u0000\u0001\u0000\u0003\u0000V\b\u0000\u0001\u0000\u0004\u0000"+
		"Y\b\u0000\u000b\u0000\f\u0000Z\u0001\u0000\u0001\u0000\u0001\u0001\u0001"+
		"\u0001\u0003\u0001a\b\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001"+
		"\u0002\u0001\u0002\u0001\u0002\u0005\u0002i\b\u0002\n\u0002\f\u0002l\t"+
		"\u0002\u0003\u0002n\b\u0002\u0001\u0002\u0001\u0002\u0001\u0003\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0004\u0001\u0004\u0001\u0004\u0001"+
		"\u0004\u0001\u0004\u0001\u0004\u0005\u0004|\b\u0004\n\u0004\f\u0004\u007f"+
		"\t\u0004\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0004\u0005\u0087\b\u0005\u000b\u0005\f\u0005\u0088\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0003\u0006\u008f\b\u0006\u0001\u0007"+
		"\u0001\u0007\u0001\u0007\u0001\u0007\u0003\u0007\u0095\b\u0007\u0001\b"+
		"\u0001\b\u0001\b\u0004\b\u009a\b\b\u000b\b\f\b\u009b\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0001\t\u0005\t\u00a3\b\t\n\t\f\t\u00a6\t\t\u0001\n\u0001\n"+
		"\u0001\n\u0001\n\u0001\n\u0005\n\u00ad\b\n\n\n\f\n\u00b0\t\n\u0001\u000b"+
		"\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0005\u000b\u00b7\b\u000b"+
		"\n\u000b\f\u000b\u00ba\t\u000b\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f"+
		"\u0001\f\u0001\f\u0001\f\u0001\r\u0001\r\u0001\u000e\u0004\u000e\u00c7"+
		"\b\u000e\u000b\u000e\f\u000e\u00c8\u0001\u000f\u0001\u000f\u0001\u000f"+
		"\u0001\u000f\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0005\u0011\u00d6\b\u0011\n\u0011\f\u0011\u00d9"+
		"\t\u0011\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001"+
		"\u0012\u0001\u0012\u0005\u0012\u00e2\b\u0012\n\u0012\f\u0012\u00e5\t\u0012"+
		"\u0001\u0012\u0001\u0012\u0004\u0012\u00e9\b\u0012\u000b\u0012\f\u0012"+
		"\u00ea\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001"+
		"\u0013\u0005\u0013\u00f3\b\u0013\n\u0013\f\u0013\u00f6\t\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0013\u0005\u0013\u00fb\b\u0013\n\u0013\f\u0013\u00fe"+
		"\t\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0003\u0013\u0103\b\u0013"+
		"\u0001\u0013\u0001\u0013\u0001\u0013\u0005\u0013\u0108\b\u0013\n\u0013"+
		"\f\u0013\u010b\t\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0004\u0013"+
		"\u0110\b\u0013\u000b\u0013\f\u0013\u0111\u0001\u0014\u0001\u0014\u0001"+
		"\u0014\u0001\u0014\u0001\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0001"+
		"\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001"+
		"\u0015\u0003\u0015\u0123\b\u0015\u0001\u0015\u0001\u0015\u0005\u0015\u0127"+
		"\b\u0015\n\u0015\f\u0015\u012a\t\u0015\u0001\u0016\u0001\u0016\u0001\u0017"+
		"\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0003\u0017"+
		"\u0134\b\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0003\u0017\u0139\b"+
		"\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0005\u0017\u013e\b\u0017\n"+
		"\u0017\f\u0017\u0141\t\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0005"+
		"\u0017\u0146\b\u0017\n\u0017\f\u0017\u0149\t\u0017\u0001\u0017\u0003\u0017"+
		"\u014c\b\u0017\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0019"+
		"\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019"+
		"\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019"+
		"\u0001\u0019\u0001\u0019\u0003\u0019\u0161\b\u0019\u0001\u001a\u0001\u001a"+
		"\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0003\u001a\u0169\b\u001a"+
		"\u0001\u001b\u0001\u001b\u0003\u001b\u016d\b\u001b\u0001\u001c\u0001\u001c"+
		"\u0001\u001c\u0005\u001c\u0172\b\u001c\n\u001c\f\u001c\u0175\t\u001c\u0001"+
		"\u001d\u0001\u001d\u0001\u001d\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u01c4"+
		"\b\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u01c9\b\u001e"+
		"\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u01ce\b\u001e\u0001\u001e"+
		"\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e"+
		"\u0001\u001e\u0003\u001e\u01d8\b\u001e\u0001\u001e\u0001\u001e\u0001\u001e"+
		"\u0003\u001e\u01dd\b\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e"+
		"\u01e2\b\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e"+
		"\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e"+
		"\u01ee\b\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u01f3\b"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u01f8\b\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u0203\b\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001"+
		"\u001e\u0001\u001e\u0001\u001e\u0005\u001e\u022d\b\u001e\n\u001e\f\u001e"+
		"\u0230\t\u001e\u0001\u001f\u0001\u001f\u0001\u001f\u0005\u001f\u0235\b"+
		"\u001f\n\u001f\f\u001f\u0238\t\u001f\u0003\u001f\u023a\b\u001f\u0001 "+
		"\u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0003 \u0247\b \u0001!\u0001!\u0001!\u0000\u0002*<\"\u0000\u0002\u0004"+
		"\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018\u001a\u001c\u001e \""+
		"$&(*,.02468:<>@B\u0000\u0007\u0002\u0000CFWW\u0001\u0000&*\u0002\u0000"+
		"\r\r\u0010\u0011\u0002\u0000\u000f\u000f\u0012\u0012\u0001\u0000\u0013"+
		"\u0016\u0001\u0000\u0017\u001a\u0001\u0000RS\u0285\u0000D\u0001\u0000"+
		"\u0000\u0000\u0002`\u0001\u0000\u0000\u0000\u0004b\u0001\u0000\u0000\u0000"+
		"\u0006q\u0001\u0000\u0000\u0000\bu\u0001\u0000\u0000\u0000\n\u0082\u0001"+
		"\u0000\u0000\u0000\f\u008a\u0001\u0000\u0000\u0000\u000e\u0090\u0001\u0000"+
		"\u0000\u0000\u0010\u0099\u0001\u0000\u0000\u0000\u0012\u009d\u0001\u0000"+
		"\u0000\u0000\u0014\u00a7\u0001\u0000\u0000\u0000\u0016\u00b1\u0001\u0000"+
		"\u0000\u0000\u0018\u00bb\u0001\u0000\u0000\u0000\u001a\u00c3\u0001\u0000"+
		"\u0000\u0000\u001c\u00c6\u0001\u0000\u0000\u0000\u001e\u00ca\u0001\u0000"+
		"\u0000\u0000 \u00ce\u0001\u0000\u0000\u0000\"\u00d2\u0001\u0000\u0000"+
		"\u0000$\u00e8\u0001\u0000\u0000\u0000&\u00ec\u0001\u0000\u0000\u0000("+
		"\u0113\u0001\u0000\u0000\u0000*\u0122\u0001\u0000\u0000\u0000,\u012b\u0001"+
		"\u0000\u0000\u0000.\u012d\u0001\u0000\u0000\u00000\u014d\u0001\u0000\u0000"+
		"\u00002\u0160\u0001\u0000\u0000\u00004\u0168\u0001\u0000\u0000\u00006"+
		"\u016c\u0001\u0000\u0000\u00008\u016e\u0001\u0000\u0000\u0000:\u0176\u0001"+
		"\u0000\u0000\u0000<\u0202\u0001\u0000\u0000\u0000>\u0239\u0001\u0000\u0000"+
		"\u0000@\u0246\u0001\u0000\u0000\u0000B\u0248\u0001\u0000\u0000\u0000D"+
		"E\u0005\u001e\u0000\u0000EF\u0005\u0001\u0000\u0000FL\u0005W\u0000\u0000"+
		"GI\u0003\u0002\u0001\u0000HG\u0001\u0000\u0000\u0000IJ\u0001\u0000\u0000"+
		"\u0000JH\u0001\u0000\u0000\u0000JK\u0001\u0000\u0000\u0000KM\u0001\u0000"+
		"\u0000\u0000LH\u0001\u0000\u0000\u0000LM\u0001\u0000\u0000\u0000MO\u0001"+
		"\u0000\u0000\u0000NP\u0003\n\u0005\u0000ON\u0001\u0000\u0000\u0000OP\u0001"+
		"\u0000\u0000\u0000PR\u0001\u0000\u0000\u0000QS\u0003\u001c\u000e\u0000"+
		"RQ\u0001\u0000\u0000\u0000RS\u0001\u0000\u0000\u0000SU\u0001\u0000\u0000"+
		"\u0000TV\u0003 \u0010\u0000UT\u0001\u0000\u0000\u0000UV\u0001\u0000\u0000"+
		"\u0000VX\u0001\u0000\u0000\u0000WY\u0003&\u0013\u0000XW\u0001\u0000\u0000"+
		"\u0000YZ\u0001\u0000\u0000\u0000ZX\u0001\u0000\u0000\u0000Z[\u0001\u0000"+
		"\u0000\u0000[\\\u0001\u0000\u0000\u0000\\]\u0005\u0000\u0000\u0001]\u0001"+
		"\u0001\u0000\u0000\u0000^a\u0003\u0004\u0002\u0000_a\u0003\b\u0004\u0000"+
		"`^\u0001\u0000\u0000\u0000`_\u0001\u0000\u0000\u0000a\u0003\u0001\u0000"+
		"\u0000\u0000bc\u0005,\u0000\u0000cd\u0005W\u0000\u0000dm\u0005\u0002\u0000"+
		"\u0000ej\u0003\u0006\u0003\u0000fg\u0005\u0003\u0000\u0000gi\u0003\u0006"+
		"\u0003\u0000hf\u0001\u0000\u0000\u0000il\u0001\u0000\u0000\u0000jh\u0001"+
		"\u0000\u0000\u0000jk\u0001\u0000\u0000\u0000kn\u0001\u0000\u0000\u0000"+
		"lj\u0001\u0000\u0000\u0000me\u0001\u0000\u0000\u0000mn\u0001\u0000\u0000"+
		"\u0000no\u0001\u0000\u0000\u0000op\u0005\u0004\u0000\u0000p\u0005\u0001"+
		"\u0000\u0000\u0000qr\u0005W\u0000\u0000rs\u0005\u0001\u0000\u0000st\u0003"+
		"*\u0015\u0000t\u0007\u0001\u0000\u0000\u0000uv\u0005-\u0000\u0000vw\u0005"+
		"W\u0000\u0000wx\u0005\u0002\u0000\u0000x}\u0005W\u0000\u0000yz\u0005\u0003"+
		"\u0000\u0000z|\u0005W\u0000\u0000{y\u0001\u0000\u0000\u0000|\u007f\u0001"+
		"\u0000\u0000\u0000}{\u0001\u0000\u0000\u0000}~\u0001\u0000\u0000\u0000"+
		"~\u0080\u0001\u0000\u0000\u0000\u007f}\u0001\u0000\u0000\u0000\u0080\u0081"+
		"\u0005\u0004\u0000\u0000\u0081\t\u0001\u0000\u0000\u0000\u0082\u0083\u0005"+
		"@\u0000\u0000\u0083\u0086\u0005\u0001\u0000\u0000\u0084\u0087\u0003\f"+
		"\u0006\u0000\u0085\u0087\u0003\u000e\u0007\u0000\u0086\u0084\u0001\u0000"+
		"\u0000\u0000\u0086\u0085\u0001\u0000\u0000\u0000\u0087\u0088\u0001\u0000"+
		"\u0000\u0000\u0088\u0086\u0001\u0000\u0000\u0000\u0088\u0089\u0001\u0000"+
		"\u0000\u0000\u0089\u000b\u0001\u0000\u0000\u0000\u008a\u008b\u0005A\u0000"+
		"\u0000\u008b\u008e\u0005W\u0000\u0000\u008c\u008d\u0005\u0001\u0000\u0000"+
		"\u008d\u008f\u0003\u0010\b\u0000\u008e\u008c\u0001\u0000\u0000\u0000\u008e"+
		"\u008f\u0001\u0000\u0000\u0000\u008f\r\u0001\u0000\u0000\u0000\u0090\u0091"+
		"\u0005*\u0000\u0000\u0091\u0094\u0005W\u0000\u0000\u0092\u0093\u0005\u0001"+
		"\u0000\u0000\u0093\u0095\u0003\u0010\b\u0000\u0094\u0092\u0001\u0000\u0000"+
		"\u0000\u0094\u0095\u0001\u0000\u0000\u0000\u0095\u000f\u0001\u0000\u0000"+
		"\u0000\u0096\u009a\u0003\u0014\n\u0000\u0097\u009a\u0003\u0012\t\u0000"+
		"\u0098\u009a\u0003\u0016\u000b\u0000\u0099\u0096\u0001\u0000\u0000\u0000"+
		"\u0099\u0097\u0001\u0000\u0000\u0000\u0099\u0098\u0001\u0000\u0000\u0000"+
		"\u009a\u009b\u0001\u0000\u0000\u0000\u009b\u0099\u0001\u0000\u0000\u0000"+
		"\u009b\u009c\u0001\u0000\u0000\u0000\u009c\u0011\u0001\u0000\u0000\u0000"+
		"\u009d\u009e\u0005<\u0000\u0000\u009e\u009f\u0005\u0001\u0000\u0000\u009f"+
		"\u00a4\u00030\u0018\u0000\u00a0\u00a1\u0005\u0003\u0000\u0000\u00a1\u00a3"+
		"\u00030\u0018\u0000\u00a2\u00a0\u0001\u0000\u0000\u0000\u00a3\u00a6\u0001"+
		"\u0000\u0000\u0000\u00a4\u00a2\u0001\u0000\u0000\u0000\u00a4\u00a5\u0001"+
		"\u0000\u0000\u0000\u00a5\u0013\u0001\u0000\u0000\u0000\u00a6\u00a4\u0001"+
		"\u0000\u0000\u0000\u00a7\u00a8\u0005>\u0000\u0000\u00a8\u00a9\u0005\u0001"+
		"\u0000\u0000\u00a9\u00ae\u00032\u0019\u0000\u00aa\u00ab\u0005\u0003\u0000"+
		"\u0000\u00ab\u00ad\u00032\u0019\u0000\u00ac\u00aa\u0001\u0000\u0000\u0000"+
		"\u00ad\u00b0\u0001\u0000\u0000\u0000\u00ae\u00ac\u0001\u0000\u0000\u0000"+
		"\u00ae\u00af\u0001\u0000\u0000\u0000\u00af\u0015\u0001\u0000\u0000\u0000"+
		"\u00b0\u00ae\u0001\u0000\u0000\u0000\u00b1\u00b2\u0005?\u0000\u0000\u00b2"+
		"\u00b3\u0005\u0001\u0000\u0000\u00b3\u00b8\u0003\u0018\f\u0000\u00b4\u00b5"+
		"\u0005\u0003\u0000\u0000\u00b5\u00b7\u0003\u0018\f\u0000\u00b6\u00b4\u0001"+
		"\u0000\u0000\u0000\u00b7\u00ba\u0001\u0000\u0000\u0000\u00b8\u00b6\u0001"+
		"\u0000\u0000\u0000\u00b8\u00b9\u0001\u0000\u0000\u0000\u00b9\u0017\u0001"+
		"\u0000\u0000\u0000\u00ba\u00b8\u0001\u0000\u0000\u0000\u00bb\u00bc\u0005"+
		"\u0005\u0000\u0000\u00bc\u00bd\u0003\u001a\r\u0000\u00bd\u00be\u0005\u0003"+
		"\u0000\u0000\u00be\u00bf\u0003\u001a\r\u0000\u00bf\u00c0\u0005\u0003\u0000"+
		"\u0000\u00c0\u00c1\u0003<\u001e\u0000\u00c1\u00c2\u0005\u0006\u0000\u0000"+
		"\u00c2\u0019\u0001\u0000\u0000\u0000\u00c3\u00c4\u0007\u0000\u0000\u0000"+
		"\u00c4\u001b\u0001\u0000\u0000\u0000\u00c5\u00c7\u0003\u001e\u000f\u0000"+
		"\u00c6\u00c5\u0001\u0000\u0000\u0000\u00c7\u00c8\u0001\u0000\u0000\u0000"+
		"\u00c8\u00c6\u0001\u0000\u0000\u0000\u00c8\u00c9\u0001\u0000\u0000\u0000"+
		"\u00c9\u001d\u0001\u0000\u0000\u0000\u00ca\u00cb\u00056\u0000\u0000\u00cb"+
		"\u00cc\u0005\u0001\u0000\u0000\u00cc\u00cd\u0003<\u001e\u0000\u00cd\u001f"+
		"\u0001\u0000\u0000\u0000\u00ce\u00cf\u0005%\u0000\u0000\u00cf\u00d0\u0005"+
		"\u0001\u0000\u0000\u00d0\u00d1\u0003\"\u0011\u0000\u00d1!\u0001\u0000"+
		"\u0000\u0000\u00d2\u00d7\u0003$\u0012\u0000\u00d3\u00d4\u0005\u0003\u0000"+
		"\u0000\u00d4\u00d6\u0003$\u0012\u0000\u00d5\u00d3\u0001\u0000\u0000\u0000"+
		"\u00d6\u00d9\u0001\u0000\u0000\u0000\u00d7\u00d5\u0001\u0000\u0000\u0000"+
		"\u00d7\u00d8\u0001\u0000\u0000\u0000\u00d8#\u0001\u0000\u0000\u0000\u00d9"+
		"\u00d7\u0001\u0000\u0000\u0000\u00da\u00db\u0005W\u0000\u0000\u00db\u00dc"+
		"\u0005\u0001\u0000\u0000\u00dc\u00dd\u0005W\u0000\u0000\u00dd\u00de\u0005"+
		"\u0005\u0000\u0000\u00de\u00e3\u0003<\u001e\u0000\u00df\u00e0\u0005\u0003"+
		"\u0000\u0000\u00e0\u00e2\u0003<\u001e\u0000\u00e1\u00df\u0001\u0000\u0000"+
		"\u0000\u00e2\u00e5\u0001\u0000\u0000\u0000\u00e3\u00e1\u0001\u0000\u0000"+
		"\u0000\u00e3\u00e4\u0001\u0000\u0000\u0000\u00e4\u00e6\u0001\u0000\u0000"+
		"\u0000\u00e5\u00e3\u0001\u0000\u0000\u0000\u00e6\u00e7\u0005\u0006\u0000"+
		"\u0000\u00e7\u00e9\u0001\u0000\u0000\u0000\u00e8\u00da\u0001\u0000\u0000"+
		"\u0000\u00e9\u00ea\u0001\u0000\u0000\u0000\u00ea\u00e8\u0001\u0000\u0000"+
		"\u0000\u00ea\u00eb\u0001\u0000\u0000\u0000\u00eb%\u0001\u0000\u0000\u0000"+
		"\u00ec\u00ed\u0005\u001f\u0000\u0000\u00ed\u00ee\u0005W\u0000\u0000\u00ee"+
		"\u00ef\u0005\u0001\u0000\u0000\u00ef\u00f0\u0005 \u0000\u0000\u00f0\u00f4"+
		"\u0005\u0001\u0000\u0000\u00f1\u00f3\u0003(\u0014\u0000\u00f2\u00f1\u0001"+
		"\u0000\u0000\u0000\u00f3\u00f6\u0001\u0000\u0000\u0000\u00f4\u00f2\u0001"+
		"\u0000\u0000\u0000\u00f4\u00f5\u0001\u0000\u0000\u0000\u00f5\u00fc\u0001"+
		"\u0000\u0000\u0000\u00f6\u00f4\u0001\u0000\u0000\u0000\u00f7\u00f8\u0005"+
		"#\u0000\u0000\u00f8\u00f9\u0005\u0001\u0000\u0000\u00f9\u00fb\u0003<\u001e"+
		"\u0000\u00fa\u00f7\u0001\u0000\u0000\u0000\u00fb\u00fe\u0001\u0000\u0000"+
		"\u0000\u00fc\u00fa\u0001\u0000\u0000\u0000\u00fc\u00fd\u0001\u0000\u0000"+
		"\u0000\u00fd\u0102\u0001\u0000\u0000\u0000\u00fe\u00fc\u0001\u0000\u0000"+
		"\u0000\u00ff\u0100\u00051\u0000\u0000\u0100\u0101\u0005\u0001\u0000\u0000"+
		"\u0101\u0103\u00038\u001c\u0000\u0102\u00ff\u0001\u0000\u0000\u0000\u0102"+
		"\u0103\u0001\u0000\u0000\u0000\u0103\u0109\u0001\u0000\u0000\u0000\u0104"+
		"\u0105\u0005\"\u0000\u0000\u0105\u0106\u0005\u0001\u0000\u0000\u0106\u0108"+
		"\u0003<\u001e\u0000\u0107\u0104\u0001\u0000\u0000\u0000\u0108\u010b\u0001"+
		"\u0000\u0000\u0000\u0109\u0107\u0001\u0000\u0000\u0000\u0109\u010a\u0001"+
		"\u0000\u0000\u0000\u010a\u010c\u0001\u0000\u0000\u0000\u010b\u0109\u0001"+
		"\u0000\u0000\u0000\u010c\u010d\u0005!\u0000\u0000\u010d\u010f\u0005\u0001"+
		"\u0000\u0000\u010e\u0110\u0003.\u0017\u0000\u010f\u010e\u0001\u0000\u0000"+
		"\u0000\u0110\u0111\u0001\u0000\u0000\u0000\u0111\u010f\u0001\u0000\u0000"+
		"\u0000\u0111\u0112\u0001\u0000\u0000\u0000\u0112\'\u0001\u0000\u0000\u0000"+
		"\u0113\u0114\u0005.\u0000\u0000\u0114\u0115\u0005W\u0000\u0000\u0115\u0116"+
		"\u0005\u0001\u0000\u0000\u0116\u0117\u0003*\u0015\u0000\u0117)\u0001\u0000"+
		"\u0000\u0000\u0118\u0119\u0006\u0015\uffff\uffff\u0000\u0119\u011a\u0005"+
		"+\u0000\u0000\u011a\u011b\u0005\u0005\u0000\u0000\u011b\u011c\u0003*\u0015"+
		"\u0000\u011c\u011d\u0005\u0007\u0000\u0000\u011d\u011e\u0003*\u0015\u0000"+
		"\u011e\u011f\u0005\u0006\u0000\u0000\u011f\u0123\u0001\u0000\u0000\u0000"+
		"\u0120\u0123\u0003,\u0016\u0000\u0121\u0123\u0005W\u0000\u0000\u0122\u0118"+
		"\u0001\u0000\u0000\u0000\u0122\u0120\u0001\u0000\u0000\u0000\u0122\u0121"+
		"\u0001\u0000\u0000\u0000\u0123\u0128\u0001\u0000\u0000\u0000\u0124\u0125"+
		"\n\u0003\u0000\u0000\u0125\u0127\u0005\b\u0000\u0000\u0126\u0124\u0001"+
		"\u0000\u0000\u0000\u0127\u012a\u0001\u0000\u0000\u0000\u0128\u0126\u0001"+
		"\u0000\u0000\u0000\u0128\u0129\u0001\u0000\u0000\u0000\u0129+\u0001\u0000"+
		"\u0000\u0000\u012a\u0128\u0001\u0000\u0000\u0000\u012b\u012c\u0007\u0001"+
		"\u0000\u0000\u012c-\u0001\u0000\u0000\u0000\u012d\u012e\u00050\u0000\u0000"+
		"\u012e\u012f\u0005W\u0000\u0000\u012f\u0133\u0005\u0001\u0000\u0000\u0130"+
		"\u0131\u00052\u0000\u0000\u0131\u0132\u0005\u0001\u0000\u0000\u0132\u0134"+
		"\u00038\u001c\u0000\u0133\u0130\u0001\u0000\u0000\u0000\u0133\u0134\u0001"+
		"\u0000\u0000\u0000\u0134\u0138\u0001\u0000\u0000\u0000\u0135\u0136\u0005"+
		"3\u0000\u0000\u0136\u0137\u0005\u0001\u0000\u0000\u0137\u0139\u0003*\u0015"+
		"\u0000\u0138\u0135\u0001\u0000\u0000\u0000\u0138\u0139\u0001\u0000\u0000"+
		"\u0000\u0139\u013f\u0001\u0000\u0000\u0000\u013a\u013b\u00054\u0000\u0000"+
		"\u013b\u013c\u0005\u0001\u0000\u0000\u013c\u013e\u0003<\u001e\u0000\u013d"+
		"\u013a\u0001\u0000\u0000\u0000\u013e\u0141\u0001\u0000\u0000\u0000\u013f"+
		"\u013d\u0001\u0000\u0000\u0000\u013f\u0140\u0001\u0000\u0000\u0000\u0140"+
		"\u0147\u0001\u0000\u0000\u0000\u0141\u013f\u0001\u0000\u0000\u0000\u0142"+
		"\u0143\u00055\u0000\u0000\u0143\u0144\u0005\u0001\u0000\u0000\u0144\u0146"+
		"\u0003<\u001e\u0000\u0145\u0142\u0001\u0000\u0000\u0000\u0146\u0149\u0001"+
		"\u0000\u0000\u0000\u0147\u0145\u0001\u0000\u0000\u0000\u0147\u0148\u0001"+
		"\u0000\u0000\u0000\u0148\u014b\u0001\u0000\u0000\u0000\u0149\u0147\u0001"+
		"\u0000\u0000\u0000\u014a\u014c\u0003\u0010\b\u0000\u014b\u014a\u0001\u0000"+
		"\u0000\u0000\u014b\u014c\u0001\u0000\u0000\u0000\u014c/\u0001\u0000\u0000"+
		"\u0000\u014d\u014e\u0005W\u0000\u0000\u014e\u014f\u0005\t\u0000\u0000"+
		"\u014f\u0150\u0005W\u0000\u0000\u01501\u0001\u0000\u0000\u0000\u0151\u0152"+
		"\u0005W\u0000\u0000\u0152\u0153\u0005\t\u0000\u0000\u0153\u0161\u0005"+
		"W\u0000\u0000\u0154\u0155\u0005W\u0000\u0000\u0155\u0156\u0005\t\u0000"+
		"\u0000\u0156\u0157\u0005W\u0000\u0000\u0157\u0158\u0005\n\u0000\u0000"+
		"\u0158\u0159\u00034\u001a\u0000\u0159\u015a\u0005\u000b\u0000\u0000\u015a"+
		"\u0161\u0001\u0000\u0000\u0000\u015b\u015c\u0005W\u0000\u0000\u015c\u015d"+
		"\u0005\t\u0000\u0000\u015d\u015e\u0005W\u0000\u0000\u015e\u015f\u0005"+
		"\f\u0000\u0000\u015f\u0161\u00036\u001b\u0000\u0160\u0151\u0001\u0000"+
		"\u0000\u0000\u0160\u0154\u0001\u0000\u0000\u0000\u0160\u015b\u0001\u0000"+
		"\u0000\u0000\u01613\u0001\u0000\u0000\u0000\u0162\u0169\u0005\r\u0000"+
		"\u0000\u0163\u0164\u0003<\u001e\u0000\u0164\u0165\u0005\u000e\u0000\u0000"+
		"\u0165\u0166\u0003<\u001e\u0000\u0166\u0169\u0001\u0000\u0000\u0000\u0167"+
		"\u0169\u0003<\u001e\u0000\u0168\u0162\u0001\u0000\u0000\u0000\u0168\u0163"+
		"\u0001\u0000\u0000\u0000\u0168\u0167\u0001\u0000\u0000\u0000\u01695\u0001"+
		"\u0000\u0000\u0000\u016a\u016d\u0005\r\u0000\u0000\u016b\u016d\u0005W"+
		"\u0000\u0000\u016c\u016a\u0001\u0000\u0000\u0000\u016c\u016b\u0001\u0000"+
		"\u0000\u0000\u016d7\u0001\u0000\u0000\u0000\u016e\u0173\u0003:\u001d\u0000"+
		"\u016f\u0170\u0005\u0003\u0000\u0000\u0170\u0172\u0003:\u001d\u0000\u0171"+
		"\u016f\u0001\u0000\u0000\u0000\u0172\u0175\u0001\u0000\u0000\u0000\u0173"+
		"\u0171\u0001\u0000\u0000\u0000\u0173\u0174\u0001\u0000\u0000\u0000\u0174"+
		"9\u0001\u0000\u0000\u0000\u0175\u0173\u0001\u0000\u0000\u0000\u0176\u0177"+
		"\u0003*\u0015\u0000\u0177\u0178\u0005W\u0000\u0000\u0178;\u0001\u0000"+
		"\u0000\u0000\u0179\u017a\u0006\u001e\uffff\uffff\u0000\u017a\u017b\u0005"+
		"\u0005\u0000\u0000\u017b\u017c\u0003<\u001e\u0000\u017c\u017d\u0005\u0006"+
		"\u0000\u0000\u017d\u0203\u0001\u0000\u0000\u0000\u017e\u017f\u0005\u000f"+
		"\u0000\u0000\u017f\u0203\u0003<\u001e\u0019\u0180\u0181\u0005\u001b\u0000"+
		"\u0000\u0181\u0203\u0003<\u001e\u0014\u0182\u0183\u0003B!\u0000\u0183"+
		"\u0184\u0005\u0005\u0000\u0000\u0184\u0185\u0005W\u0000\u0000\u0185\u0186"+
		"\u0005\u0001\u0000\u0000\u0186\u0187\u0003*\u0015\u0000\u0187\u0188\u0005"+
		"\u0006\u0000\u0000\u0188\u0189\u0005\u0001\u0000\u0000\u0189\u018a\u0003"+
		"<\u001e\u0010\u018a\u0203\u0001\u0000\u0000\u0000\u018b\u018c\u0003B!"+
		"\u0000\u018c\u018d\u0005\u0005\u0000\u0000\u018d\u018e\u0005W\u0000\u0000"+
		"\u018e\u018f\u0005Q\u0000\u0000\u018f\u0190\u0003<\u001e\u0000\u0190\u0191"+
		"\u0005\u0006\u0000\u0000\u0191\u0192\u0005\u0001\u0000\u0000\u0192\u0193"+
		"\u0003<\u001e\u000f\u0193\u0203\u0001\u0000\u0000\u0000\u0194\u0195\u0005"+
		"M\u0000\u0000\u0195\u0196\u0005\u0005\u0000\u0000\u0196\u0197\u0003<\u001e"+
		"\u0000\u0197\u0198\u0005\u0006\u0000\u0000\u0198\u0203\u0001\u0000\u0000"+
		"\u0000\u0199\u019a\u0005O\u0000\u0000\u019a\u019b\u0005\u0005\u0000\u0000"+
		"\u019b\u019c\u0003<\u001e\u0000\u019c\u019d\u0005\u0006\u0000\u0000\u019d"+
		"\u0203\u0001\u0000\u0000\u0000\u019e\u019f\u0005N\u0000\u0000\u019f\u01a0"+
		"\u0005\u0005\u0000\u0000\u01a0\u01a1\u0003<\u001e\u0000\u01a1\u01a2\u0005"+
		"\u0006\u0000\u0000\u01a2\u0203\u0001\u0000\u0000\u0000\u01a3\u01a4\u0005"+
		"P\u0000\u0000\u01a4\u01a5\u0005\u0005\u0000\u0000\u01a5\u01a6\u0003<\u001e"+
		"\u0000\u01a6\u01a7\u0005\u0006\u0000\u0000\u01a7\u0203\u0001\u0000\u0000"+
		"\u0000\u01a8\u01a9\u0005L\u0000\u0000\u01a9\u01aa\u0005\u0005\u0000\u0000"+
		"\u01aa\u01ab\u0003<\u001e\u0000\u01ab\u01ac\u0005\u0006\u0000\u0000\u01ac"+
		"\u0203\u0001\u0000\u0000\u0000\u01ad\u01ae\u0005J\u0000\u0000\u01ae\u01af"+
		"\u0005\u0005\u0000\u0000\u01af\u01b0\u0003<\u001e\u0000\u01b0\u01b1\u0005"+
		"\u0006\u0000\u0000\u01b1\u0203\u0001\u0000\u0000\u0000\u01b2\u01b3\u0005"+
		"K\u0000\u0000\u01b3\u01b4\u0005\u0005\u0000\u0000\u01b4\u01b5\u0003<\u001e"+
		"\u0000\u01b5\u01b6\u0005\u0003\u0000\u0000\u01b6\u01b7\u0003<\u001e\u0000"+
		"\u01b7\u01b8\u0005\u0003\u0000\u0000\u01b8\u01b9\u0003<\u001e\u0000\u01b9"+
		"\u01ba\u0005\u0006\u0000\u0000\u01ba\u0203\u0001\u0000\u0000\u0000\u01bb"+
		"\u01bc\u00058\u0000\u0000\u01bc\u01bd\u0005\u0005\u0000\u0000\u01bd\u01be"+
		"\u0003<\u001e\u0000\u01be\u01bf\u0005\u0006\u0000\u0000\u01bf\u0203\u0001"+
		"\u0000\u0000\u0000\u01c0\u01c1\u00059\u0000\u0000\u01c1\u01c3\u0005\u0002"+
		"\u0000\u0000\u01c2\u01c4\u0003<\u001e\u0000\u01c3\u01c2\u0001\u0000\u0000"+
		"\u0000\u01c3\u01c4\u0001\u0000\u0000\u0000\u01c4\u01c5\u0001\u0000\u0000"+
		"\u0000\u01c5\u01c6\u0005\u0004\u0000\u0000\u01c6\u01c8\u0005\u0002\u0000"+
		"\u0000\u01c7\u01c9\u0003<\u001e\u0000\u01c8\u01c7\u0001\u0000\u0000\u0000"+
		"\u01c8\u01c9\u0001\u0000\u0000\u0000\u01c9\u01ca\u0001\u0000\u0000\u0000"+
		"\u01ca\u01cb\u0005\u0004\u0000\u0000\u01cb\u01cd\u0005\u0002\u0000\u0000"+
		"\u01cc\u01ce\u0003<\u001e\u0000\u01cd\u01cc\u0001\u0000\u0000\u0000\u01cd"+
		"\u01ce\u0001\u0000\u0000\u0000\u01ce\u01cf\u0001\u0000\u0000\u0000\u01cf"+
		"\u01d0\u0005\u0004\u0000\u0000\u01d0\u01d1\u0005\u0005\u0000\u0000\u01d1"+
		"\u01d2\u0003<\u001e\u0000\u01d2\u01d3\u0005\u0006\u0000\u0000\u01d3\u0203"+
		"\u0001\u0000\u0000\u0000\u01d4\u01d5\u0005:\u0000\u0000\u01d5\u01d7\u0005"+
		"\u0002\u0000\u0000\u01d6\u01d8\u0003<\u001e\u0000\u01d7\u01d6\u0001\u0000"+
		"\u0000\u0000\u01d7\u01d8\u0001\u0000\u0000\u0000\u01d8\u01d9\u0001\u0000"+
		"\u0000\u0000\u01d9\u01da\u0005\u0004\u0000\u0000\u01da\u01dc\u0005\u0002"+
		"\u0000\u0000\u01db\u01dd\u0003<\u001e\u0000\u01dc\u01db\u0001\u0000\u0000"+
		"\u0000\u01dc\u01dd\u0001\u0000\u0000\u0000\u01dd\u01de\u0001\u0000\u0000"+
		"\u0000\u01de\u01df\u0005\u0004\u0000\u0000\u01df\u01e1\u0005\u0002\u0000"+
		"\u0000\u01e0\u01e2\u0003<\u001e\u0000\u01e1\u01e0\u0001\u0000\u0000\u0000"+
		"\u01e1\u01e2\u0001\u0000\u0000\u0000\u01e2\u01e3\u0001\u0000\u0000\u0000"+
		"\u01e3\u01e4\u0005\u0004\u0000\u0000\u01e4\u01e5\u0005\u0005\u0000\u0000"+
		"\u01e5\u01e6\u0003<\u001e\u0000\u01e6\u01e7\u0005\u0006\u0000\u0000\u01e7"+
		"\u0203\u0001\u0000\u0000\u0000\u01e8\u01e9\u0005W\u0000\u0000\u01e9\u01ea"+
		"\u0005\t\u0000\u0000\u01ea\u01eb\u0005W\u0000\u0000\u01eb\u01ed\u0005"+
		"\u0002\u0000\u0000\u01ec\u01ee\u0003<\u001e\u0000\u01ed\u01ec\u0001\u0000"+
		"\u0000\u0000\u01ed\u01ee\u0001\u0000\u0000\u0000\u01ee\u01ef\u0001\u0000"+
		"\u0000\u0000\u01ef\u01f0\u0005\u0004\u0000\u0000\u01f0\u01f2\u0005\u0002"+
		"\u0000\u0000\u01f1\u01f3\u0003<\u001e\u0000\u01f2\u01f1\u0001\u0000\u0000"+
		"\u0000\u01f2\u01f3\u0001\u0000\u0000\u0000\u01f3\u01f4\u0001\u0000\u0000"+
		"\u0000\u01f4\u01f5\u0005\u0004\u0000\u0000\u01f5\u01f7\u0005\u0002\u0000"+
		"\u0000\u01f6\u01f8\u0003<\u001e\u0000\u01f7\u01f6\u0001\u0000\u0000\u0000"+
		"\u01f7\u01f8\u0001\u0000\u0000\u0000\u01f8\u01f9\u0001\u0000\u0000\u0000"+
		"\u01f9\u0203\u0005\u0004\u0000\u0000\u01fa\u01fb\u0005;\u0000\u0000\u01fb"+
		"\u01fc\u0005\u0005\u0000\u0000\u01fc\u01fd\u0003<\u001e\u0000\u01fd\u01fe"+
		"\u0005\u0003\u0000\u0000\u01fe\u01ff\u0003<\u001e\u0000\u01ff\u0200\u0005"+
		"\u0006\u0000\u0000\u0200\u0203\u0001\u0000\u0000\u0000\u0201\u0203\u0003"+
		"@ \u0000\u0202\u0179\u0001\u0000\u0000\u0000\u0202\u017e\u0001\u0000\u0000"+
		"\u0000\u0202\u0180\u0001\u0000\u0000\u0000\u0202\u0182\u0001\u0000\u0000"+
		"\u0000\u0202\u018b\u0001\u0000\u0000\u0000\u0202\u0194\u0001\u0000\u0000"+
		"\u0000\u0202\u0199\u0001\u0000\u0000\u0000\u0202\u019e\u0001\u0000\u0000"+
		"\u0000\u0202\u01a3\u0001\u0000\u0000\u0000\u0202\u01a8\u0001\u0000\u0000"+
		"\u0000\u0202\u01ad\u0001\u0000\u0000\u0000\u0202\u01b2\u0001\u0000\u0000"+
		"\u0000\u0202\u01bb\u0001\u0000\u0000\u0000\u0202\u01c0\u0001\u0000\u0000"+
		"\u0000\u0202\u01d4\u0001\u0000\u0000\u0000\u0202\u01e8\u0001\u0000\u0000"+
		"\u0000\u0202\u01fa\u0001\u0000\u0000\u0000\u0202\u0201\u0001\u0000\u0000"+
		"\u0000\u0203\u022e\u0001\u0000\u0000\u0000\u0204\u0205\n\u0018\u0000\u0000"+
		"\u0205\u0206\u0007\u0002\u0000\u0000\u0206\u022d\u0003<\u001e\u0019\u0207"+
		"\u0208\n\u0017\u0000\u0000\u0208\u0209\u0007\u0003\u0000\u0000\u0209\u022d"+
		"\u0003<\u001e\u0018\u020a\u020b\n\u0016\u0000\u0000\u020b\u020c\u0007"+
		"\u0004\u0000\u0000\u020c\u022d\u0003<\u001e\u0017\u020d\u020e\n\u0015"+
		"\u0000\u0000\u020e\u020f\u0007\u0005\u0000\u0000\u020f\u022d\u0003<\u001e"+
		"\u0016\u0210\u0211\n\u0013\u0000\u0000\u0211\u0212\u0005\u001c\u0000\u0000"+
		"\u0212\u022d\u0003<\u001e\u0014\u0213\u0214\n\u0012\u0000\u0000\u0214"+
		"\u0215\u0005\u001d\u0000\u0000\u0215\u022d\u0003<\u001e\u0013\u0216\u0217"+
		"\n\u0011\u0000\u0000\u0217\u0218\u0005\u0007\u0000\u0000\u0218\u022d\u0003"+
		"<\u001e\u0012\u0219\u021a\n\u0007\u0000\u0000\u021a\u021b\u00057\u0000"+
		"\u0000\u021b\u022d\u0003<\u001e\b\u021c\u021d\n\u001e\u0000\u0000\u021d"+
		"\u021e\u0005\n\u0000\u0000\u021e\u021f\u0003<\u001e\u0000\u021f\u0220"+
		"\u0005\u000b\u0000\u0000\u0220\u022d\u0001\u0000\u0000\u0000\u0221\u0222"+
		"\n\u001d\u0000\u0000\u0222\u0223\u0005\u0005\u0000\u0000\u0223\u0224\u0003"+
		">\u001f\u0000\u0224\u0225\u0005\u0006\u0000\u0000\u0225\u022d\u0001\u0000"+
		"\u0000\u0000\u0226\u0227\n\u001c\u0000\u0000\u0227\u0228\u0005\t\u0000"+
		"\u0000\u0228\u022d\u0005W\u0000\u0000\u0229\u022a\n\u001b\u0000\u0000"+
		"\u022a\u022b\u0005\f\u0000\u0000\u022b\u022d\u0005W\u0000\u0000\u022c"+
		"\u0204\u0001\u0000\u0000\u0000\u022c\u0207\u0001\u0000\u0000\u0000\u022c"+
		"\u020a\u0001\u0000\u0000\u0000\u022c\u020d\u0001\u0000\u0000\u0000\u022c"+
		"\u0210\u0001\u0000\u0000\u0000\u022c\u0213\u0001\u0000\u0000\u0000\u022c"+
		"\u0216\u0001\u0000\u0000\u0000\u022c\u0219\u0001\u0000\u0000\u0000\u022c"+
		"\u021c\u0001\u0000\u0000\u0000\u022c\u0221\u0001\u0000\u0000\u0000\u022c"+
		"\u0226\u0001\u0000\u0000\u0000\u022c\u0229\u0001\u0000\u0000\u0000\u022d"+
		"\u0230\u0001\u0000\u0000\u0000\u022e\u022c\u0001\u0000\u0000\u0000\u022e"+
		"\u022f\u0001\u0000\u0000\u0000\u022f=\u0001\u0000\u0000\u0000\u0230\u022e"+
		"\u0001\u0000\u0000\u0000\u0231\u0236\u0003<\u001e\u0000\u0232\u0233\u0005"+
		"\u0003\u0000\u0000\u0233\u0235\u0003<\u001e\u0000\u0234\u0232\u0001\u0000"+
		"\u0000\u0000\u0235\u0238\u0001\u0000\u0000\u0000\u0236\u0234\u0001\u0000"+
		"\u0000\u0000\u0236\u0237\u0001\u0000\u0000\u0000\u0237\u023a\u0001\u0000"+
		"\u0000\u0000\u0238\u0236\u0001\u0000\u0000\u0000\u0239\u0231\u0001\u0000"+
		"\u0000\u0000\u0239\u023a\u0001\u0000\u0000\u0000\u023a?\u0001\u0000\u0000"+
		"\u0000\u023b\u0247\u0005T\u0000\u0000\u023c\u0247\u0005U\u0000\u0000\u023d"+
		"\u0247\u0005V\u0000\u0000\u023e\u0247\u0005C\u0000\u0000\u023f\u0247\u0005"+
		"B\u0000\u0000\u0240\u0247\u0005E\u0000\u0000\u0241\u0247\u0005F\u0000"+
		"\u0000\u0242\u0247\u0005I\u0000\u0000\u0243\u0247\u0005G\u0000\u0000\u0244"+
		"\u0247\u0005H\u0000\u0000\u0245\u0247\u0005W\u0000\u0000\u0246\u023b\u0001"+
		"\u0000\u0000\u0000\u0246\u023c\u0001\u0000\u0000\u0000\u0246\u023d\u0001"+
		"\u0000\u0000\u0000\u0246\u023e\u0001\u0000\u0000\u0000\u0246\u023f\u0001"+
		"\u0000\u0000\u0000\u0246\u0240\u0001\u0000\u0000\u0000\u0246\u0241\u0001"+
		"\u0000\u0000\u0000\u0246\u0242\u0001\u0000\u0000\u0000\u0246\u0243\u0001"+
		"\u0000\u0000\u0000\u0246\u0244\u0001\u0000\u0000\u0000\u0246\u0245\u0001"+
		"\u0000\u0000\u0000\u0247A\u0001\u0000\u0000\u0000\u0248\u0249\u0007\u0006"+
		"\u0000\u0000\u0249C\u0001\u0000\u0000\u00006JLORUZ`jm}\u0086\u0088\u008e"+
		"\u0094\u0099\u009b\u00a4\u00ae\u00b8\u00c8\u00d7\u00e3\u00ea\u00f4\u00fc"+
		"\u0102\u0109\u0111\u0122\u0128\u0133\u0138\u013f\u0147\u014b\u0160\u0168"+
		"\u016c\u0173\u01c3\u01c8\u01cd\u01d7\u01dc\u01e1\u01ed\u01f2\u01f7\u0202"+
		"\u022c\u022e\u0236\u0239\u0246";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}