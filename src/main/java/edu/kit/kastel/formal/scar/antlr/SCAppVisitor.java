// Generated from /home/jonas/projects/Scar/src/main/antlr/edu/kit/kastel/formal/scar/SCApp.g4 by ANTLR 4.13.2
package edu.kit.kastel.formal.scar.antlr;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SCAppParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SCAppVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SCAppParser#scapp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScapp(SCAppParser.ScappContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#userDefType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUserDefType(SCAppParser.UserDefTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#structDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructDecl(SCAppParser.StructDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#structVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructVar(SCAppParser.StructVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#enumDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumDecl(SCAppParser.EnumDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#roleset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoleset(SCAppParser.RolesetContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#role}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRole(SCAppParser.RoleContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#account}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccount(SCAppParser.AccountContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#capabilities}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCapabilities(SCAppParser.CapabilitiesContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#callCapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallCapa(SCAppParser.CallCapaContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#modifyCapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModifyCapa(SCAppParser.ModifyCapaContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#transferCapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTransferCapa(SCAppParser.TransferCapaContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#transfer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTransfer(SCAppParser.TransferContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#accountSet}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccountSet(SCAppParser.AccountSetContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#liveSpec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiveSpec(SCAppParser.LiveSpecContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#lProp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLProp(SCAppParser.LPropContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#appInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAppInit(SCAppParser.AppInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#conList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConList(SCAppParser.ConListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#conInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConInit(SCAppParser.ConInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#contract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContract(SCAppParser.ContractContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#stateVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStateVar(SCAppParser.StateVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#typename}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypename(SCAppParser.TypenameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#elementaryTypeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElementaryTypeName(SCAppParser.ElementaryTypeNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(SCAppParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#qualifiedFun}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedFun(SCAppParser.QualifiedFunContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varLocExpr}
	 * labeled alternative in {@link SCAppParser#location}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarLocExpr(SCAppParser.VarLocExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrMapLocExpr}
	 * labeled alternative in {@link SCAppParser#location}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrMapLocExpr(SCAppParser.ArrMapLocExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code structLocExpr}
	 * labeled alternative in {@link SCAppParser#location}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructLocExpr(SCAppParser.StructLocExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code starExpr}
	 * labeled alternative in {@link SCAppParser#arrMapIndExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStarExpr(SCAppParser.StarExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code rangeExpr}
	 * labeled alternative in {@link SCAppParser#arrMapIndExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRangeExpr(SCAppParser.RangeExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code indexExpr}
	 * labeled alternative in {@link SCAppParser#arrMapIndExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexExpr(SCAppParser.IndexExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#structElemExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructElemExpr(SCAppParser.StructElemExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParams(SCAppParser.ParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(SCAppParser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logicNotExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicNotExpr(SCAppParser.LogicNotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sendExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSendExpr(SCAppParser.SendExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ownsExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOwnsExpr(SCAppParser.OwnsExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpr(SCAppParser.ParExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code enabledExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnabledExpr(SCAppParser.EnabledExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setquantifiedexpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetquantifiedexpr(SCAppParser.SetquantifiedexprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code landExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLandExpr(SCAppParser.LandExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpr(SCAppParser.UnaryMinusExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sumExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSumExpr(SCAppParser.SumExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lorExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLorExpr(SCAppParser.LorExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code impExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImpExpr(SCAppParser.ImpExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code memberAccessExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberAccessExpr(SCAppParser.MemberAccessExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code enabledUntilExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnabledUntilExpr(SCAppParser.EnabledUntilExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code accessExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccessExpr(SCAppParser.AccessExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sizeExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSizeExpr(SCAppParser.SizeExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimExpr(SCAppParser.PrimExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code addSubExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddSubExpr(SCAppParser.AddSubExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mulModExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulModExpr(SCAppParser.MulModExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code inEqExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInEqExpr(SCAppParser.InEqExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boxExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoxExpr(SCAppParser.BoxExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code enumStructExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumStructExpr(SCAppParser.EnumStructExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code keysExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeysExpr(SCAppParser.KeysExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hashExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHashExpr(SCAppParser.HashExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eqExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqExpr(SCAppParser.EqExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funCallExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunCallExpr(SCAppParser.FunCallExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code txExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTxExpr(SCAppParser.TxExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code quantifiedexpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuantifiedexpr(SCAppParser.QuantifiedexprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valuesExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuesExpr(SCAppParser.ValuesExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oldExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOldExpr(SCAppParser.OldExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code weakUntilExpr}
	 * labeled alternative in {@link SCAppParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWeakUntilExpr(SCAppParser.WeakUntilExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#argList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgList(SCAppParser.ArgListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolLitExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolLitExpr(SCAppParser.BoolLitExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numLitExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumLitExpr(SCAppParser.NumLitExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringLitExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLitExpr(SCAppParser.StringLitExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code thisExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThisExpr(SCAppParser.ThisExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code resultExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResultExpr(SCAppParser.ResultExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code selfExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelfExpr(SCAppParser.SelfExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code callerExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallerExpr(SCAppParser.CallerExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code amtExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmtExpr(SCAppParser.AmtExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blocknumExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlocknumExpr(SCAppParser.BlocknumExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code systimeExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSystimeExpr(SCAppParser.SystimeExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link SCAppParser#primaryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdExpr(SCAppParser.IdExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SCAppParser#quantifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuantifier(SCAppParser.QuantifierContext ctx);
}