package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.model.{Actor, SCApp}

import scala.collection.mutable

/**
 * Analyses a given SC app for consistency of the call specification.
 *
 * @param _scapp the app to be analysed
 */
class CallAnalysis(_scapp: SCApp) extends ModelAnalysis(_scapp):


  override def analyseCallerSet(caller: CallerSet): Set[ModelAnaErrRes] =
    // get functions that are available to callee and thereby also to caller (who shouldn't be able to access them)
    def getAnaRes(callee: CallerSet) =
      ModelAnaErrRes(caller.actor, (callee.called diff caller.called).map(f => Resource.Fun(f)), callee.actor)
    //the call capa spec is inconsistent iff a callee's capas are not a subset of the caller's
    def inconsistent(callee: CallerSet) =
      !callee.called.subsetOf(caller.called)

    caller.called.flatMap(callee =>
      callerSets.filter(c => c.actor == Actor.Fun(callee))
        .filter(inconsistent)
        .map(callee => getAnaRes(callee)))

