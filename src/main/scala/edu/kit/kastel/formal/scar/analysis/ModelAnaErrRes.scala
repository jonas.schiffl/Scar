package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.model.{Actor, Location, QualifiedFun, TransferCapa}

/**
 * A class representing an inconsistency in a model: An actor can access
 * a resource outside of their specified capabilities
 */
case class ModelAnaErrRes(actor: Actor, resources: Set[Resource], via: Actor):
  def errorMsg: String = resources.map(r =>
    s"$actor can ${r.actionString} via $via but is not allowed to do so according to spec")
    .mkString("\n")


enum Resource:
  case Fun(f: QualifiedFun)
  case State(loc: Location)
  case Transfer(tx: TransferCapa)
  def actionString: String = this match
    case Resource.Fun(f) => s"call ${f.contractName}.${f.funName}"
    case Resource.State(loc) => s"access $loc"
    case Resource.Transfer(tx) => s"transfer ${tx.limit} to ${tx.to}"