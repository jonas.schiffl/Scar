package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.model.{Actor, CallCapa, QualifiedFun, SCApp}

trait ModelAnalysis(_scapp: SCApp):
  val scapp: SCApp = _scapp
  val callCapas: Set[CallCapa] = scapp.capas.callCapas

  val callerSets: Set[CallerSet] =
    callCapas.map(c => CallerSet(c.actor, callCapas.filter(call => call.actor == c.actor)
      .map(call => call.callee)))

  def analyse: Set[ModelAnaErrRes] =
    callerSets.flatMap(cs => analyseCallerSet(cs))

  def analyseCallerSet(set: CallerSet): Set[ModelAnaErrRes]

case class CallerSet(actor: Actor, called: Set[QualifiedFun])
