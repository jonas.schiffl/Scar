package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.model.{Actor, Location, ModifyCapa, SCApp}

import scala.collection.mutable

class ModifiesAnalysis(_scapp: SCApp) extends ModelAnalysis(_scapp):

  private val modCapas = scapp.capas.modCapas

  override def analyseCallerSet(caller: CallerSet): Set[ModelAnaErrRes] =
    val res: mutable.Set[ModelAnaErrRes] = mutable.Set()
    val callees = caller.called
    for callee <- callees do
      val calleeActor = Actor.Fun(callee)
      val eeCapas = getModCapas(calleeActor)
      val unmatched = eeCapas.filter(ee => inconsistent(getModCapas(caller.actor), ee))
      if unmatched.nonEmpty then
        res.add(ModelAnaErrRes(caller.actor, unmatched.map(u => Resource.State(u.loc)), calleeActor))
    res.toSet

  private def inconsistent(callerCapas: Set[ModifyCapa], calleeCapa: ModifyCapa): Boolean =
    !hasSupersetIn(calleeCapa.loc, callerCapas.map(c => c.loc))

  private def hasSupersetIn(ee: Location, callerLocs: Set[Location]): Boolean =
    callerLocs.exists(callerLoc => ee.isLocSubsetOf(callerLoc))

  private def getModCapas(a: Actor): Set[ModifyCapa] =
    modCapas.filter(c => c.actor == a)
