package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.model.*

import scala.collection.mutable

class TxAnalysis(_scapp: SCApp) extends ModelAnalysis(_scapp):
  private val txCapas = scapp.capas.txCapas

  override def analyseCallerSet(cs: CallerSet): Set[ModelAnaErrRes] =
    val res: mutable.Set[ModelAnaErrRes] = mutable.Set()
    val callees = cs.called
    for callee <- callees do
      val calleeActor = Actor.Fun(callee)
      val eeCapas = getTxCapas(calleeActor)
      val unmatched = eeCapas.filter(ee => !hasSupersetIn(ee, getTxCapas(cs.actor), callee))
      if unmatched.nonEmpty then
        res.add(ModelAnaErrRes(cs.actor, unmatched.map(u => Resource.Transfer(u)), calleeActor))
    res.toSet

  private def hasSupersetIn(ee: TransferCapa, er: Set[TransferCapa], qf: QualifiedFun): Boolean =
    er.exists(callerTx => isTxSubsetOf(ee, callerTx, getCallingRoles(qf)))

  private def getTxCapas(a: Actor): Set[TransferCapa] =
    txCapas.filter(c => c.actor == a)

  private def isTxSubsetOf(sub: TransferCapa, sup: TransferCapa, calleeRoles: Set[Role]): Boolean =
    isAccSetSubsetOf(sub.from, sup.from, calleeRoles)
      && isAccSetSubsetOf(sub.to, sup.to, calleeRoles)
      && isLimitLeq(sub, sup)

  private def isAccSetSubsetOf(sub: AccountSet, sup: AccountSet, calleeRoles: Set[Role]): Boolean =
    sub match
      case AccountSet.ROLE(rsub) => sup match
        case AccountSet.ROLE(rsup) => rsub == rsup
        case AccountSet.ACCOUNT(a) => false
        case AccountSet.ANY => true
        case AccountSet.SELF | AccountSet.CALLER => false
      case AccountSet.ACCOUNT(asub) => sup match
        case AccountSet.ROLE(r) => calleeRoles.exists(ree => ree != r)
        case AccountSet.ACCOUNT(asup) => asub == asub
        case AccountSet.ANY => true
        case AccountSet.SELF | AccountSet.CALLER => false
      case AccountSet.ANY => sup match
        case AccountSet.ROLE(r) => false
        case AccountSet.ACCOUNT(a) => false
        case AccountSet.ANY => true
        case AccountSet.SELF | AccountSet.CALLER => false
      case AccountSet.SELF | AccountSet.CALLER => sup match
        case AccountSet.ROLE(r) => calleeRoles.exists(ree => ree != r)
        case AccountSet.ACCOUNT(a) => false
        case AccountSet.ANY => true
        case AccountSet.SELF | AccountSet.CALLER => true

  private def isLimitLeq(sub: TransferCapa, sup: TransferCapa): Boolean =
    val lex = sub.limit
    val upex = sup.limit
    if sameIndex(lex, upex) then
      true
    else
      lex match
        case NumberLiteralExpression(lowval, _, _) => upex match
          case NumberLiteralExpression(uval, _, _) => lowval <= uval
          case _ => false
        case MemberAccessExpr(expr, memberName, t, _) => upex match
          case MemberAccessExpr(expr, memberName, t, _) => true
          case _ => false
        case _ => false

  private def sameIndex(lex: Expr, upex: Expr): Boolean =
    lex match
      case ArrayAccExpr(arr, t, li, _) => upex match
        case ArrayAccExpr(arr, t, ui, _) => equalIndices(li, ui)
        case _ => false
      case MappingAccExpr(mapping, t, li, _) => upex match
        case MappingAccExpr(mapping, t, ui, _) => equalIndices(li, ui)
        case _ => false
      case _ => false

  private def getCallingRoles(qf: QualifiedFun): Set[Role] =
    scapp.capas.roles.filter(r =>
      callCapas.exists(c => c.actor == Actor.Ro(r) && c.callee == qf))