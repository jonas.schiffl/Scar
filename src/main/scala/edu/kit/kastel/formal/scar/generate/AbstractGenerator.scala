package edu.kit.kastel.formal.scar.generate

import edu.kit.kastel.formal.scar.model.SCApp

trait AbstractGenerator(_scapp: SCApp) {

  var curIndent: Int = 0
  val sb: StringBuilder = StringBuilder()
  val scapp: SCApp = _scapp
  val exceptions: Set[String] = Set.empty

  def getString: GenerationResult

  def generate(): Unit =
    val res = getString
    if res.exceptions.nonEmpty then
      println("Errors while generating:")
      res.exceptions.foreach(println)
    print(res.result)

  def generateToFile(fileName: String): Unit =
    val res = getString
    if res.exceptions.nonEmpty then
      println("Errors while generating:")
      res.exceptions.foreach(println)
    val writer = java.io.PrintWriter(fileName)
    writer.write(getString.result)
    writer.close()

  def ind(): String =
    "  " * curIndent
  
}

case class GenerationResult (result: String, exceptions: Set[String])
