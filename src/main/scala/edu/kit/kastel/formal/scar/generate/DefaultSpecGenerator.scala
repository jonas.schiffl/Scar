package edu.kit.kastel.formal.scar.generate

import DefaultSpecGenerator.e2s
import ScarGenerator.typeToScarString
import edu.kit.kastel.formal.scar.model.*

/**
 * A specification generator which outputs spec expressions as Scar,
 * with the possibility of a prefix to output as comments
 *
 * @param commentKey the String starting a comment in the target language, e.g., "//" in Solidity
 */


class DefaultSpecGenerator(commentKey: String) extends SpecGenerator:

  def getFunSpecString(f: Fun, ind: String): String =
    val sb = StringBuilder()
    for pre <- f.spec.pre do
      sb.append(ind + commentKey)
      sb.append("pre: " + e2s(pre) + "\n")
    for post <- f.spec.post do
      sb.append(ind + commentKey)
      sb.append("post: "+ e2s(post) + "\n")
    sb.result()

  def getInitSpecString(c: Contract, ind: String): String =
    val sb = StringBuilder()
    for init <- c.initCond(1) do
      sb.append(ind + commentKey)
      sb.append("init: " + e2s(init) + "\n")
    sb.result()

  // not needed here because the Scar generator translates the frame conditions
  override def getFunFrameString(qf: QualifiedFun, modCapas: Set[ModifyCapa], payable: Boolean, ind: String): String = ""


object DefaultSpecGenerator extends SpecGenerator:

  def getFunSpecString(f: Fun, ind: String): String = DefaultSpecGenerator("").getFunSpecString(f, ind)

  def getInitSpecString(c: Contract, ind: String): String = DefaultSpecGenerator("").getInitSpecString(c, ind)

  override def getFunFrameString(qf: QualifiedFun, modCapas: Set[ModifyCapa], payable: Boolean, ind: String): String = ""

  def e2s(expr: Expr): String =
    expr match
      case VarExpr(name, t, pos) => name
      case expr: QuantifiedExpr => quExprToString(expr)
      case expr: SetQuantifiedExpr => setQuExprToString(expr)
      case ArrayAccExpr(arr, t, index, pos) => s"${e2s(arr)}[${e2s(index)}]"
      case ArrayFunAccExpr(arr, t, index, pos) => s"${e2s(arr)}(${e2s(index)})"
      case MappingAccExpr(e, t, ind, pos) => s"${e2s(e)}[${e2s(ind)}]"
      case MappingFunAccExpr(e, t, ind, pos) => s"${e2s(e)}(${e2s(ind)})"
      case KeysExpr(e, t, pos) => s"keys(${e2s(e)})"
      case ValuesExpr(e, t, pos) => s"values(${e2s(e)})"
      case SizeExpr(e, pos) => s"size(${e2s(e)})"
      case EnumConstExpr(enumName, constName, t, pos) => s"$enumName#$constName"
      case StructAccExpr(structExpr, t, structFieldName, pos) => s"${e2s(structExpr)}#$structFieldName"
      case FunCallExpr(f, params, t, pos) => s"${e2s(f)}(${params.toString.mkString(", ")})"
      case MemberAccessExpr(expr, name, t, pos) => s"${e2s(expr)}.$name"
      case UnaryMinusExpr(e, pos) => s"-${e2s(e)}"
      case MulExpr(l, r, t, pos) => binExprToString(l, r, "*", MulExpr(l, r, t, pos))
      case DivExpr(l, r, t, pos) => binExprToString(l, r, "/", DivExpr(l, r, t, pos))
      case ModExpr(l, r, t, pos) => binExprToString(l, r, "%", ModExpr(l, r, t, pos))
      case AddExpr(l, r, t, pos) => binExprToString(l, r, "+", AddExpr(l, r, t, pos))
      case SubExpr(l, r, t, pos) => binExprToString(l, r, "-", SubExpr(l, r, t, pos))
      case LtExpr(l, r, pos) => binExprToString(l, r, "<", LtExpr(l, r, pos))
      case GtExpr(l, r, pos) => binExprToString(l, r, ">", GtExpr(l, r, pos))
      case LeqExpr(l, r, pos) => binExprToString(l, r, "<=", LeqExpr(l, r, pos))
      case GeqExpr(l, r, pos) => binExprToString(l, r, ">=", GeqExpr(l, r, pos))
      case EqExpr(l, r, pos) => binExprToString(l, r, "==", EqExpr(l, r, pos))
      case NeqExpr(l, r, pos) => binExprToString(l, r, "!=", NeqExpr(l, r, pos))
      case LogicNotExpr(e, pos) => s"!${e2s(e)}"
      case LandExpr(l, r, pos) => binExprToString(l, r, "&&", LandExpr(l, r, pos))
      case LorExpr(l, r, pos) => binExprToString(l, r, "||", LorExpr(l, r, pos))
      case ImpExpr(l, r, pos) => binExprToString(l, r, "=>", ImpExpr(l, r, pos))
      case expr: LiteralExpression => litExpToString(expr)
      case OldExpr(e, t, pos) => s"\\old(${e2s(e)})"
      case HashExpr(e, pos) => s"\\hash(${e2s(e)})"
      case ResultExpr(_, _) => "\\result"
      case ThisExpr(_) => "\\this"
      case CallerExpr(_) => "\\caller"
      case SelfExpr(_) => "\\self"
      case AmtExpr(_) => "\\amt"
      case SystimeExpr(_) => "\\systime"
      case StarLocExpr(t, pos) => "*"
      case RangeLocExpr(from, to, t, pos) => s"${e2s(from)}..${e2s(to)}"
      case IndexLocExpr(index, t, pos) => s"${e2s(index)}"
      case SendExpr(from, to, amt, pos) => s"\\send(${e2s(from)}, ${e2s(to)}, ${e2s(amt)})"
      case BoxExpr(expr, pos) => s"G(${e2s(expr)})"
      case EnabledExpr(account, paramCond, amt, expr, pos) =>
        s"enabled{${optExprToString(account)}}{${optExprToString(paramCond)}}{${optExprToString(amt)}}(${e2s(expr)})"
      case EnabledUntilExpr(account, paramCond, amt, expr, pos) =>
        s"enabledU{${optExprToString(account)}}{${optExprToString(paramCond)}}{${optExprToString(amt)}}(${e2s(expr)})"
      case TxExpr(fun, caller, paramCond, amt, pos) =>
        s"$fun{${optExprToString(caller)}}{${optExprToString(paramCond)}}{${optExprToString(amt)}}"
      case OwnsExpr(account, amt, _) => s"owns(${e2s(account)}, ${e2s(amt)})"
      case WeakUntilExpr(l, r, pos) => s"${e2s(l)} UW ${e2s(r)}"
      case ERR_EXPR(msg, pos) => s"EXPRESSION_ERROR $msg at $pos"
      case e => s"Unmatched expression ${e.getClass} in Generator at ${e.pos}"

  private def optExprToString(e: Option[Expr]): String =
    e match
      case Some(expr) => e2s(expr)
      case None => ""

  private def litExpToString(e: LiteralExpression): String =
    e match
      case BoolLiteralExpression(value, pos) => value.toString
      case NumberLiteralExpression(value, t, pos) => value.toString
      case StringLiteralExpression(value, pos) => value
      case _ => ""

  private def binExprToString(l: Expr, r: Expr, op: String, parent: Expr): String =
    // parens if parent expr binds stronger than or equal to child
    val lString = if parent.bindsStrongerOrEqual(l) && l.arity > 1 then s"(${e2s(l)})" else e2s(l)
    val rString = if parent.bindsStrongerOrEqual(r) && r.arity > 1 then s"(${e2s(r)})" else e2s(r)
    s"$lString $op $rString"

  private def quExprToString(expr: QuantifiedExpr): String =
    expr match
      case ExistsExpr(qVar, matrix, pos) => s"\\exists (${qVar.name}:${typeToScarString(qVar.t)}): ${e2s(matrix)}"
      case ForallExpr(qVar, matrix, pos) => s"\\forall (${qVar.name}:${typeToScarString(qVar.t)}): ${e2s(matrix)}"
      case _ => ""

  private def setQuExprToString(expr: SetQuantifiedExpr): String =
    expr match
      case SetExistsExpr(qVar, set, matrix, pos) => s"\\exists (${qVar.name} \\in ${e2s(set)}): ${e2s(matrix)}"
      case SetForallExpr(qVar, set, matrix, pos) => s"\\forall (${qVar.name} \\in ${e2s(set)}): ${e2s(matrix)}"
      case _ => ""
