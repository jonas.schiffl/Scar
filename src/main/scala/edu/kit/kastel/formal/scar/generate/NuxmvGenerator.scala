package edu.kit.kastel.formal.scar.generate

import edu.kit.kastel.formal.scar.model.*

class NuxmvGenerator(_scapp: SCApp, bitLen: Int, arrLen: Int) extends AbstractGenerator(_scapp) :

  private val inputs: Array[String] = Array(s"amt: unsigned word[$bitLen];",
    s"caller : unsigned word[$bitLen];",
    s"blocknum : unsigned word[$bitLen];",
    s"systime : unsigned word[$bitLen];")

  override def getString: GenerationResult =
    sb.clear()
    scapp.contracts.foreach(c => sb.append(translateContract(c)))
    sb.append(translateTempProps(scapp.tempProps))
    GenerationResult(sb.result(), exceptions)

  private def translateTempProps(tempProps: Set[Expr]): String =
    val tb = new StringBuilder()
    tb.append("INVARSPEC\n")
    curIndent += 1
    tb.append(tempProps.map(tp => s"${ind()}${translateExpr(tp, false)}").mkString(" &\n"))
    curIndent -= 1
    tb.result()

  private def translateContract(contract: Contract): String =
    val usb = new StringBuilder()
    usb.append(s"MODULE ${contract.name}\n")
    usb.append("VAR\n")
    curIndent += 1
    contract.stateVars.foreach(sv => usb.append(s"${ind()}${sv.name}: ${tlType(sv.t)};\n"))
    curIndent -= 1
    usb.append(inputVars(contract))
    usb.append(translateInit(contract))
    usb.append(translateFunctions(contract))
    usb.result()

  private def translateInit(contract: Contract): String =
    val init = contract.initCond(1)
    if init.isEmpty then return ""
    val ib = new StringBuilder()
    ib.append("INIT\n")
    curIndent += 1
    ib.append(init.map(i => s"${ind()}${translateExpr(i, false)}").mkString(" &\n"))
    curIndent -= 1
    ib.result()

  private def translateFunctions(contract: Contract): String =
    val fb = new StringBuilder()
    fb.append("TRANS (\n")
    curIndent += 1
    fb.append(contract.functions.map(f => translateFunction(f)).mkString("\n & \n"))
    fb.append(");\n")
    fb.result()

  private def translateFunction(f: Fun): String =
    val fb = new StringBuilder()
    curIndent += 1
    fb.append(s"(${ind()}message = ${f.name} \n")
    if f.spec.pre.nonEmpty then
    fb.append(s"${ind()}& ${f.spec.pre.map(p => s"${ind()}${translateExpr(p, false)}").mkString(" &\n")}\n")
    fb.append(s"${ind()}->\n")
    fb.append(s"${f.spec.post.map(p => s"${ind()}${translateExpr(p, true)}").mkString(" &\n")}\n)")
    curIndent -= 1
    fb.result()

  private def translateExpr(e: Expr, post: Boolean): String =
    e match
      case OldExpr(e, t, pos) => translateExpr(e, post)
      case VarExpr(name, t, pos) => if post then s"next($name)" else name
      case ArrayAccExpr(arr, t, index, pos) => s"${translateExpr(arr, post)}[${translateExpr(index, post)}]"
      case MappingAccExpr(mapping, t, index, pos) => s"${translateExpr(mapping, post)}[${translateExpr(index, post)}]"
      case EnumConstExpr(enumName, constName, t, pos) => constName
      case MemberAccessExpr(contractExpr, memberName, t, pos) => s"${translateExpr(contractExpr, post)}.$memberName"
      case UnaryMinusExpr(e, pos) => s"-${translateExpr(e, post)}"
      case expr: BinArithExpr => binArithExpToString(expr, post)
      case expr: CompExpr => compExpToString(expr, post)
      case EqExpr(l, r, pos) => s"${translateExpr(l, post)} = ${translateExpr(r, post)}"
      case NeqExpr(l, r, pos) => s"${translateExpr(l, post)} != ${translateExpr(r, post)}"
      case LogicNotExpr(e, pos) => s"!${translateExpr(e, post)}"
      case LandExpr(l, r, pos) => s"${translateExpr(l, post)} & ${translateExpr(r, post)}"
      case LorExpr(l, r, pos) => s"${translateExpr(l, post)} | ${translateExpr(r, post)}"
      case ImpExpr(l, r, pos) => s"${translateExpr(l, post)} -> ${translateExpr(r, post)}"
      case CallerExpr(pos) => "caller"
      case AmtExpr(pos) => "amt"
      case BlocknumExpr(pos) => "blocknum"
      case SystimeExpr(pos) => "systime"
      case expression: LiteralExpression => translateLitExpr(expression)
      // ---------------------------------- liveness
      case BoxExpr(expr, pos) => s"G (${translateExpr(expr, false)})"
      case WeakUntilExpr(l, r, pos) => // x WU y <==> y V (y || x)
        s"${translateExpr(r, false)} V ( ${translateExpr(r, false)} | ${translateExpr(l, false)} )"
      case EnabledExpr(account, paramCond, amt, expr, pos) => "Not yet implemented"
      case EnabledUntilExpr(account, paramCond, amt, expr, pos) => "Not yet implemented"
      case TxExpr(fun, caller, paramCond, amt, pos) =>
        val callerString = if caller.isEmpty then ""
          else s" & caller = ${translateExpr(caller.get, false)}"
        val paramString = if paramCond.isEmpty then ""
          else s" & ${translateExpr(paramCond.get, false)}"
        val amtString = if amt.isEmpty then ""
          else s" & amt = ${translateExpr(amt.get, false)}"
        s"message = ${fun.funName} $callerString $paramString $amtString"
      case OwnsExpr(account, amtExpr, pos) => "Not yet implemented"
      // ---------------------------------- unsupported
      case SendExpr(fromExpr, toExpr, amtExpr, pos) => "UNSUPPORTED EXPR TYPE"
      case HashExpr(e, pos) => "UNSUPPORTED EXPR TYPE"
      case FunCallExpr(funExpr, params, t, pos) => "UNSUPPORTED EXPR TYPE"
      case SelfExpr(pos) => "UNSUPPORTED EXPR TYPE"
      case StructAccExpr(structExpr, t, structFieldName, pos) => "UNSUPPORTED EXPR TYPE"
      case StarLocExpr(t, pos) => "UNSUPPORTED EXPR TYPE"
      case RangeLocExpr(from, to, t, pos) => "UNSUPPORTED EXPR TYPE"
      case IndexLocExpr(index, t, pos) => "UNSUPPORTED EXPR TYPE"
      case expr: QuantifiedExpr => "UNSUPPORTED EXPR TYPE"
      case ERR_EXPR(msg, pos) => s"ERR_EXPR: $msg"

  private def inputVars(c: Contract): String =
    val ivb = new StringBuilder()
    ivb.append("IVAR\n")
    curIndent += 1
    ivb.append(s"message : {${c.functions.map(f => f.name).mkString(", ")}};\n")
    inputs.foreach(i => ivb.append(s"${ind()}$i\n"))
    curIndent -= 1
    ivb.result()


  private def tlType(t: ModelType): String =
    t match
      case AccountType => s"unsigned word[$bitLen]"
      case ArrayType(t) => s"array word[$arrLen] of ${tlType(t)}"
      case MappingType(fromType, toType) => s"array word[$arrLen] of ${tlType(toType)}"
      case BoolType => "boolean"
      case numberType: NumberType => numberTypeToString(numberType)
      case StringType => s"unsigned word[$bitLen]"
      case u: UserDefinedType => tlUdt(u)
      case EXPR_ERR_TYPE(err) => "EXPR_ERR_TYPE"

  private def numberTypeToString(t: NumberType) =
    t match
      case IntType => s"signed word[$bitLen]"
      case UintType => s"unsigned word[$bitLen]"

  private def tlUdt(u: UserDefinedType): String =
    u match
      case StructType(name, fields) => "cannot translate struct types yet"
      case EnumType(name, values) =>
        val sb = new StringBuilder()
        s"{${values.mkString(", ")}}"

  private def compExpToString(e: CompExpr, post: Boolean): String =
    e match
      case GtExpr(l, r, pos) => s"${translateExpr(l, post)} > ${translateExpr(r, post)}"
      case GeqExpr(l, r, pos) => s"${translateExpr(l, post)} >= ${translateExpr(r, post)}"
      case LtExpr(l, r, pos) => s"${translateExpr(l, post)} < ${translateExpr(r, post)}"
      case LeqExpr(l, r, pos) => s"${translateExpr(l, post)} <= ${translateExpr(r, post)}"

  private def binArithExpToString(e: BinArithExpr, post: Boolean): String =
    e match
      case AddExpr(l, r, _, pos) => s"${translateExpr(l, post)} + ${translateExpr(r, post)}"
      case SubExpr(l, r, _, pos) => s"${translateExpr(l, post)} - ${translateExpr(r, post)}"
      case MulExpr(l, r, _, pos) => s"${translateExpr(l, post)} * ${translateExpr(r, post)}"
      case DivExpr(l, r, _, pos) => s"${translateExpr(l, post)} / ${translateExpr(r, post)}"
      case ModExpr(l, r, _, pos) => s"${translateExpr(l, post)} % ${translateExpr(r, post)}"

  private def translateLitExpr(e: LiteralExpression): String =
    e match
      case BoolLiteralExpression(value, pos) => value.toString
      case NumberLiteralExpression(value, t, pos) => value.toString
      case StringLiteralExpression(value, pos) => value
