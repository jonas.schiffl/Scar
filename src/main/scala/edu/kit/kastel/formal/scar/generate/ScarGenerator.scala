package edu.kit.kastel.formal.scar.generate

import DefaultSpecGenerator.e2s
import ScarGenerator.*
import edu.kit.kastel.formal.scar.model.*

import scala.collection.mutable

class ScarGenerator(_scapp: SCApp) extends AbstractGenerator(_scapp) {

  val specGen: DefaultSpecGenerator = DefaultSpecGenerator("")
  private val capas = scapp.capas

  override def getString: GenerationResult =
    sb.clear()
    sb.append(s"application: ${scapp.name} \n\n")
    if scapp.capas.roles.nonEmpty then
      sb.append(rolesToScarString())
    for udenum <- scapp.userDefTypes.enumTypes do
      sb.append(userDefEnumString(udenum))
    for udstruct <- scapp.userDefTypes.structTypes do
      sb.append(userDefStructString(udstruct))
    for tp <- scapp.tempProps do
      sb.append(s"tempspec: ${e2s(tp)}\n")
    for c <- scapp.contracts do
      sb.append(contractString(c))
    GenerationResult(sb.result(), exceptions)

  private def contractString(c: Contract): String =
    val csb = StringBuilder()
    csb.append(s"${ind()}contract ${c.name}: \n")
    curIndent += 1
    csb.append(s"${ind()}state: \n")
    curIndent += 1
    for sv <- c.stateVars do
      csb.append(s"${ind()}var ${stateVarToString(sv)} \n")
    curIndent -= 1
    for inv <- c.invariants do
      csb.append(s"${ind()}invariant: ${e2s(inv)} \n")
    for init <- c.initCond(1) do
      csb.append(s"${ind()}init: ${e2s(init)} \n")
    csb.append(s"${ind()}functions: \n")
    curIndent += 1
    for f <- c.functions do
      csb.append(funString(f, c))
    curIndent -= 2
    csb.result()

  private def funString(f: Fun, c: Contract): String =
    val fsb = StringBuilder()
    fsb.append(s"${ind()}fun ${f.name}: \n")
    curIndent += 1
    if f.params.nonEmpty then
      fsb.append(s"${ind()}params: ${f.params.map(p => paramToString(p)).mkString(", ")} \n")
    if f.retType.nonEmpty then
      fsb.append(s"${ind()}returns: ${typeToScarString(f.retType.get)} \n")
    for pre <- f.spec.pre do
      fsb.append(s"${ind()}pre: ${e2s(pre)} \n")
    for post <- f.spec.post do
      fsb.append(s"${ind()}post: ${e2s(post)} \n")
    for c <- getFunCapas(QualifiedFun(f.name, c.name)) do
      fsb.append(ind() + capaToScarString(c))
    curIndent -= 1
    fsb.result()

  private def userDefEnumString(enumType: EnumType): String =
    s"enum ${enumType.name} ${enumType.consts.mkString("{", ", ", "}")} \n"

  private def userDefStructString(structType: StructType): String =
    s"struct ${structType.name} ${structType.vars.map(sv => stateVarToString(sv)).mkString("{", ", ", "}")} \n"

  private def stateVarToString(sv: StateVar): String =
    s"${sv.name}: ${typeToScarString(sv.t)}"

  private def paramToString(sv: Param): String =
    s"${typeToScarString(sv.t)} ${sv.name}"

  private def getFunCapas(qf: QualifiedFun): Set[Capability] =
    val res: mutable.Set[Capability] = mutable.Set()
    res.addAll(capas.txCapas).addAll(capas.modCapas).addAll(capas.callCapas)
    res.filter(c => c.actor == Actor.Fun(qf)).toSet

  private def rolesToScarString(): String =
    val rsb = StringBuilder()
    rsb.append("roles: \n")
    curIndent += 1
    for r <- capas.roles do
      rsb.append(ind() + "role " + r.name)
      val rCapas = getRoleCapas(r)
      if rCapas.isEmpty then rsb.append("\n")
      else
        rsb.append(":\n")
        curIndent += 1
        for rcapa <- rCapas do
          rsb.append(ind() + capaToScarString(rcapa))
        curIndent -= 1
    curIndent -= 1
    rsb.append("\n")
    rsb.result()

  private def getRoleCapas(r: Role): Set[Capability] =
    val res: mutable.Set[Capability] = mutable.Set()
    res.addAll(capas.txCapas).addAll(capas.modCapas).addAll(capas.callCapas)
    res.filter(c => c.actor == Actor.Ro(Role(r.name))).toSet
}

object ScarGenerator:

  val CALLS = "calls"
  val MOD = "modifies"
  val TRANSFERS = "transfers"

  def typeToScarString(t: ModelType): String =
    t match
      case AccountType => "account"
      case ArrayType(t) => s"${typeToScarString(t)}[]"
      case MappingType(fromType, toType) => s"mapping(${typeToScarString(fromType)}=>${typeToScarString(toType)})"
      case BoolType => "bool"
      case numberType: NumberType => numberTypeToString(numberType)
      case StringType => "string"
      case definedType: UserDefinedType => udtToString(definedType)
      case EXPR_ERR_TYPE(err) => "EXPR_ERR_TYPE"
      case t => s"Unmatched Type ${t.toString}"

  def capaToScarString(capa: Capability): String =
    capa match
      case CallCapa(caller, callee) => s"$CALLS: ${callee.contractName}.${callee.funName}\n"
      case ModifyCapa(actor, loc) => s"$MOD: ${locToScarString(loc)}\n"
      case TransferCapa(actor, from, to, limit) => s"$TRANSFERS: ${txToScarString(from, to, limit)}\n"

  private def locToScarString(loc: Location): String =
    loc match
      case VarLoc(v) => s"${v.contractName}.${v.varName}"
      case ArrElementLoc(arr, index) => s"${arr.contractName}.${arr.varName}[${e2s(index)}]"
      case MapElementLoc(map, index) => s"${map.contractName}.${map.varName}[${e2s(index)}]"
      case ArrAllLoc(arr) => s"${arr.contractName}.${arr.varName}[*]"
      case MapAllLoc(map) => s"${map.contractName}.${map.varName}[*]"
      case ArrRangeLoc(arr, from, to) => s"${arr.contractName}.${arr.varName}[${e2s(from)}..${e2s(to)}]"
      case StructAllLoc(struct) => s"${struct.contractName}.${struct.varName}[*]"
      case StructVarLoc(struct, varname) => s"${struct.contractName}.${struct.varName}#$varname"

  private def txToScarString(from: AccountSet, to: AccountSet, limit: Expr): String =
    def accSet2String(a: AccountSet): String =
      a match
        case AccountSet.ROLE(r) => r.name
        case AccountSet.ACCOUNT(a) => a.name
        case AccountSet.ANY => "\\any"
        case AccountSet.SELF => "\\self"
        case AccountSet.CALLER => "\\caller"
    val limitString = e2s(limit)
    s"(${accSet2String(from)}, ${accSet2String(to)}, $limitString)"

  private def numberTypeToString(numberType: NumberType): String =
    numberType match
      case IntType => "int"
      case UintType => "uint"

  private def udtToString(userDefinedType: UserDefinedType): String =
    userDefinedType match
      case EnumType(name, consts) => name
      case StructType(name, vars) => name
      case _ => ""