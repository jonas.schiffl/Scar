package edu.kit.kastel.formal.scar.generate

import edu.kit.kastel.formal.scar.model.*

object SolcVerifySolSpecGenerator extends SolSpecGenerator:

  private val NOTICEPRE = "/// @notice precondition "
  private val NOTICEPOST = "/// @notice postcondition "
  private val NOTICEMOD = "/// @notice modifies "

  override def getFunSpecString(f: Fun, ind: String): String =
    val sb = StringBuilder()
    for pre <- f.spec.pre do
      sb.append(ind + NOTICEPRE)
      sb.append(e2s(pre) + "\n")
    for post <- f.spec.post do
      sb.append(ind + NOTICEPOST)
      sb.append(e2s(post) + "\n")
    sb.result()

  override def getFunFrameString(qf: QualifiedFun, modCapas: Set[ModifyCapa], payable: Boolean, ind: String): String =
    val fsb = StringBuilder()
    val locs = modCapas.filter(mc => mc.actor == Actor.Fun(qf)).map(mc => mc.loc)
    val thisContract = qf.contractName
    locs.foreach(loc => fsb.append(s"$ind$NOTICEMOD${loc2s(loc, thisContract)}\n"))
    if payable then
      fsb.append(s"$ind$NOTICEMOD address(this).balance\n")
    fsb.result()

  override def getInitSpecString(c: Contract, ind: String): String =
    val sb = StringBuilder()
    for init <- c.initCond(1) do
      sb.append(ind + NOTICEPOST)
      sb.append(e2s(init) + "\n")
    sb.result()

  def e2s(expr: Expr): String =
    // TODO match all expressions
    expr match
      case VarExpr(name, t, pos) => name
      case expr: QuantifiedExpr => quExprToString(expr)
      case e: SetQuantifiedExpr => setQExToString(e)
      case ArrayAccExpr(arr, t, index, pos) => s"${e2s(arr)}[${e2s(index)}]"
      case EnumConstExpr(enumName, constName, t, pos) => s"UTIL.$enumName.$constName"
      case StructAccExpr(structExpr, t, structFieldName, pos) => s"${e2s(structExpr)}.$structFieldName"
      case FunCallExpr(f, params, t, pos) => s"${e2s(f)}(${params.toString.mkString(", ")})"
      case MemberAccessExpr(expr, name, t, pos) => s"${e2s(expr)}.$name"
      case UnaryMinusExpr(e, pos) => s"-${e2s(e)}"
      case MulExpr(l, r, t, pos) => binExprToString(l, r, "*")
      case DivExpr(l, r, t, pos) => binExprToString(l, r, "/")
      case ModExpr(l, r, t, pos) => binExprToString(l, r, "%")
      case AddExpr(l, r, t, pos) => binExprToString(l, r, "+")
      case SubExpr(l, r, t, pos) => binExprToString(l, r, "-")
      case LtExpr(l, r, pos) => binExprToString(l, r, "<")
      case GtExpr(l, r, pos) => binExprToString(l, r, ">")
      case LeqExpr(l, r, pos) => binExprToString(l, r, "<=")
      case GeqExpr(l, r, pos) => binExprToString(l, r, ">=")
      case EqExpr(l, r, pos) => binExprToString(l, r, "==")
      case NeqExpr(l, r, pos) => binExprToString(l, r, "!=")
      case RefEqExpr(l, r, _) => binExprToString(l, r, "==")
      case RefNeqExpr(l, r, _) => binExprToString(l, r, "!=")
      case LogicNotExpr(e, pos) => s"!${e2s(e)}"
      case LandExpr(l, r, pos) => binExprToString(l, r, "&&")
      case LorExpr(l, r, pos) => binExprToString(l, r, "||")
      case ImpExpr(l, r, pos) => e2s(LorExpr(LogicNotExpr(l, pos), r, pos))
      case expr: LiteralExpression => litExpToString(expr)
      case OldExpr(e, t, pos) => s"__verifier_old_${SolidityGenerator.typeToSolString(t)}(${e2s(e)})"
      case MappingAccExpr(e, t, ind, pos) => s"${e2s(e)}[${e2s(ind)}]"
      case CallerExpr(_) => "msg.sender"
      case AmtExpr(_) => "msg.value"
      case ThisExpr(_) => "this"
      case BlocknumExpr(_) => "block.number"
      case SystimeExpr(_) => "block.timestamp"
      case SizeExpr(a, pos) => s"${e2s(a)}.length"
      case SumExpr(t, arr, _) => s"__verifier_sum_${SolidityGenerator.typeToSolString(t)}(${e2s(arr)})"
      // This assumes that the function returns a variable named "result"
      case ResultExpr(_, _) => "result"
      case s: SendExpr => translateSend(s)
      // TODO should maybe be translated at some point
      case ArrayFunAccExpr(_, _, _, _) =>
        exceptions.add("Spec Gen: Unsupported expression ArrayFunAccExpr")
        ""
      case MappingFunAccExpr(_, _, _, _) =>
        exceptions.add("Spec Gen: Unsupported expression MappingFunAccExpr")
        ""
      case KeysExpr(_, _, _) =>
        exceptions.add("Spec Gen: Unsupported expression KeysExpr")
        ""
      case ValuesExpr(_, _, _) =>
        exceptions.add("Spec Gen: Unsupported expression ValuesExpr")
        ""
      case HashExpr(_, _) =>
        exceptions.add("Spec Gen: Unsupported expression HashExpr")
        ""
      // The expressions below do not occur in function contracts, so they are not translated
      case EnabledExpr(_, _, _, _, _) => ""
      case SelfExpr(_) => ""
      case EnabledUntilExpr(_, _, _, _, _) => ""
      case TxExpr(_, _, _, _, _) => ""
      case OwnsExpr(_, _, _) => ""
      case BoxExpr(_, _) => ""
      case WeakUntilExpr(_, _, _) => ""
      case StarLocExpr(_, _) => ""
      case RangeLocExpr(_, _, _, _) => ""
      case IndexLocExpr(_, _, _) => ""
      case ERR_EXPR(msg, pos) => "EXPRESSION_ERROR"

  private def loc2s(loc: Location, thisContract: String): String =
    loc match
      case VarLoc(v) => contractPrefix(v.contractName, thisContract) + v.varName
      case ArrElementLoc(arr, index) => contractPrefix(arr.contractName, thisContract) + arr.varName + "[" + e2s(index) + "]"
      case MapElementLoc(map, index) => contractPrefix(map.contractName, thisContract) + map.varName + "[" + e2s(index) + "]"
      case ArrAllLoc(arr) => contractPrefix(arr.contractName, thisContract) + arr.varName + "[*]"
      case MapAllLoc(map) => contractPrefix(map.contractName, thisContract) + map.varName + "[*]"
      case ArrRangeLoc(arr, from, to) => "Array Range Unsupported"
      case StructAllLoc(struct) => contractPrefix(struct.contractName, thisContract) + struct.varName + ".*"
      case StructVarLoc(struct, varname) => contractPrefix(struct.contractName, thisContract) + struct.varName + "." + varname

  private def contractPrefix(locContract:String, thisContract: String): String =
    if locContract == thisContract then ""
    else locContract + "."

  private def binExprToString(l: Expr, r: Expr, op: String): String =
    s"${e2s(l)} $op ${e2s(r)}"

  private def quExprToString(expr: QuantifiedExpr): String =
    expr match
      case ExistsExpr(qVar, matrix, pos) => s"exists (${SolidityGenerator.typeToSolString(qVar.t)} ${qVar.name}) ${e2s(matrix)}"
      case ForallExpr(qVar, matrix, pos) => s"forall (${SolidityGenerator.typeToSolString(qVar.t)} ${qVar.name}) ${e2s(matrix)}"
      case _ => ""

  private def setQExToString(expr: SetQuantifiedExpr): String =
    expr match
      case SetExistsExpr(qVar, set, matrix, pos) =>
        set match
          case VarExpr(name, t, pos) =>
            t match
              case ArrayType(_) => s"exists (${SolidityGenerator.typeToSolString(qVar.t)} ${qVar.name}) ${e2s(matrix)}"
              case _ =>
                exceptions.add(s"Spec Gen: SetExistsExpr for non-array type is not supported at $pos")
                ""
          case _ =>
            exceptions.add(s"Spec Gen: Error in SetExistsExpr; bad type for set at $pos")
            ""
      case SetForallExpr(qVar, set, matrix, pos) =>
        set match
          case VarExpr(name, t, pos) =>
            t match
              case ArrayType(_) => s"forall (${SolidityGenerator.typeToSolString(qVar.t)} ${qVar.name}) ${e2s(matrix)}"
              case _ =>
                exceptions.add(s"Spec Gen: SetForallExpr for non-array type is not supported at $pos")
                ""
          case _ =>
            exceptions.add(s"Spec Gen: Error in SetForallExpr; bad type for set at $pos")
            ""
      case _ => ""

  private def translateSend(s: SendExpr): String =
    val from = e2s(s.fromExpr)
    val to = e2s(s.toExpr)
    val amt = e2s(s.amtExpr)
    s"$from.balance == __verifier_old_uint($from.balance) - $amt && $to.balance == __verifier_old_uint($to.balance) + $amt"

  private def litExpToString(e: LiteralExpression): String =
    e match
      case BoolLiteralExpression(value, pos) => value.toString
      case NumberLiteralExpression(value, t, pos) => value.toString
      case StringLiteralExpression(value, pos) => value
      case _ => ""