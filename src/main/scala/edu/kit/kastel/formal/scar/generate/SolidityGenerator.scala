package edu.kit.kastel.formal.scar.generate

import SolidityGenerator.*
import edu.kit.kastel.formal.scar.model.*

import scala.collection.mutable

class SolidityGenerator(_scapp: SCApp, _specgen: Option[SolSpecGenerator]) extends AbstractGenerator(_scapp):

  private val specgen: SpecGenerator = _specgen match
    case Some(s) => s
    case None => DefaultSpecGenerator("//")

  val capas: Capabilities = scapp.capas

  override def getString: GenerationResult =
    sb.clear()
    sb.append("// SPDX-License-Identifier: UNLICENSED\n")  // to prevent warning
    // solc-verify was forked at 0.7.6-develop, which is strictly less than 0.7
    sb.append(s"pragma solidity >0.6 <0.7;\n\n")
    sb.append(utilContractString())
    for c <- scapp.contracts do
      sb.append(contractString(c))
    GenerationResult(sb.result(), exceptions)

  private def utilContractString(): String =
    val usb = StringBuilder()
    usb.append("library UTIL {\n")
    curIndent += 1
    //user-defined types
    for udenum <- scapp.userDefTypes.enumTypes do
      usb.append(ind() + userDefEnumString(udenum))
    for udstruct <- scapp.userDefTypes.structTypes do
      usb.append(ind() + userDefStructString(udstruct))
    usb.append("\n")
    // access control
    usb.append(s"${ind()}${enumString("Role", scapp.capas.roles.map(r => r.name))}\n")
    usb.append(s"${ind()}function hasRole(address a, Role r) internal pure returns (bool) { }\n")
    curIndent -= 1
    usb.append("}\n\n")
    usb.result()

  private def getConstructorString(c: Contract, ind: String): String =
    val csb = StringBuilder()
    csb.append(specgen.getInitSpecString(c, ind))
    val paramString = c.initCond(0).map(p => paramToString(p)).mkString(", ")
    csb.append(s"$ind constructor ($paramString) { }\n\n")
    csb.result()

  private def contractString(c: Contract): String =
    val csb = StringBuilder()
    csb.append(s"${ind()}contract ${c.name} {\n")
    curIndent += 1
    csb.append(ind() + getModifierStrings(c, capas))
    for sv <- c.stateVars do
      csb.append(s"${ind()} ${stateVarToString(sv)};\n")
    sb.append("\n")
    csb.append(getConstructorString(c, ind()))
    for f <- c.functions do
      csb.append(funString(f, c))
    curIndent -= 1
    csb.append(s"${ind()}}\n\n")
    csb.result()

  private def funString(f: Fun, c: Contract): String =
    val fsb = StringBuilder()
    val specStr = specgen.getFunSpecString(f, ind())
    val payable = specStr.contains("msg.value")
    val payableStr =  if payable then "payable" else ""
    fsb.append(specStr)
    fsb.append(specgen.getFunFrameString(QualifiedFun(f.name, c.name), capas.modCapas, true, ind()))
    val paramstr = f.params.map(p => paramToString(p)).mkString(", ")
    val modstr = getModStringForFun(QualifiedFun(f.name, c.name), capas)
    fsb.append(s"${ind()}function ${f.name}" +
      s"($paramstr) $modstr public $payableStr ${getReturns(f)} { }\n\n")
    fsb.result()

  private def getReturns(fun: Fun): String =
    if fun.retType.nonEmpty then
      s"returns (${typeToSolString(fun.retType.get)} result)"
    else ""

  private def enumString(name: String, members: scala.collection.Iterable[String]) =
    s"enum $name {${if (members.isEmpty) "ANY" else members.mkString(", ")}}"

  private def userDefEnumString(enumType: EnumType): String =
    enumString(enumType.name, enumType.consts) + "\n"

  private def userDefStructString(structType: StructType): String =
    s"struct ${structType.name} ${structType.vars.map(sv => stateVarToString(sv)).mkString("{", "; ", ";}")}\n"

  private def stateVarToString(sv: StateVar): String =
    s"${typeToSolString(sv.t)} ${sv.name}"

  private def paramToString(sv: Param): String =
    s"${paramTypeToSolString(sv.t)} ${sv.name}"

  private def getModStringForFun(f: QualifiedFun, capas: Capabilities): String =
    val callerSet = getCallingRolesForFun(f, capas).map(r => r.name)
    val msb = StringBuilder()
    if callerSet.nonEmpty then
      msb.append(modifierName(callerSet))
    msb.result()

  private def modifierName(callerSet: Set[String]): String=
    s"only${callerSet.mkString("")}"

  private def getModifierStrings(c: Contract, capas: Capabilities): String =
    val msb = StringBuilder()
    val callerSets: mutable.Set[Set[String]] = mutable.Set()
    for f <- c.functions do
      val qf = QualifiedFun(f.name, c.name)
      callerSets.add(getCallingRolesForFun(qf, capas).map(r => r.name))
    for c <- callerSets do
      if c.nonEmpty then msb.append(getModifierString(c))
    msb.result()

  private def getModifierString(callerSet: Set[String]): String =
    val msb = StringBuilder()
    msb.append(s"${ind()}modifier ")
    msb.append(modifierName(callerSet))
    msb.append(" { \n")
    curIndent += 1
    val hasRoles = callerSet.map(cs => s"UTIL.hasRole(msg.sender, UTIL.Role.$cs)")
    msb.append(s"${ind()}require(${hasRoles.mkString(" || ")});")
    curIndent -= 1
    msb.append(s"\n ${ind()}_;\n${ind()}} \n\n")
    msb.result()

object SolidityGenerator:

  def typeToSolString(t: ModelType): String =
    t match
      case AccountType => "address"
      case ArrayType(t) => s"${typeToSolString(t)}[]"
      case MappingType(fromType, toType) => s"mapping(${typeToSolString(fromType)}=>${typeToSolString(toType)})"
      case BoolType => "bool"
      case numberType: NumberType => numberTypeToSolString(numberType)
      case StringType => "string"
      case u: UserDefinedType => "UTIL." + u.name
      case EXPR_ERR_TYPE(err) => "EXPR_ERR_TYPE"

  // for function parameters, we need to specify calldata memory location for complex types
  private def paramTypeToSolString(t: ModelType): String =
    t match
      case ArrayType(_) | MappingType(_,_) | StringType => s"${typeToSolString(t)} calldata"
      case u: UserDefinedType => s"${typeToSolString(t)} calldata"
      case _ => typeToSolString(t)

  private def numberTypeToSolString(numberType: NumberType): String =
    numberType match
      case IntType => "int"
      case UintType => "uint"