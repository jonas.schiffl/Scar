package edu.kit.kastel.formal.scar.generate

import edu.kit.kastel.formal.scar.model.{Contract, Fun, ModifyCapa, QualifiedFun}

import scala.collection.mutable

trait SpecGenerator {

  val exceptions: mutable.Set[String] = mutable.Set.empty

  def getFunSpecString(f: Fun, ind: String): String
  
  def getFunFrameString(qf: QualifiedFun, modCapas: Set[ModifyCapa], payable: Boolean, ind: String): String

  def getInitSpecString(c: Contract, ind: String): String

}
