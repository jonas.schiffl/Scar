package edu.kit.kastel.formal.scar.generate

import edu.kit.kastel.formal.scar.model.SCApp

object TestOutputGenerator {

  def printShortTestString(scapp: SCApp): Unit =
    println(s"app name: ${scapp.name}")
    for c <- scapp.contracts do
      println(s"contract ${c.name}")
      for sv <- c.stateVars do
        println(s"statevar ${sv.name}: ${sv.t}")
      for f <- c.functions do
        println(s"function ${f.name}")

}
