package edu.kit.kastel.formal.scar.main

import edu.kit.kastel.formal.scar.analysis.*
import edu.kit.kastel.formal.scar.generate.{ScarGenerator, SolcVerifySolSpecGenerator, SolidityGenerator}
import edu.kit.kastel.formal.scar.translation.Text2scapp.{generateSCApp, translateFromFile}
import edu.kit.kastel.formal.scar.verification.{findErrors, integrateErrors, readErrorsJson, writeErrorsJson}
import scopt.OParser

import java.nio.file.{Files, Path}
import scala.jdk.CollectionConverters.ListHasAsScala
import scala.util.{Failure, Success}
import edu.kit.kastel.formal.scar.generate.NuxmvGenerator


object Main:
  def main(args: Array[String]): Unit =
    OParser.parse(parser, args, null) match
      case Some(config) => config match
        case GenerateConfig(scar, skeletonTarget) =>
          if skeletonTarget != null then
            writeSkeleton(scar, skeletonTarget)
          else generateSolidity(scar)
        case AnalyzeConfig(scar, implementation, errorTarget, docker) =>
          writeErrors(scar, implementation, errorTarget, Option(docker))
        case IntegrateConfig(scar, errorSource, scarTarget) =>
          writeIntegrated(scar, errorSource, scarTarget)
        case CheckCapasConfig(scar) =>
          checkCapas(scar)
        case CheckScarConfig(scar, print) =>
          checkScar(scar, print)
        case NuXmvTranslateConfig(scar, outPath) => translate2NuXmv(scar, outPath)
        case null => print(OParser.usage(parser))
      case _ => () // arguments are bad, error message will have been displayed

def writeSkeleton(scar: Path, target: Path) =
  val gen = SolidityGenerator(generateSCApp(scar), Some(SolcVerifySolSpecGenerator))
  Files.write(target, gen.getString.result.getBytes)

def generateSolidity(scar: Path) =
  if scar.toFile.isDirectory then
    Files.list(scar).forEach { file =>
      if file.toString.endsWith(".scar") then
        val gen = SolidityGenerator(generateSCApp(file), Some(SolcVerifySolSpecGenerator))
        Files.write(getFileOfType(file, ".sol"), gen.getString.result.getBytes)
    }
  else
    val gen = SolidityGenerator(generateSCApp(scar), Some(SolcVerifySolSpecGenerator))
    Files.write(getFileOfType(scar, ".sol"), gen.getString.result.getBytes)

def writeErrors(scar: Path, implementation: Path, errorTarget: Path, docker: Option[Path] = None) =
  val model = generateSCApp(scar)
  val lines = Files.readAllLines(implementation).asScala.toSeq
  val errors = findErrors(model, lines, docker)
  Files.write(errorTarget, writeErrorsJson(errors).getBytes) 


def writeIntegrated(scar: Path, errorSource: Path, targetScar: Path) =
  val model = generateSCApp(scar)
  val errors = readErrorsJson(Files.readString(errorSource))
  val integratedModel = integrateErrors(model, errors)
  Files.write(targetScar, ScarGenerator(integratedModel).getString.result.getBytes)

def checkCapas(path: Path) : Unit =
  val sc = translateFromFile(path.toString)
  print("Translating SCAR file...")
  sc match
    case Failure(exception) => println(exception.getMessage)
    case Success(res) =>
      println("SCAR file translated successfully.")
      println("Checking SCAR capability consistency...")
      val anaRes = CallAnalysis(res).analyse ++ TxAnalysis(res).analyse ++ ModifiesAnalysis(res).analyse
      if anaRes.isEmpty then
        println("Success! SCAR model is consistent.")
      else
        println("Errors found in SCAR model:")
        anaRes.foreach(r => println(r.errorMsg))


def checkScar(scar: Path, print: Boolean) : Unit =
  val res = translateFromFile(scar.toString)
  println("Checking SCAR specification... \n")
  res match
    case Failure(exception) => println(exception.getMessage)
    case Success(suc) =>
      println("Success! SCAR specification is syntactically correct.")
      if print then
        println("Printing SCAR specification...")
        println(ScarGenerator(suc).getString)

def translate2NuXmv(scar: Path, skeletonTarget: Path) =
  val gen = NuxmvGenerator(generateSCApp(scar), 16, 16)
  Files.write(skeletonTarget, gen.getString.result.getBytes)

private sealed trait Config
private case class GenerateConfig(scar: Path, skeletonTarget: Path) extends Config
private case class NuXmvTranslateConfig(scar: Path, skeletonTarget: Path) extends Config
private case class AnalyzeConfig(scar: Path, implementation: Path, errorTarget: Path, docker: Path) extends Config
private case class IntegrateConfig(scar: Path, errorSource: Path, scarTarget: Path) extends Config
private case class CheckScarConfig(scar: Path, print: Boolean) extends Config
private case class CheckCapasConfig(scar: Path) extends Config

private val builder = OParser.builder[Config]
private val parser: OParser[Unit, Config] =
  import builder.*
  OParser.sequence(
    programName("scar"),
    head("scar", "0.1"),
    cmd("translate")
      .children(
        cmd("nuxmv")
          .action((_, _) => NuXmvTranslateConfig(null, null))
          .text("Translate a SCAR specification to NuXmv")
          .children(
            arg[Path]("<scar>")
              .action((x, c) => c.asInstanceOf[NuXmvTranslateConfig].copy(scar = x)),
            arg[Path]("<outputPath>")
              .action((x, c) => c.asInstanceOf[NuXmvTranslateConfig].copy(skeletonTarget = x))
          ),
      ),
    cmd("align")
      .action((_, _) => GenerateConfig(null, null))
      .text("Generate a Solidity skeleton from a SCAR specification")
      .children(
        arg[Path]("<scar>")
          .action((x, c) => c.asInstanceOf[GenerateConfig].copy(scar = x)),
        arg[Path]("<outputPath>")
          .action((x, c) => c.asInstanceOf[GenerateConfig].copy(skeletonTarget = x))
      ),
    cmd("generateSolidity")
      .action((_, _) => GenerateConfig(null, null))
      .text("Generate Solidity files from SCAR files. Existing files will be overwritten. Translates all files if <scar> is a folder.")
      .children(
        arg[Path]("<scar>")
          .action((x, c) => c.asInstanceOf[GenerateConfig].copy(scar = x))
      ),
    cmd("analyze")
      .action((_, _) => AnalyzeConfig(null, null, null, null))
      .text("Find the errors in a Solidity implementation against a SCAR specification")
      .children(
        arg[Path]("<scar>")
          .action((x, c) => c.asInstanceOf[AnalyzeConfig].copy(scar = x)),
        arg[Path]("<implementation>")
          .action((x, c) => c.asInstanceOf[AnalyzeConfig].copy(implementation = x)),
        arg[Path]("<errorTarget>")
          .action((x, c) => c.asInstanceOf[AnalyzeConfig].copy(errorTarget = x)),
        arg[Path]("<docker>")
          .action((x, c) => c.asInstanceOf[AnalyzeConfig].copy(docker = x))
          .text("Path to the docker executable, not necessary if docker is in PATH")
          .optional()
      ),
    cmd("integrate")
      .action((_, _) => IntegrateConfig(null, null, null))
      .text("Resolve given errors in a Solidity implementation against a SCAR specification " +
        "and integrate them into the specification to match the implementation")
      .children(
        arg[Path]("<scar>")
          .action((x, c) => c.asInstanceOf[IntegrateConfig].copy(scar = x)),
        arg[Path]("<errorSource>")
          .action((x, c) => c.asInstanceOf[IntegrateConfig].copy(errorSource = x)),
        arg[Path]("<scarTarget>")
          .action((x, c) => c.asInstanceOf[IntegrateConfig].copy(scarTarget = x))
      ),
    cmd("capa-check")
      .action((_, _) => CheckCapasConfig(null))
      .text("Check whether a SCAR model with capabilities is consistent")
      .children(
        arg[Path]("<scar>")
          .action((x, c) => c.asInstanceOf[CheckCapasConfig].copy(scar = x))
      ),
    cmd("check")
      .action((_, _) => CheckScarConfig(null, false))
      .text("Check a SCAR specification for syntactic correctness")
      .children(
        arg[Path]("<scar>")
          .action((x, c) => c.asInstanceOf[CheckScarConfig].copy(scar = x)),
        opt[Unit]('p', "print")
        .action((_, c) => c.asInstanceOf[CheckScarConfig].copy(print = true))
        .text("Print the model if parsing is successful")
      )
  )

private def getFileOfType(file: Path, newExtension: String): Path =
  val fileName = file.getFileName.toString
  val newName = fileName.substring(0, fileName.lastIndexOf('.')) + newExtension
  file.resolveSibling(newName)