package edu.kit.kastel.formal.scar.model

import scala.collection.mutable

case class Capabilities(roles: Set[Role],
                        callCapas: Set[CallCapa],
                        modCapas: Set[ModifyCapa],
                        txCapas: Set[TransferCapa])

case class Role(name: String)

enum Actor:
  case Fun(f: QualifiedFun)
  case Acc(acc: Account)
  case Ro(r: Role)
  override def toString: String = this match
    case Fun(f) => s"function ${f.contractName}.${f.funName}"
    case Acc(a) => s"account $a"
    case Ro(r) => s"role ${r.name}"

enum AccountSet:
  case ROLE(r: Role)
  case ACCOUNT(a: Account)
  case ANY
  case SELF
  case CALLER

sealed abstract class Capability(val actor: Actor)

/**
 * A capability to call a function
 * @param actor the actor that is allowed to call the function
 * @param callee the function that can be called
 */
case class CallCapa(override val actor: Actor, callee: QualifiedFun) extends Capability(actor)

/**
 * A capability to modify a location
 * @param actor the actor that is allowed to modify the location
 * @param loc the location that can be modified
 */
case class ModifyCapa(override val actor: Actor, loc: Location) extends Capability(actor)

/**
  * A capability to transfer tokens
  * @param actor the actor that is allowed to transfer tokens
  * @param to the recipients of the transfer
  * @param limit the maximum amount of tokens that can be transferred
  */
case class TransferCapa(override val actor: Actor, from: AccountSet, to: AccountSet, limit: Expr) extends Capability(actor)

/**
 * Computes the set of roles that are allowed to call a given function
 * @param fun the function
 * @param c the capability set
 * @return the set of roles that are allowed to call fun
 */
def getCallingRolesForFun(fun: QualifiedFun, c: Capabilities): Set[Role] =
  val callers = c.callCapas.filter(call => call.callee.funName == fun.funName)
  val roles: mutable.Set[Role] = mutable.Set()
  for c <- callers do
    c.actor match
      case Actor.Fun(f) => ()
      case Actor.Acc(acc) => ()
      case Actor.Ro(r) => roles.add(r)
  roles.toSet

/**
 * Computes the set of all locations a function is allowed to modify for given capabilities
 * @param fun the function
 * @param c the capability set
 * @return the set of locations fun is allowed to modify
 */
def getFunctionFrame(fun: QualifiedFun, c: Capabilities): Set[Location] =
  c.modCapas.filter(mc => mc.actor == Actor.Fun(fun)).map(mc => mc.loc)