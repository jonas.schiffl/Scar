package edu.kit.kastel.formal.scar.model

import edu.kit.kastel.formal.scar.generate.DefaultSpecGenerator

/**
 * This class models expressions which occur in the Scar specification languages.
 * Each expression has a @ModelType and possibly a String describing its position
 * in the model text file
 */

sealed abstract class Expr extends Ordered[Expr] {
  val t: ModelType
  val pos: String
  override def toString: String = DefaultSpecGenerator.e2s(this)

  def precedence: Int = this match
    case _: UnaryMinusExpr => 1
    case _: BinArithExpr => 2
    case _: CompExpr => 3
    case _: LogicNotExpr => 4
    case _: LandExpr => 5
    case _: LorExpr => 6
    case _: ImpExpr => 7
    case _ => 0
  
  def compare(that: Expr): Int = this.precedence - that.precedence

  def arity: Int = this match
    case _: UnaryMinusExpr => 1
    case _: LogicNotExpr => 1
    case _: BinArithExpr => 2
    case _: CompExpr => 2
    case _: LandExpr => 2
    case _: LorExpr => 2
    case _: ImpExpr => 2
    case _ => 0
  
  /**
   * @return true if this expression binds stronger than that expression
   */
  def bindsStrongerOrEqual(that: Expr): Boolean =
    this.compare(that) <= 0
}

case class VarExpr(name: String, t: ModelType, pos: String) extends Expr

abstract class QuantifiedExpr(qVar: VarExpr, matrix: Expr, pos: String) extends Expr {
  override val t: ModelType = BoolType
}

abstract class SetQuantifiedExpr(qVar: VarExpr, set: Expr, matrix: Expr, pos: String) extends Expr {
  override val t: ModelType = BoolType
}

case class ExistsExpr(qVar: VarExpr, matrix: Expr, pos: String) extends QuantifiedExpr(qVar, matrix, pos)
case class ForallExpr(qVar: VarExpr, matrix: Expr, pos: String) extends QuantifiedExpr(qVar, matrix, pos)

case class SetForallExpr(qVar: VarExpr, set: Expr, matrix: Expr, pos: String)
  extends SetQuantifiedExpr(qVar, set, matrix, pos)

case class SetExistsExpr(qVar: VarExpr, set: Expr, matrix: Expr, pos: String)
  extends SetQuantifiedExpr(qVar, set, matrix, pos)

case class ArrayAccExpr(arr: Expr, t: ModelType, index: Expr, pos: String) extends Expr
case class ArrayFunAccExpr(arr: Expr, t: ModelType, index: Expr, pos: String) extends Expr
case class MappingAccExpr(mapping: Expr, t: ModelType, index: Expr, pos: String) extends Expr
case class MappingFunAccExpr(mapping: Expr, t: ModelType, index: Expr, pos: String) extends Expr

case class EnumConstExpr(enumName: String, constName: String, t: ModelType, pos:String) extends Expr
case class StructAccExpr(structExpr: Expr, t: ModelType, structFieldName: String, pos:String) extends Expr

case class FunCallExpr(funExpr: Expr, params: List[Expr], t: ModelType, pos: String) extends Expr

// contract members, i.e., state variables and functions
case class MemberAccessExpr(contractExpr: Expr, memberName: String, t: ModelType, pos: String) extends Expr

case class UnaryMinusExpr(e: Expr, pos: String) extends Expr {override val t: ModelType = IntType}

sealed trait BinArithExpr(val l: Expr, val r: Expr, val t: ModelType, val pos: String) extends Expr
case class MulExpr(override val l: Expr,
                   override val r: Expr,
                   override val t: ModelType,
                   override val pos: String) extends BinArithExpr(l, r, t, pos)
case class DivExpr(override val l: Expr,
                   override val r: Expr,
                   override val t: ModelType,
                   override val pos: String) extends BinArithExpr(l, r, t, pos)
case class ModExpr(override val l: Expr,
                   override val r: Expr,
                   override val t: ModelType,
                   override val pos: String) extends BinArithExpr(l, r, t, pos)
case class AddExpr(override val l: Expr,
                   override val r: Expr,
                   override val t: ModelType,
                   override val pos: String) extends BinArithExpr(l, r, t, pos)
case class SubExpr(override val l: Expr,
                   override val r: Expr,
                   override val t: ModelType,
                   override val pos: String) extends BinArithExpr(l, r, t, pos)

sealed trait CompExpr(val l: Expr, val r: Expr, val pos: String) extends Expr {override val t: ModelType = BoolType}
case class LtExpr(override val l: Expr, override val r: Expr, override val pos: String) extends CompExpr(l, r, pos)
case class GtExpr(override val l: Expr, override val r: Expr, override val pos: String) extends CompExpr(l, r, pos)
case class LeqExpr(override val l: Expr, override val r: Expr, override val pos: String) extends CompExpr(l, r, pos)
case class GeqExpr(override val l: Expr, override val r: Expr, override val pos: String) extends CompExpr(l, r, pos)

case class EqExpr(l: Expr,r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}
case class NeqExpr(l: Expr,r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}

case class RefEqExpr(l: Expr, r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}

case class RefNeqExpr(l: Expr, r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}

case class LogicNotExpr(e: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}
case class LandExpr(l: Expr, r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}
case class LorExpr(l: Expr, r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}
case class ImpExpr(l: Expr, r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}

case class OldExpr(e: Expr, t: ModelType, pos: String) extends Expr

case class SumExpr(t: NumberType, set: Expr, pos: String) extends Expr
case class KeysExpr(e: Expr, t: SetType, pos: String) extends Expr
case class ValuesExpr(e: Expr, t: SetType, pos: String) extends Expr
case class SizeExpr(e: Expr, pos: String) extends Expr {override val t: ModelType = UintType}

case class HashExpr(e: Expr, pos: String) extends Expr {override val t: ModelType = IntType}

case class CallerExpr(pos: String) extends Expr {override val t: ModelType = AccountType}

case class ResultExpr(t: ModelType, pos: String) extends Expr

case class ThisExpr(pos: String) extends Expr {override val t: ModelType = AccountType}

case class AmtExpr(pos: String) extends Expr {override val t: ModelType = UintType}

case class SelfExpr(pos: String) extends Expr {override val t: ModelType = AccountType}

case class BlocknumExpr(pos: String) extends Expr {override val t: ModelType = UintType}

case class SystimeExpr(pos: String) extends Expr {override val t: ModelType = UintType}

case class SendExpr(fromExpr: Expr, toExpr: Expr, amtExpr: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}

trait LiteralExpression(t: ModelType, pos: String) extends Expr
case class NumberLiteralExpression[T <: NumberType](value: Long, t: T, pos: String) extends LiteralExpression(t, pos)

case class StringLiteralExpression(value: String, pos: String) extends LiteralExpression(StringType, pos) {
  override val t: ModelType = StringType
}
case class BoolLiteralExpression(value: Boolean, pos: String) extends LiteralExpression(BoolType, pos) {
  override val t: ModelType = BoolType
}

// Temporal Expressions
case class EnabledExpr(
    account: Option[Expr], paramCond: Option[Expr], amt: Option[Expr], expr: Expr, pos: String
  ) extends Expr {override val t: ModelType = BoolType}
case class EnabledUntilExpr(
    account: Option[Expr], paramCond: Option[Expr], amt: Option[Expr], expr: Expr, pos: String
  ) extends Expr {override val t: ModelType = BoolType}
case class TxExpr(
    fun: QualifiedFun, caller: Option[Expr], paramCond: Option[Expr], amt: Option[Expr], pos: String
  ) extends Expr {override val t: ModelType = BoolType}
case class OwnsExpr(account: Expr, amtExpr: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}
case class BoxExpr(expr: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}
case class WeakUntilExpr(l: Expr, r: Expr, pos: String) extends Expr {override val t: ModelType = BoolType}

// Location Types
case class StarLocExpr(t: ModelType, pos: String) extends Expr
case class RangeLocExpr(from: Expr, to: Expr, t: ModelType, pos: String) extends Expr
case class IndexLocExpr(index: Expr, t: ModelType, pos: String) extends Expr

case class ERR_EXPR(msg: String, pos: String) extends Expr {
  override val t: ModelType = EXPR_ERR_TYPE(msg + " at " + pos)
}

