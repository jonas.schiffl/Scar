package edu.kit.kastel.formal.scar.model

sealed trait Location():
  def isLocSubsetOf(upper: Location): Boolean =
    this match
      case VarLoc(v) => upper == this
      case ArrAllLoc(arr) => upper == this
      case MapAllLoc(map) => upper == this
      case StructAllLoc(struct) => upper == this
      case StructVarLoc(struct, varname) => upper == this
      case ArrElementLoc(arr, index) => upper match
        case ArrElementLoc(arr, upperIndex) => equalIndices(index, upperIndex)
        case ArrAllLoc(arr) => true
        case _ => false
      case MapElementLoc(map, index) => upper match
        case MapElementLoc(map, upperIndex) => equalIndices(index, upperIndex)
        case MapAllLoc(map) => true
        case _ => false
      case ArrRangeLoc(arr, from, to) => upper match
        case ArrElementLoc(arr, index) => indexInRange(index, from, to)
        case ArrRangeLoc(arr, upperFrom, upperTo) => rangeInRange(from, to, upperFrom, upperTo)
        case ArrAllLoc(arr) => true
        case _ => false

  private def indexInRange(index: Expr, from: Expr, to: Expr): Boolean =
    if index == from || index == to then
      true
    else
      index match
        case NumberLiteralExpression(ival, _, _) => from match
          case NumberLiteralExpression(fromval, _, _) => to match
            case NumberLiteralExpression(toval, _, _) => fromval <= ival && ival <= toval
            case _ => false
          case _ => false
        case _ => false


  private def rangeInRange(from: Expr, to: Expr, upperFrom: Expr, upperTo: Expr): Boolean =
    if upperFrom == from && upperTo == to then
      true
    else
      from match
        case NumberLiteralExpression(fval, _, _) => upperFrom match
          case NumberLiteralExpression(ufval, _, _) => to match
            case NumberLiteralExpression(toval, _, _) => upperTo match
              case NumberLiteralExpression(utoval, _, _) => ufval <= fval && toval <= utoval
              case _ => false
            case _ => false
          case _ => false
        case _ => false

case class VarLoc(v: QualifiedStateVar) extends Location
case class ArrElementLoc(arr: QualifiedStateVar, index: Expr) extends Location
case class MapElementLoc(map: QualifiedStateVar, index: Expr) extends Location
case class ArrAllLoc(arr: QualifiedStateVar) extends Location
case class MapAllLoc(map: QualifiedStateVar) extends Location
case class ArrRangeLoc(arr: QualifiedStateVar, from: Expr, to: Expr) extends Location
case class StructAllLoc(struct: QualifiedStateVar) extends Location
case class StructVarLoc(struct: QualifiedStateVar, varname: String) extends Location


def equalIndices(i1: Expr, i2: Expr): Boolean =
  if i1.t != i2.t then
    return false
  if i1 == i2 then
    return true
  val res = i1 match
    case SelfExpr(_) => i2 match
      case SelfExpr(_) | CallerExpr(_) => true
      case _ => false
    case CallerExpr(_) => i2 match
      case SelfExpr(_) | CallerExpr(_) => true
      case _ => false
    case _ => false
  res