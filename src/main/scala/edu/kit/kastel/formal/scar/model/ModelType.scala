package edu.kit.kastel.formal.scar.model

abstract class ModelType

sealed abstract class NumberType extends ModelType
object IntType extends NumberType
object UintType extends NumberType
object BoolType extends ModelType
object StringType extends ModelType
object AccountType extends ModelType
case class ArrayType(t: ModelType) extends ModelType
case class MappingType(fromType: ModelType, toType: ModelType) extends ModelType

case class SetType(t: ModelType) extends ModelType

abstract class UserDefinedType(val name: String) extends ModelType
case class ContractType(override val name: String, varNames: Set[String], funNames: Set[String]) extends UserDefinedType(name: String)
case class EnumType(override val name: String, consts: Set[String]) extends UserDefinedType(name: String)
case class StructType(override val name: String, vars: Set[StateVar]) extends UserDefinedType(name: String)

/**
 * A type for the expressions used in frame conditions.
 * Expressions of this type cannot occur outside of a *modifies* clause.
 */
object LocType extends ModelType

case class EXPR_ERR_TYPE(err: String) extends ModelType