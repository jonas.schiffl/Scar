package edu.kit.kastel.formal.scar.model

import upickle.default.*
import upickle.default.ReadWriter.join

import scala.util.{Failure, Success, Try}

class Account(val name: String)

/** An object representing one smart contract.
 *
 * @param name name of the contract
 * @param stateVars state variables of the contract
 * @param functions functions of the contract
 * @param initCond initial conditions of the contract
 */
case class Contract(override val name: String,
                    stateVars: Set[StateVar],
                    invariants: Set[Expr],
                    functions: Set[Fun],
                    initCond: (Set[Param], Set[Expr])) extends Account(name)

/** An object representing a smart contract application.
 *
 * @param name name of the application
 * @param userDefTypes user-defined types in the application
 * @param contracts contracts in the application
 * @param capas capabilities of the application
 */
case class SCApp(name: String,
                userDefTypes: UserDefinedTypes,
                contracts: Set[Contract],
                capas: Capabilities,
                tempProps: Set[Expr])

case class UserDefinedTypes(enumTypes: Set[EnumType], structTypes: Set[StructType], contractTypes: Set[ContractType])

case class ExtAccount(override val name: String) extends Account(name)

/** An object representing a (complete) function, with signature and contract.
 *
 * @param name name of the function
 * @param retType return type of the function
 * @param params parameters of the function
 * @param spec specification of the function
 * @param payable whether the function is payable
 */
case class Fun(name: String, retType: Option[ModelType], params: Set[Param], spec: FunSpec)

/** A function represented by its name and the contract it belongs to.
 *
 * @param funName name of the function
 * @param contractName name of the contract
 */
case class QualifiedFun(funName: String, contractName: String) derives ReadWriter:
  override def toString: String = s"$contractName.$funName"

case class StateVar(name: String, t: ModelType)
case class Param(name: String, t: ModelType)
case class QualifiedStateVar(varName: String, contractName: String) derives ReadWriter:
  override def toString: String = s"$contractName.$varName"


/** pre- and postcondition of a function.
 *
 * @param pre precondition, with the intended meaning that a function which is called in
 *            a state that does not fulfill this condition reverts without changing the state.
 * @param post postcondition, with the intended meaning that a function which is called in
 *             a state that fulfills the precondition will terminate in a state satisfying
 *             the postcondition.
 */
case class FunSpec(pre: Set[Expr], post: Set[Expr])

def getContractByName(name: String, scapp: SCApp) : Option[Contract] =
  scapp.contracts.find(c => c.name == name)

def getFunctionByName(name: String, contract: Contract) : Option[Fun] =
  contract.functions.find(f => f.name == name)

def getStateVarByName(name: String, contract: Contract): Option[StateVar] =
  contract.stateVars.find(sv => sv.name == name)

def findQualFun(funName: String, conName: String, contracts: Set[Contract]): Try[QualifiedFun] =
  contracts.find(c => c.name == conName) match
    case None => Failure(Exception(s"Contract $conName not found when looking for $conName.$funName"))
    case Some(c) =>
      c.functions.find(f => f.name == funName) match
        case None => Failure(Exception(s"Function $funName of contract $conName not found"))
        case Some(value) => Success(QualifiedFun(funName, conName))

def findQualVar(varName: String, conName: String, contracts: Set[Contract]): Try[QualifiedStateVar] =
  contracts.find(c => c.name == conName) match
    case None => Failure(Exception(s"Contract $conName not found"))
    case Some(c) =>
      c.stateVars.find(sv => sv.name == varName) match
        case None => Failure(Exception(s"Variable $varName of contract $conName not found"))
        case Some(value) => Success(QualifiedStateVar(varName, conName))