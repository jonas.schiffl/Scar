package edu.kit.kastel.formal.scar.model

case class SCEnv(knownAccounts: Set[SCApp])

case class FunCall(caller: Account, value: Int, time: Int, fun: Fun, contract: Contract, params: Set[Param])


