package edu.kit.kastel.formal.scar.smt

import edu.kit.kastel.formal.scar.model.*
import z3.scala.*

import scala.collection.mutable

class Scapp2z3Translator(_z3: Z3Context):

  val errors: mutable.Set[String] = mutable.Set()
  private val z3 = _z3
  private val intSort = z3.mkIntSort()
  private val boolSort = z3.mkBoolSort()
  private val errorSort = z3.mkUninterpretedSort("ERROR")

  //  def scapp2SMTAST(scapp: SCApp): Z3AST =

  def expr2SMT(expr: Expr): Z3AST =
    try {

      expr match
      case VarExpr(name, t, pos) => z3.mkConst(name, type2sort(t))
      case NumberLiteralExpression(value, t, pos) => z3.mkInt(value.toInt, intSort)
      case BoolLiteralExpression(value, pos) => if value then z3.mkTrue() else z3.mkFalse()
      case ArrayAccExpr(arr, t, index, pos) => z3.mkSelect(expr2SMT(arr), expr2SMT(index))
      case MappingAccExpr(mapping, t, index, pos) => z3.mkSelect(expr2SMT(mapping), expr2SMT(index))
      case UnaryMinusExpr(e, pos) => z3.mkUnaryMinus(expr2SMT(e))
      case expr: BinArithExpr => expr match
        case MulExpr(l, r, t, pos) => z3.mkMul(expr2SMT(l), expr2SMT(r))
        case DivExpr(l, r, t, pos) => z3.mkDiv(expr2SMT(l), expr2SMT(r))
        case ModExpr(l, r, t, pos) => z3.mkMod(expr2SMT(l), expr2SMT(r))
        case AddExpr(l, r, t, pos) => z3.mkAdd(expr2SMT(l), expr2SMT(r))
        case SubExpr(l, r, t, pos) => z3.mkSub(expr2SMT(l), expr2SMT(r))
      case expr: CompExpr => expr match
        case LtExpr(l, r, pos) => z3.mkLT(expr2SMT(l), expr2SMT(r))
        case GtExpr(l, r, pos) => z3.mkGT(expr2SMT(l), expr2SMT(r))
        case LeqExpr(l, r, pos) =>
        case GeqExpr(l, r, pos) => z3.mkGE(expr2SMT(l), expr2SMT(r))
      case EqExpr(l, r, pos) => z3.mkEq(expr2SMT(l), expr2SMT(r))
      case NeqExpr(l, r, pos) => z3.mkNot(z3.mkEq(expr2SMT(l), expr2SMT(r)))
      case LogicNotExpr(e, pos) => z3.mkNot(expr2SMT(e))
      case LandExpr(l, r, pos) => z3.mkAnd(expr2SMT(l), expr2SMT(r))
      case LorExpr(l, r, pos) => z3.mkOr(expr2SMT(l), expr2SMT(r))
      case ImpExpr(l, r, pos) => z3.mkImplies(expr2SMT(l), expr2SMT(r))
      case OldExpr(_,_,_)
            | CallerExpr(_)
            | AmtExpr(_)
            | SelfExpr(_)
            | EnumConstExpr(_, _, _, _)
            | StringLiteralExpression(_,_)
            | StructAccExpr(_, _, _, _)
            | MemberAccessExpr(_, _, _, _)
            | FunCallExpr(_, _, _, _)
            | StarLocExpr(_, _)
            | RangeLocExpr(_,_,_,_)
            | IndexLocExpr(_,_,_)
            | ForallExpr(_,_,_)
            | ExistsExpr(_,_,_) =>
        errors.add(s"expr2SMT: translating $expr not implemented")
        z3.mkConst("ERROR", errorSort)
      case _ =>
        errors.add(s"expr2SMT: unexpected expression $expr")
        z3.mkConst("ERROR", errorSort)
    } catch
      case e: Exception => e.printStackTrace()
    z3.mkTrue()

  private def type2sort(t: ModelType): Z3Sort =
    t match
      case IntType | UintType => intSort
      case BoolType => boolSort
      case ArrayType(t) => z3.mkArraySort(intSort, type2sort(t))
      case MappingType(fromType, toType) =>
        z3.mkArraySort(type2sort(fromType), type2sort(toType))
      case x =>
        errors.add(s"Model Type to SMT Sort conversion failed: Not yet implemented or unknown type $x")
        errorSort
