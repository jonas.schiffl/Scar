package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.antlr.SCAppBaseVisitor
import edu.kit.kastel.formal.scar.antlr.SCAppParser.{ElementaryTypeNameContext, TypenameContext}
import edu.kit.kastel.formal.scar.model.*
import org.antlr.v4.runtime.ParserRuleContext

import scala.collection.mutable

/**
 * This class inherits from the antlr-generated parser.
 * It offers functionality that all parsers need, like translating types.
 * @tparam T the return type
 */
class AbstractScappVisitor[T] extends SCAppBaseVisitor[T] {

  val exceptions: mutable.Set[String] = mutable.Set()

  def parserTypeToModelType(ctx: TypenameContext, udts: UserDefinedTypes): ModelType =
    if ctx.arrType != null then
      ArrayType(parserTypeToModelType(ctx.arrType, udts))
    else if ctx.MAPPING() != null then
      MappingType(parserTypeToModelType(ctx.fromType, udts), parserTypeToModelType(ctx.toType, udts))
    else if ctx.elementaryTypeName != null then
      parseElementaryType(ctx.elementaryTypeName)
    else if  ctx.userDefTypeName != null then
      val name = ctx.userDefTypeName.getText
      (udts.enumTypes ++ udts.structTypes ++ udts.contractTypes).find(e => e.name == name) match
        case Some(a) => a
        case None =>
          exceptions.add(s"Unknown type: ${ctx.getText}")
          EXPR_ERR_TYPE(s"Unknown type: ${ctx.getText}")
      else
        exceptions.add(s"Unknown type: ${ctx.getText}")
        EXPR_ERR_TYPE(s"Unknown type: ${ctx.getText}")

  def parseElementaryType(ctx: ElementaryTypeNameContext): ModelType =
    if ctx.INT() != null then
      return IntType
    else if ctx.UINT() != null then
      return UintType
    else if ctx.BOOL() != null then
      return BoolType
    else if ctx.STRING() != null then
      return StringType
    else if ctx.ACCOUNT() != null then
      return AccountType
    else
      exceptions.add(s"Unknown type: ${ctx.getText}")
    EXPR_ERR_TYPE(s"Unknown type: ${ctx.getText}")

  def ctxPos(ctx: ParserRuleContext): String =
    s"line ${ctx.start.getLine}, col ${ctx.start.getCharPositionInLine}"

}
