package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.model.{Account, Actor, Capabilities, Contract, QualifiedFun, Role}

import scala.collection.mutable

/**
 * Checks whether the capabilities of a SCApp object make sense *syntactically*,
 * e.g., whether elements of the given name actually exist.
 * This class does NOT perform semantic analysis.
 * @param contracts the contracts of the app
 * @param capas the capabilities to check
 */
class CapabilitiesChecker(contracts: Set[Contract], capas: Capabilities):

  val errors: mutable.Set[String] = mutable.Set()
  private val roles = capas.roles

  def check(): Set[String] =
    for call <- capas.callCapas do
      call.actor match
        case Actor.Fun(f) => checkFun(f)
        case Actor.Acc(acc) => checkAcc(acc)
        case Actor.Ro(r) => checkRole(r)
    for tx <- capas.txCapas do
      () // TODO
    for mod <- capas.modCapas do
      () // TODO
    errors.toSet

  private def checkFun(f: QualifiedFun): Unit =
    contracts.find(c => c.name == f.contractName) match
      case None => addErr(s"Unknown contract ${f.contractName}")
      case Some(c) =>
        c.functions.find(g => g.name == f.funName) match
          case None => addErr(s"Unknown function ${f.funName} in contract ${c.name}")
          case Some(_) => ()

  private def checkRole(r: Role): Unit =
    capas.roles.find(r2 => r2.name == r.name) match
      case None => addErr(s"Unknown role ${r.name}")
      case Some(c) => ()

  private def checkAcc(acc: Account): Unit =
    ()

  private def addErr(msg: String): Unit =
    errors.add(s"Capabilities Checker: $msg")

