package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.antlr.SCAppParser
import edu.kit.kastel.formal.scar.model
import edu.kit.kastel.formal.scar.model.AccountSet.*
import edu.kit.kastel.formal.scar.model.*

import scala.collection.mutable
import scala.jdk.CollectionConverters.*
import scala.util.{Failure, Success, Try}

/**
 * Translates a string representing a capability specification. The translation results in sets of
 * call, modify, and transfer capabilities, which can be assembled into a Capabilities object.
 * @param contracts the set of contracts in the application
 * @param roles the set of roles in the application
 */
class CapabilitiesTranslator(contracts: Set[Contract], roles: Set[Role]) extends AbstractScappVisitor[Try[Location]] {

  def translateCapa(a: Actor, capaString: String, context: SpecContext): Try[Capabilities] =
    val modCapas: mutable.Set[ModifyCapa] = mutable.Set()
    val callCapas: mutable.Set[CallCapa] = mutable.Set()
    val txCapas: mutable.Set[TransferCapa] = mutable.Set()
    val ctx = Text2scapp.parseCapaString(capaString)

    for tx <- ctx.txs.asScala do
      for t <- tx.transfers.asScala do
        translateTxCapa(a, t, context) match
          case Some(tx) => txCapas.add(tx)
          case None => ()
    for mod <- ctx.mods.asScala do
      for l <- mod.locs.asScala do
        val loc = visit(l)
        loc match
          case Failure(e) => exceptions.add(e.getMessage)
          case Success(l) => modCapas.add(ModifyCapa(a, l))
    for call <- ctx.calls.asScala do
      for c <- call.callees.asScala do
        val calleeName = c.funName.getText
        val calleeContractName = c.contractName.getText
        findQualFun(calleeName, calleeContractName, contracts) match
          case Failure(exception) => exceptions.add(exception.getMessage)
          case Success(qf) => callCapas.add(model.CallCapa(a, qf))
    if exceptions.nonEmpty then
      Failure(Exception(exceptions.mkString("\n")))
    else
      val capas = Capabilities(roles, callCapas.toSet, modCapas.toSet, txCapas.toSet)
      val checkerErrors = CapabilitiesChecker(contracts, capas).check()
      if checkerErrors.nonEmpty then
        Failure(Exception(checkerErrors.mkString("\n")))
      else
        Success(capas)

  private def translateTxCapa(a: Actor, t: SCAppParser.TransferContext, context: SpecContext): Option[TransferCapa] =
    val limit = translateLimitExpr(t.limitExpr, context)
    limit match
      case err: ERR_EXPR =>
        println(s"${err.msg} at ${ctxPos(t.limitExpr)}")
        exceptions.add(err.msg)
      case _ =>
    val from = translateAccountSet(t.from, a)
    from match
      case Failure(exception) =>
        exceptions.add(exception.getMessage)
        None
      case Success(fromAs) =>
        val to = translateAccountSet(t.to, a)
        to match
          case Failure(exception) =>
            exceptions.add(exception.getMessage)
            None
          case Success(toAs) => Some(TransferCapa(a, fromAs, toAs, limit))


  private def translateAccountSet(ctx: SCAppParser.AccountSetContext, a: Actor): Try[AccountSet] =
    if ctx.ANY() != null then
      Success(ANY)
    else if ctx.SELF() != null then
      a match
        case Actor.Ro(_) => Success(SELF)
        case _ =>
          Failure(Exception("SELF not allowed outside of role capability specification"))
    else if ctx.THIS() != null then
      a match
        case Actor.Fun(f) =>
          contracts.find(c => c.name == f.contractName) match
            case Some(c) => Success(ACCOUNT(c))
            case None => Failure(Exception("Unreachable"))
        case _ =>
          Failure(Exception("THIS not allowed outside of function capability specification"))
    else if ctx.CALLER() != null then
      a match
        case Actor.Fun(_) => Success(CALLER)
        case _ =>
          Failure(Exception("SELF not allowed outside of role capability specification"))
    else if ctx.ID() != null then
      val name = ctx.ID().getText
      contracts.find(c => c.name == name) match
        case Some(c) => Success(AccountSet.ACCOUNT(c))
        case None =>
          roles.find(r => r.name == name) match
            case Some(r) => Success(AccountSet.ROLE(r))
            case None => Failure(Exception(s"Invalid tx capability account set expression: $name"))
    else
      Failure(Exception(s"Invalid tx capability account set expression: ${ctx.getText}"))


  override def visitVarLocExpr(ctx: SCAppParser.VarLocExprContext): Try[Location] =
    findQualVar(ctx.varName.getText, ctx.contractName.getText, contracts) match
      case Failure(e) => Failure(e)
      case Success(qsv) => Success(VarLoc(qsv))

  override def visitArrMapLocExpr(ctx: SCAppParser.ArrMapLocExprContext): Try[Location] =
    findQualVar(ctx.arrMapName.getText, ctx.contractName.getText, contracts) match
      case Failure(e) => Failure(e)
      case Success(qsv) =>
        getTypeOfQSV(qsv) match
          case ArrayType(t) =>
            translateIndExpr(ctx.arrMapIndExpr) match
              case StarLocExpr(_, _) => Success(ArrAllLoc(qsv))
              case e: IndexLocExpr => Success(ArrElementLoc(qsv, e.index))
              case e: RangeLocExpr => Success(ArrRangeLoc(qsv, e.from, e.to))
              case _ => Failure(Exception(s"Bad loc expression at ${ctxPos(ctx)}"))
          case MappingType(fromType, toType) =>
            translateIndExpr(ctx.arrMapIndExpr) match
              case StarLocExpr(_, _) => Success(MapAllLoc(qsv))
              case e: IndexLocExpr => Success(MapElementLoc(qsv, e.index))
              case _ => Failure(Exception(s"Bad loc expression at ${ctxPos(ctx)}"))
          case _ => Failure(Exception(s"Not an array or mapping variable: ${qsv.varName}"))

  override def visitStructLocExpr(ctx: SCAppParser.StructLocExprContext): Try[Location] =
    findQualVar(ctx.structName.getText, ctx.contractName.getText, contracts) match
      case Failure(e) => Failure(e)
      case Success(qsv) =>
        if ctx.structElemExpr().star != null then
          Success(StructAllLoc(qsv))
        else if ctx.structElemExpr().structElemName != null then
          Success(StructVarLoc(qsv, ctx.structElemExpr().structElemName.getText))
        else
          Failure(Exception(s"Bad struct loc expression at ${ctxPos(ctx)}"))

  private def translateIndExpr(ctx: SCAppParser.ArrMapIndExprContext): Expr =
    val precs = contracts.map(c => PreliminaryContract(c.name, c.stateVars, Set.empty, Set.empty, Set.empty, Set.empty))
    val tl = ExprTranslator(SpecContext(None, None, precs,
      UserDefinedTypes(Set.empty, Set.empty, Set.empty), Set.empty, SpecKind.ROLE_CAPA, ctxPos(ctx)))
    tl.visit(ctx)

  private def translateLimitExpr(ctx: SCAppParser.ExprContext, sc: SpecContext): Expr =
    val precs = contracts.map(c => PreliminaryContract(c.name, c.stateVars, Set.empty, Set.empty, Set.empty, Set.empty))
    val tl = ExprTranslator(sc.copy(kind = SpecKind.TX_CAPA))
    tl.visit(ctx)

  private def getTypeOfQSV(qsv: QualifiedStateVar): ModelType =
    contracts.find(c => c.name == qsv.contractName) match
      case None => EXPR_ERR_TYPE(s"Contract ${qsv.contractName} not found in capability specification")
      case Some(c) =>
        c.stateVars.find(sv => sv.name == qsv.varName) match
          case None => EXPR_ERR_TYPE(
            s"Variable ${qsv.varName} of contract ${qsv.contractName} not found in capability specification of role")
          case Some(value) => value.t
}
