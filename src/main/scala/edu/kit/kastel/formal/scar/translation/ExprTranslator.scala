package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.antlr.SCAppParser
import edu.kit.kastel.formal.scar.antlr.SCAppParser.{BoolLitExprContext, IdExprContext, NumLitExprContext, PrimExprContext}
import edu.kit.kastel.formal.scar.model.*
import org.antlr.v4.runtime.ParserRuleContext

import scala.collection.mutable
import scala.jdk.CollectionConverters.*

/**
 * This class translates the specification of a smart contract application.
 * @param con The context of the specification (state variables, parameters, user-defined types etc)
 */
class ExprTranslator(con: SpecContext) extends AbstractScappVisitor[Expr] {

  val qVars: mutable.Set[VarExpr] = mutable.Set()

  override def visitPrimExpr(ctx: SCAppParser.PrimExprContext): Expr =
    visit(ctx.primaryExpr())

  override def visitIdExpr(ctx: SCAppParser.IdExprContext): Expr =
    getExprFromName(ctx.getText, con, con.pos)

  override def visitStringLitExpr(ctx: SCAppParser.StringLitExprContext): Expr = 
    StringLiteralExpression(ctx.STRING_LIT().getText, con.pos)

  override def visitBoolLitExpr(ctx: SCAppParser.BoolLitExprContext): Expr = 
    ctx.BOOL_LIT().getText match 
      case "true" => BoolLiteralExpression(true, con.pos)
      case "false" => BoolLiteralExpression(false, con.pos)
      case x => ERR_EXPR(x, con.pos)

  override def visitNumLitExpr(ctx: SCAppParser.NumLitExprContext): Expr = 
    val littxt = ctx.INT_LIT().getText
    try
      val value = ctx.INT_LIT().getText.toLong
      if value < 0 then
        NumberLiteralExpression(value, IntType, con.pos)
      else NumberLiteralExpression(value, UintType, con.pos)
    catch
      case e: NumberFormatException => ERR_EXPR("Wrong number format", con.pos)

  override def visitEqExpr(ctx: SCAppParser.EqExprContext): Expr = 
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    ctx.op.getText match
      case "==" => EqExpr(left, right, con.pos)
      case "!=" => NeqExpr(left, right, con.pos)
      case "===" => RefEqExpr(left, right, con.pos)
      case "!==" => RefNeqExpr(left, right, con.pos)
      case s => ERR_EXPR(s"Unknown operator $s", con.pos)

  override def visitInEqExpr(ctx: SCAppParser.InEqExprContext): Expr = 
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    ctx.op.getText match
      case "<" => LtExpr(left, right, con.pos)
      case ">" => GtExpr(left, right, con.pos)
      case "<=" => LeqExpr(left, right, con.pos)
      case ">=" => GeqExpr(left, right, con.pos)
      case s => ERR_EXPR(s"Unknown operator $s", con.pos)

  override def visitLogicNotExpr(ctx: SCAppParser.LogicNotExprContext): Expr = 
    val e: Expr = visit(ctx.expr())
    LogicNotExpr(e, con.pos)

  override def visitLandExpr(ctx: SCAppParser.LandExprContext): Expr = 
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    LandExpr(left, right, con.pos)

  override def visitLorExpr(ctx: SCAppParser.LorExprContext): Expr = 
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    LorExpr(left, right, con.pos)

  override def visitImpExpr(ctx: SCAppParser.ImpExprContext): Expr =
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    ImpExpr(left, right, con.pos)

  override def visitAddSubExpr(ctx: SCAppParser.AddSubExprContext): Expr = 
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    ctx.op.getText match 
      case "+" => AddExpr(left, right, getArithBinExType(left, right), con.pos)
      case "-" => SubExpr(left, right, getArithBinExType(left, right), con.pos)
      case s => ERR_EXPR(s"Unknown operator $s", con.pos)

  override def visitMulModExpr(ctx: SCAppParser.MulModExprContext): Expr = 
    val left: Expr = visit(ctx.l)
    val right: Expr = visit(ctx.r)
    ctx.op.getText match 
      case "*" => MulExpr(left, right, getArithBinExType(left, right), con.pos)
      case "/" => DivExpr(left, right, getArithBinExType(left, right), con.pos)
      case "%" => ModExpr(left, right, getArithBinExType(left, right), con.pos)
      case s => ERR_EXPR(s"Unknown operator $s", con.pos)

  override def visitParExpr(ctx: SCAppParser.ParExprContext): Expr = 
    visit(ctx.expr())

  override def visitUnaryMinusExpr(ctx: SCAppParser.UnaryMinusExprContext): Expr = 
    val e = visit(ctx.expr())
    UnaryMinusExpr(e, con.pos)

  override def visitQuantifiedexpr(ctx: SCAppParser.QuantifiedexprContext): Expr = {
    val qVar = VarExpr(ctx.v.getText, parserTypeToModelType(ctx.t, con.udts), con.pos)
    qVars.add(qVar)
    var res: Expr = ERR_EXPR("unknown op", con.pos)
    if ctx.quantifier().EXISTS() != null then
      res = ExistsExpr(qVar, visit(ctx.matrix), con.pos)
    else if ctx.quantifier().FORALL() != null then
      res = ForallExpr(qVar, visit(ctx.matrix), con.pos)
    // reset quantified variable set
    qVars.clear()
    res

  }

  override def visitSetquantifiedexpr(ctx: SCAppParser.SetquantifiedexprContext): Expr =
    val set = visit(ctx.set)
    set.t match
      case SetType(ty) =>
        val qVar = VarExpr(ctx.v.getText, ty, con.pos)
        qVars.add(qVar)
        var res: Expr = ERR_EXPR("unknown op", con.pos)
        if ctx.quantifier().EXISTS() != null then
          res = SetExistsExpr(qVar, set, visit(ctx.matrix), con.pos)
        else if ctx.quantifier().FORALL() != null then
          res = SetForallExpr(qVar, set, visit(ctx.matrix), con.pos)
        // reset quantified variable set
        qVars.clear()
        res
      case _ => ERR_EXPR(s"Bad type ${set.t} in set quantified expression", ctxPos(ctx))

  override def visitAccessExpr(ctx: SCAppParser.AccessExprContext): Expr =
    val arrExpr = visit(ctx.arrName)
    val indExpr = visit(ctx.index)
    arrExpr.t match
      case ArrayType(at) => ArrayAccExpr(arrExpr, at, indExpr, con.pos)
      case MappingType(from, to) => MappingAccExpr(arrExpr, to, indExpr, con.pos)
      case _ => ERR_EXPR(s"ExprVisitor: Bad type ${arrExpr.t} in access expression", con.pos)

  override def visitFunCallExpr(ctx: SCAppParser.FunCallExprContext): Expr =
    val funExpr = visit(ctx.funName)
    funExpr match
      case MemberAccessExpr(c, m, t, pos) =>
        val paramList = ctx.ps.args.asScala.map(p => visit(p)).toList
        FunCallExpr(funExpr, paramList, t, con.pos)
      case VarExpr(name, t, pos) =>
        if ctx.ps.args.size() != 1 then
          return ERR_EXPR(s"Bad function call expression / functional access expression", con.pos)
        t match
          case ArrayType(at) =>
            ArrayFunAccExpr(funExpr, at, visit(ctx.ps.args.get(0)), con.pos)
          case MappingType(from, to) =>
            MappingFunAccExpr(funExpr, to, visit(ctx.ps.args.get(0)), con.pos)
      case _ => ERR_EXPR(s"Bad function call expression / functional access expression", con.pos)

  override def visitMemberAccessExpr(ctx: SCAppParser.MemberAccessExprContext): Expr =
    val leftExpr = visit(ctx.expr())
    val id = ctx.identifier.getText
    val retModelType = leftExpr match
      case VarExpr(name, t, pos) => getTypeOfMember(name, id, con)
      case ArrayAccExpr(arr, t, index, pos) => t
      case MemberAccessExpr(l, r, t, pos) => t
      case _ => EXPR_ERR_TYPE(s"Bad Member Access Expression at ${con.pos}")
    MemberAccessExpr(leftExpr, id, retModelType, con.pos)

  override def visitEnumStructExpr(ctx: SCAppParser.EnumStructExprContext): Expr =
    val leftExpr = visit(ctx.expr)
    val id = ctx.identifier.getText
    leftExpr.t match
      case et: EnumType => EnumConstExpr(et.name, id, et, con.pos)
      case StructType(n, vars) =>
        val v = vars.find(v => v.name == id)
        val t: ModelType = v match
          case Some(value) => v.get.t
          case None => EXPR_ERR_TYPE(s"Unknown struct member $id at ${con.pos}")
        StructAccExpr(leftExpr, t, id, con.pos)
      case EXPR_ERR_TYPE(err) => ERR_EXPR(err, con.pos)

  override def visitOldExpr(ctx: SCAppParser.OldExprContext): Expr =
    if con.kind != SpecKind.POSTCONDITION && con.kind != SpecKind.TMP then
      ERR_EXPR(s"\\old expression outside of postcondition or temporal spec", con.pos)
    val e = visit(ctx.expr)
    OldExpr(e, e.t, con.pos)

  override def visitSumExpr(ctx: SCAppParser.SumExprContext): Expr =
    val setExpr = visit(ctx.expr())
    setExpr.t match
      case SetType(t) =>
        t match
          case IntType => SumExpr(IntType, setExpr, ctxPos(ctx))
          case UintType => SumExpr(UintType, setExpr, ctxPos(ctx))
          case _ => ERR_EXPR(s"Bad type $t in sum expression", ctxPos(ctx))
      case _ => ERR_EXPR(s"Bad type ${setExpr.t} in sum expression", ctxPos(ctx))

  override def visitKeysExpr(ctx: SCAppParser.KeysExprContext): Expr =
    val mapExpr = visit(ctx.expr())
    mapExpr.t match
      case MappingType(from, to) => KeysExpr(mapExpr, SetType(from), ctxPos(ctx))
      case _ => ERR_EXPR(s"Bad type ${mapExpr.t} in keys expression", ctxPos(ctx))

  override def visitValuesExpr(ctx: SCAppParser.ValuesExprContext): Expr =
    val mapExpr = visit(ctx.expr())
    mapExpr.t match
      case MappingType(from, to) => ValuesExpr(mapExpr, SetType(to), ctxPos(ctx))
      case _ => ERR_EXPR(s"Bad type ${mapExpr.t} in values expression", ctxPos(ctx))

  override def visitSizeExpr(ctx: SCAppParser.SizeExprContext): Expr =
    val setExpr = visit(ctx.expr())
    setExpr match
      case VarExpr(name, t, pos) =>
        t match
          case ArrayType(_) => SizeExpr(setExpr, ctxPos(ctx))
          case MappingType(_, _) => SizeExpr(setExpr, ctxPos(ctx))
          case _ => ERR_EXPR(s"Bad type $t in size expression", ctxPos(ctx))
      case _ => ERR_EXPR(s"Bad type ${setExpr.t} in size expression", ctxPos(ctx))

  override def visitHashExpr(ctx: SCAppParser.HashExprContext): Expr =
    HashExpr(visit(ctx.expr()), con.pos)
    
  override def visitSendExpr(ctx: SCAppParser.SendExprContext): Expr =
    if con.kind != SpecKind.POSTCONDITION && con.kind != SpecKind.TMP then
      ERR_EXPR(s"\\send expression outside of postcondition or temporal spec", con.pos)
    val from = visit(ctx.from)
    val to = visit(ctx.to)
    val amt = visit(ctx.amt)
    SendExpr(from, to, amt, con.pos)

  override def visitSelfExpr(ctx: SCAppParser.SelfExprContext): Expr =
    if con.kind != SpecKind.ROLE_CAPA then
      ERR_EXPR("self outside of role capability", con.pos)
    SelfExpr(con.pos)

  override def visitBlocknumExpr(ctx: SCAppParser.BlocknumExprContext): Expr =
    BlocknumExpr(con.pos)

  override def visitSystimeExpr(ctx: SCAppParser.SystimeExprContext): Expr =
    SystimeExpr(con.pos)

  override def visitCallerExpr(ctx: SCAppParser.CallerExprContext): Expr =
    CallerExpr(con.pos)

  override def visitThisExpr(ctx: SCAppParser.ThisExprContext): Expr =
    ThisExpr(con.pos)

  override def visitResultExpr(ctx: SCAppParser.ResultExprContext): Expr =
    con.thisFunction match
      case Some(f) => f.retType match
        case Some(value) => ResultExpr(value, con.pos)
        case None => ERR_EXPR(s"Cannot use \\result here: Function ${f.name} has no return type", con.pos)
      case None => ERR_EXPR(s"Cannot use \\result here: Not in a function", con.pos)

  override def visitAmtExpr(ctx: SCAppParser.AmtExprContext): Expr =
    AmtExpr(con.pos)

  override def visitStarExpr(ctx: SCAppParser.StarExprContext): Expr =
    StarLocExpr(LocType, ctxPos(ctx))

  override def visitRangeExpr(ctx: SCAppParser.RangeExprContext): Expr =
    RangeLocExpr(visit(ctx.fromExpr), visit(ctx.toExpr), LocType, ctxPos(ctx))

  override def visitIndexExpr(ctx: SCAppParser.IndexExprContext): Expr =
    IndexLocExpr(visit(ctx.expr()), LocType, ctxPos(ctx))

  private def getArithBinExType(left: Expr, right: Expr): ModelType =
    if left.t == right.t then
      left.t
    else IntType

  private def getExprFromName(name: String, con: SpecContext, pos: String): Expr =
    con.contracts.find(c => c.name == name) match
      case Some(contract) => VarExpr(name, AccountType, pos)
      case None =>
        con.udts.enumTypes.find(u => u.name == name) match
          case Some(et) => VarExpr(name, et, pos)
          case None =>
            con.params.find(p => p.name == name) match
              case Some(value) => VarExpr(name, value.t, pos)
              case None =>
                qVars.find(p => p.name == name) match
                  case Some(value) => VarExpr(name, value.t, pos)
                  case None =>
                    con.thisContract match
                      case Some(c) =>
                        c.stateVars.find(v => v.name == name) match
                          case Some(v) => VarExpr(name, v.t, pos)
                          case None => ERR_EXPR(s"Unknown variable $name", pos)
                      case None =>
                        val msg = s"Unknown variable $name"
                        ERR_EXPR(msg, pos)

  private def getTypeOfMember(name: String, memberName: String, con: SpecContext): ModelType =
    con.contracts.find(c => c.name == name) match
      case Some(c) => c.stateVars.find(sv => sv.name == memberName) match
        case Some(sv) => sv.t
        case None => c.functions.find(f => f.name == memberName) match
          case Some(fun) => fun.retType match
            case Some(value) => value
            case None => EXPR_ERR_TYPE(s"function $name without return type cannot be used in specification")
          case None => EXPR_ERR_TYPE(s"Unknown member $memberName of contract $name")
      case None => EXPR_ERR_TYPE(s"Unknown contract $name")
}
