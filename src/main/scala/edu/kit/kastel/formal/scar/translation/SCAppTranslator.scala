package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.model.*
import edu.kit.kastel.formal.scar.antlr.SCAppParser.*
import edu.kit.kastel.formal.scar.model.{Actor, CallCapa, Contract, ContractType, ERR_EXPR, EnumType, Expr, Fun, FunSpec, ModelType, ModifyCapa, Param, Role, StateVar, StructType, TransferCapa, UserDefinedTypes, findQualFun}

import scala.collection.mutable
import scala.jdk.CollectionConverters.*
import scala.util.{Failure, Success, Try}

/**
 *  The main translation class.
 *  Translates the ANTLR representation of a SCApp model to a SCApp object.
 *  First translates the user-defined types (enums and structs) and collects the names
 *  and types of state variables and functions. Then translates the function specifications,
 *  capabilities, and temporal properties.
 */
class SCAppTranslator extends AbstractScappVisitor[Unit] {

  //the application name
  var name: String = ""
  // the contracts of the application
  val contracts: mutable.Set[Contract] = mutable.Set()
  // an intermediate representation of all contracts where the function spec is not yet translated
  private val prelimContracts: mutable.Set[PreliminaryContract] = mutable.Set()
  // the user-defined enum types of the application
  val structTypes: mutable.Set[StructType] = mutable.Set()
  // the user-defined struct types of the application
  val enumTypes: mutable.Set[EnumType] = mutable.Set()
  // contract types (one for each contract in the app)
  var contractTypes: Set[ContractType] = Set.empty
  // counter for setting default contract address
  private var ctr = 0
  // roles in the application
  val roles: mutable.Set[Role] = mutable.Set()
  private val prelimRoles: mutable.Set[PrelimRole] = mutable.Set()
  // preliminary Capabilities
  private val prelimFunCapas: mutable.Set[PrelimFunCapa] = mutable.Set()
  // preliminary temporal properties
  private val prelimTmpProps: mutable.Set[SpecString] = mutable.Set()
  var tmpProps: Set[Expr] = Set.empty
  val modCapas: mutable.Set[ModifyCapa] = mutable.Set()
  val callCapas: mutable.Set[CallCapa] = mutable.Set()
  val txCapas: mutable.Set[TransferCapa] = mutable.Set()


  override def visitScapp(ctx: ScappContext): Unit =
    name = ctx.name.getText
    // get contract types (needs to be done beforehand)
    contractTypes = ctx.contracts.asScala
      .map(contractCtx => ContractType(contractCtx.name.getText,
        contractCtx.stateVars.asScala.map(svCtx => svCtx.name.getText).toSet,
        contractCtx.functions.asScala.map(fnCtx => fnCtx.name.getText).toSet)).toSet
    // translate the main part (user-defined types, state variables, functions, ...)
    visitChildren(ctx)
    val userDefTypes = UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes)
    // translate specification
    for pc <- prelimContracts do
      //translate function specification
      val funs: mutable.Set[Fun] = mutable.Set()
      for f <- pc.functions do
        // translate preconditions
        val pres: mutable.Set[Expr] = mutable.Set()
        for pre <- f.preStrings do
          pres.add(translatePre(userDefTypes, pc, f, pre))
        // translate postconditions
        val posts: mutable.Set[Expr] = mutable.Set()
        for post <- f.postStrings do
          posts.add(translatePost(userDefTypes, pc, f, post))
        funs.add(Fun(f.name, f.retType, f.params, FunSpec(pres.toSet, posts.toSet)))
      // translate initial condition and invariants
      val initConds = pc.initStrings.map(i => translateInit(userDefTypes, pc, i))
      val invariants = pc.invariants.map(i => translateSpecExpr(i, SpecContext(Some(pc), None, prelimContracts.toSet, userDefTypes, Set.empty, SpecKind.INVARIANT, i.pos)))
      contracts.add(Contract(pc.name, pc.stateVars, invariants, funs.toSet, (pc.initParams, initConds)))
    // translate capabilities (only if no exceptions so far)
    if exceptions.isEmpty then
      // translate capabilities
      val prelimCapas: Set[(Actor, String)] =
        prelimRoles
        .map(p => (Actor.Ro(Role(p.name)), p.capas))
        .addAll(prelimFunCapas
          .map(f => (Actor.Fun(findQualFun(f.funName, f.contractName, contracts.toSet).get), f.capaString)))
          .toSet
      for pr <- prelimCapas do
        val context = getSpecContext(pr._1)
        translateCapa(pr, context)
      // translate temporal properties
      tmpProps = prelimTmpProps.map(p => translateTmpProp(p)).toSet

  private def getSpecContext(actor: Actor): SpecContext =
    actor match
      case Actor.Fun(f) => SpecContext(None, None, prelimContracts.toSet,
        UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes),
        // find the actual function to provide context for translation
        contracts.find(c => c.name == f.contractName).get.functions.find(fun => fun.name == f.funName).get.params,
        SpecKind.FUN_CAPA, "")
      case _ => SpecContext(None, None, prelimContracts.toSet,
        UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes), Set.empty, SpecKind.ROLE_CAPA, "")

  private def translateTmpProp(specString: SpecString) : Expr =
    try
      val exprCtx = Text2scapp.parseLPropString(specString.spec)
      val context = SpecContext(None, None, prelimContracts.toSet,
                    UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes), 
                    Set.empty, SpecKind.TMP, specString.pos)
      val tl = new TempPropTranslator(context)
      val cond = tl.visit(exprCtx)
      val checker = SpecExprChecker(context)
      checker.checkSpecExpr(cond)
      for err <- checker.errors do
        exceptions.add(err)
      cond
    catch
      case e: Exception =>
        exceptions.add(e.toString)
        ERR_EXPR("Error when parsing temporal property: " + e.toString, specString.pos)



  override def visitLProp(ctx: LPropContext): Unit =
    prelimTmpProps.add(SpecString(ctx.getText, ctxPos(ctx)))

  private def translateCapa(pr: (Actor, String), context: SpecContext) =
    val trans = new CapabilitiesTranslator(contracts.toSet, roles.toSet)
    trans.translateCapa(pr._1, pr._2, context) match
      case Failure(exception) => exceptions.add(exception.toString)
      case Success(capas) =>
        modCapas.addAll(capas.modCapas)
        txCapas.addAll(capas.txCapas)
        callCapas.addAll(capas.callCapas)

  override def visitUserDefType(ctx: UserDefTypeContext): Unit =
    if ctx.enumDecl() != null then
      val name = ctx.enumDecl().name.getText
      val consts: mutable.Set[String] = mutable.Set()
      for (c <- ctx.enumDecl().consts.asScala)
        consts.add(c.getText)
      enumTypes.add(EnumType(name, consts.toSet))
    if ctx.structDecl() != null then
      val name = ctx.structDecl().name.getText
      val vars: mutable.Set[StateVar] = mutable.Set()
      val udts = UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes)
      for (v <- ctx.structDecl().vars.asScala) do
        vars.add(StateVar(v.name.getText, parserTypeToModelType(v.t, udts)))
      structTypes.add(StructType(name, vars.toSet))

  override def visitContract(ctx: ContractContext): Unit =
    ctr += 1
    var initparams = Set.empty[Param]
    if ctx.INITPARAMS() != null then
      initparams = ctx.inparams.param().asScala.map(p => Param(
        p.name.getText, parserTypeToModelType(p.typename(), UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes)))).toSet
    val inits = ctx.initConditions.asScala.map(i => SpecString(i.getText, ctxPos(i))).toSet
    val invars = ctx.invariants.asScala.map(i => SpecString(i.getText, ctxPos(i))).toSet
    val statevars = ctx.stateVars.asScala.map(translateStateVar).toSet
    val funs = ctx.functions.asScala.map(translateFunction).toSet
    val name = ctx.name.getText
    for (fun <- ctx.functions.asScala)
      if fun.capabilities() != null then
        for tx <- fun.capabilities().txs.asScala do
          prelimFunCapas.add(PrelimFunCapa(name, fun.name.getText, tx.getText))
        for c <- fun.capabilities().calls.asScala do
          prelimFunCapas.add(PrelimFunCapa(name, fun.name.getText, c.getText))
        for mod <- fun.capabilities().mods.asScala do
          prelimFunCapas.add(PrelimFunCapa(name, fun.name.getText, mod.getText))
    prelimContracts.add(PreliminaryContract(name, statevars, invars, funs, inits, initparams))

  override def visitRoleset(ctx: RolesetContext): Unit =
    for r <- ctx.roles.asScala do
      r.capabilities() match
        case c: CapabilitiesContext =>
          for tx <- c.txs.asScala do
            prelimRoles.add(PrelimRole(r.ID().getText, tx.getText))
          for call <- c.calls.asScala do
            prelimRoles.add(PrelimRole(r.ID().getText, call.getText))
          for mod <- c.mods.asScala do
            prelimRoles.add(PrelimRole(r.ID().getText, mod.getText))
        case null => ()
      roles.add(Role(r.ID().getText))


  private def translateStateVar(ctx: StateVarContext): StateVar =
    val udts = UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes)
    val name = ctx.name.getText
    val t = parserTypeToModelType(ctx.typename(), udts)
    StateVar(name, t)

  private def translateFunction(ctx: FunctionContext): FunWithSpecString =
    val udts = UserDefinedTypes(enumTypes.toSet, structTypes.toSet, contractTypes)

    val funName = ctx.name.getText
    var retType: Option[ModelType] = None
    if ctx.rettype != null then
      retType = Some(parserTypeToModelType(ctx.rettype, udts))

    val params: mutable.Set[Param] = mutable.Set()
    if ctx.params != null && ctx.params.param.size > 0 then
      for p <- ctx.params.param.asScala do
        val name = p.name.getText
        val t = parserTypeToModelType(p.typename, udts)
        params.add(Param(name, t))

    val pres: mutable.Set[SpecString] = mutable.Set()
    if ctx.preconditions != null then
      for pre <- ctx.preconditions.asScala do
        pres.add(SpecString(pre.getText, ctxPos(pre)))
    val posts: mutable.Set[SpecString] = mutable.Set()
    if ctx.postconditions != null then
      for post <- ctx.postconditions.asScala do
        posts.add(SpecString(post.getText, ctxPos(post)))
    FunWithSpecString(funName, retType, params.toSet, pres.toSet, posts.toSet)

  private def translatePre(userDefTypes: UserDefinedTypes,
                           pc: PreliminaryContract,
                           f: FunWithSpecString,
                           pre: SpecString): Expr =
    val context = SpecContext(Some(pc), Some(f), prelimContracts.toSet, userDefTypes, f.params, SpecKind.PRECONDITION, pre.pos)
    translateSpecExpr(pre, context)

  private def translatePost(userDefTypes: UserDefinedTypes,
                            pc: PreliminaryContract,
                            f: FunWithSpecString,
                            post: SpecString): Expr =
    val context = SpecContext(Some(pc), Some(f), prelimContracts.toSet, userDefTypes, f.params, SpecKind.POSTCONDITION, post.pos)
    translateSpecExpr(post, context)

  private def translateInit(userDefTypes: UserDefinedTypes,
                            pc: PreliminaryContract,
                            init: SpecString): Expr =
    val context = SpecContext(Some(pc), None, prelimContracts.toSet, userDefTypes, pc.initParams, SpecKind.INIT, init.pos)
    translateSpecExpr(init, context)

  private def translateSpecExpr(specString: SpecString, context: SpecContext): Expr =
    val exprCtx = Text2scapp.parseExprString(specString.spec)
    val ev = new ExprTranslator(context)
    val cond = ev.visit(exprCtx)
    val checker = SpecExprChecker(context)
    checker.checkSpecExpr(cond)
    for err <- checker.errors do
      exceptions.add(err)
    cond

}

/**
 * An intermediate helper class representing a Function where the specification
 * (i.e., pre- and postconditions) are still untranslated and present only as Strings,
 * to be translated later
 */
case class FunWithSpecString(
              name: String,
              retType: Option[ModelType],
              params: Set[Param],
              preStrings: Set[SpecString],
              postStrings: Set[SpecString])

case class SpecString(spec: String, pos: String)

/**
 * Intermediate contracts where function pre- and postconditions are not yet translated
 */
case class PreliminaryContract(name: String,
              stateVars: Set[StateVar],
              invariants: Set[SpecString],
              functions: Set[FunWithSpecString],
              initStrings: Set[SpecString],
              initParams: Set[Param])

/**
 * Context which is passed to the specification translator, e.g., for deciding which type a spec expression has
 * @param thisContract the current contract
 * @param contracts other contracts in the application
 * @param udts all user-defined datatypes (enums and structs) in the application
 * @param params function parameters (empty if not in a function context)
 * @param kind the kind of specification
 * @param pos the position of the expression in the model text file
 */
case class SpecContext(thisContract: Option[PreliminaryContract],
                       thisFunction: Option[FunWithSpecString],
                       contracts: Set[PreliminaryContract],
                       udts: UserDefinedTypes,
                       params: Set[Param],
                       kind: SpecKind,
                       pos: String)

enum SpecKind:
  case INIT, INVARIANT, PRECONDITION, POSTCONDITION, ROLE_CAPA, FUN_CAPA, TX_CAPA, TMP

case class PrelimRole(name: String, capas: String)

case class PrelimFunCapa(contractName: String, funName: String, capaString: String)