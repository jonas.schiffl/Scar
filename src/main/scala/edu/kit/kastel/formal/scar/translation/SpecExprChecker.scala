package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.model.*

import scala.collection.mutable

/**
 * Performs type correctness checks of expressions in the spec.
 */
class SpecExprChecker(con: SpecContext) {

  val errors: mutable.Set[String] = mutable.Set()

  def checkSpecExpr(expr: Expr): Unit =
    if expr.t != BoolType then
      errors.add(s"Toplevel spec expression must have Boolean type, but found ${expr.t}; ${con.pos}")
    checkExpr(expr)

  private def checkExpr(expr: Expr): Unit =
    expr match
      case VarExpr(name, t, pos) => ()
      case ForallExpr(qvar, matrix, pos) => checkSpecExpr(matrix)
      case ExistsExpr(qvar, matrix, pos) => checkSpecExpr(matrix)
      case SetForallExpr(qvar, set, matrix, pos) => checkSpecExpr(matrix)
      case SetExistsExpr(qvar, set, matrix, pos) => checkSpecExpr(matrix)
      case ArrayAccExpr(arr, t, index, pos) => checkArrExpr(arr, t, index, pos)
      case ArrayFunAccExpr(arr, t, index, pos) => checkArrExpr(arr, t, index, pos)
      case MappingAccExpr(m, t, i, pos) => checkMappingAccExpr(m, t, i, pos)
      case MappingFunAccExpr(m, t, i, pos) => checkMappingAccExpr(m, t, i, pos)
      case KeysExpr(m, t, pos) =>
        m.t match
          case MappingType(_, _) => checkExpr(m)
          case _ => errors.add(s"Bad type ${m.t} in size expression at $pos")
      case ValuesExpr(m, t, pos) =>
        m.t match
          case ArrayType(_) | MappingType(_, _) => checkExpr(m)
          case _ =>  errors.add(s"Bad type ${m.t} in size expression at $pos")
      case SizeExpr(m, pos) =>
        m.t match
          case ArrayType(_) | MappingType(_, _) => checkExpr(m)
          case _ =>  errors.add(s"Bad type ${m.t} in size expression at $pos")
      case EnumConstExpr(enumName, constName, t, pos) => checkEnumExpr(enumName, constName, t, pos)
      case expr: StructAccExpr => checkStructAccExpr(expr)
      case e: FunCallExpr => checkFunCallExpr(e)
      case expr: MemberAccessExpr => checkMemberAccExpr(expr)
      case UnaryMinusExpr(e, pos) =>
        if !e.t.isInstanceOf[NumberType] then
          errors.add(s"Non-number type in unary minus expr at $pos")
        checkExpr(e)
      case expr: BinArithExpr => checkBinArithExpr(expr)
      case expr: CompExpr => checkArithCompExpr(expr)
      case expr: EqExpr => checkEqExpr(expr.l, expr.r, expr.pos)
      case expr: NeqExpr => checkEqExpr(expr.l, expr.r, expr.pos)
      case LogicNotExpr(e, pos) =>
        if e.t != BoolType then errors.add(s"Non-bool type in logic not expression: $pos")
        checkExpr(e)
      case LandExpr(l, r, pos) =>
        if l.t != BoolType || r.t != BoolType then errors.add(s"Non-bool type in logic expression: $pos")
        checkExpr(l)
        checkExpr(r)
      case LorExpr(l, r, pos) =>
        if l.t != BoolType || r.t != BoolType then errors.add(s"Non-bool type in logic expression: $pos")
        checkExpr(l)
        checkExpr(r)
      case ImpExpr(l, r, pos) =>
        if l.t != BoolType || r.t != BoolType then errors.add(s"Non-bool type in logic expression: $pos")
        checkExpr(l)
        checkExpr(r)
      case expression: LiteralExpression => ()
      case ResultExpr(_, _) | ThisExpr(_) | CallerExpr(_) | SystimeExpr(_) | BlocknumExpr(_) | AmtExpr(_) => ()
      case OldExpr(e, t, pos) => checkExpr(e)
      case HashExpr(e, pos) => checkExpr(e)
      case e: SendExpr => checkSendExpr(e)
      case BoxExpr(expr, pos) =>
        if expr.t != BoolType then errors.add(s"Non-bool type in box expression: $pos")
        checkExpr(expr)
        // TODO better checks for enabled and enabled_until
      case EnabledExpr(account, paramCond, amt, expr, pos) =>
        checkOptExpr(account)
        checkOptExpr(paramCond)
        checkOptExpr(amt)
        checkExpr(expr)
      case EnabledUntilExpr(account, paramCond, amt, expr, pos) =>
        checkOptExpr(account)
        checkOptExpr(paramCond)
        checkOptExpr(amt)
        checkExpr(expr)
      case TxExpr(fun, caller, paramCond, amt, pos) =>
      case OwnsExpr(account, amt, pos) =>
        if account.t != AccountType then errors.add(s"Non-account type in owns account expression: $pos")
        if amt.t != UintType then errors.add(s"Non-uint type in owns amt expression: $pos")
        checkExpr(account)
        checkExpr(amt)
      case WeakUntilExpr(l, r, pos) =>
        if l.t != BoolType || r.t != BoolType then errors.add(s"Non-bool type in weak until expression: $pos")
        checkExpr(l)
        checkExpr(r)
      case ERR_EXPR(msg, pos) => errors.add(s"$msg at $pos")
      case e => errors.add(s"SpecExprChecker: Unmatched expression ${e.toString} at ${e.pos}")

  private def checkOptExpr(e: Option[Expr]): Unit =
    e match
      case Some(e) => checkExpr(e)
      case None => ()

  private def checkEqExpr(l: Expr, r: Expr, pos: String): Unit =
    if !((l.t == r.t) || (l.t.isInstanceOf[NumberType] && r.t.isInstanceOf[NumberType])) then
      errors.add(s"Comparing incompatible types ${l.t} and ${r.t} at $pos")
    checkExpr(l)
    checkExpr(r)

  private def checkBinArithExpr(expr: BinArithExpr): Unit =
    if expr.t != IntType && expr.t != UintType then
      errors.add(s"Bad type in arithmetic expression: ${expr.t.toString} at ${expr.pos}")
    checkExpr(expr.l)
    checkExpr(expr.r)

  private def checkArithCompExpr(expr: CompExpr): Unit =
    if expr.t != BoolType then
      errors.add(s"Bad type in comparison expression: ${expr.t.toString} at ${expr.pos}")
    if !expr.l.t.isInstanceOf[NumberType] || !expr.r.t.isInstanceOf[NumberType] then
      errors.add(s"Non-number type in comparison expression: ${expr.t.toString} at ${expr.pos}")
    checkExpr(expr.l)
    checkExpr(expr.r)

  private def checkArrExpr(arr: Expr, t: ModelType, i: Expr, pos: String): Unit =
    arr.t match
      case ArrayType(_) =>
      case _ => errors.add(s"Bad array access expression at $pos, expected array type, but got ${arr.t.toString}")
    if i.t != UintType then
      errors.add(s"Array index expression must be of type uint at $pos (${i.toString} is type ${i.t.toString})")
    checkExpr(arr)
    checkExpr(i)

  private def checkMappingAccExpr(m: Expr, t: ModelType, i: Expr, pos: String): Unit =
    m.t match
      case MappingType(from, _) =>
        if i.t != from then
          errors.add(s"Wrong type ${i.t} in mapping access at $pos")
      case _ => errors.add(s"Bad mapping access expression at $pos; expected mapping type, but got ${t.toString}")

  private def checkEnumExpr(name: String, constName: String, t: ModelType, pos: String): Unit =
    con.udts.enumTypes.find(e => e.name == name) match
      case None => errors.add(s"No such enum type: $name at $pos")
      case Some(value) =>
        value.consts.find(c => c == constName) match
          case None => errors.add(s"No such enum constant for enum $name: $constName at $pos")
          case Some(_) => ()

  private def checkStructAccExpr(e: StructAccExpr): Unit =
    e.t match
      case EXPR_ERR_TYPE(err) => errors.add(err)
      case _ =>
        e.structExpr.t match
          case s: StructType => con.udts.structTypes.find(x => x.name == s.name) match
            case None => errors.add(s"Unknown struct type ${s.name} at ${e.pos}")
            case Some(s) => s.vars.find(v => v.name == e.structFieldName) match
              case None => errors.add(s"Unknown struct field ${e.structFieldName} at ${e.pos}")
              case Some(_) => ()

  private def checkSendExpr(expr: SendExpr): Unit =
    val to = expr.toExpr
    val amt = expr.amtExpr
    if to.t != AccountType then
      errors.add("First argument of send expression must be of type Account")
    if !amt.t.isInstanceOf[NumberType] then
      errors.add("Second argument of send expression must be of Number type")
    checkExpr(to)
    checkExpr(amt)

  private def checkMemberAccExpr(expr: MemberAccessExpr): Unit =
    if expr.contractExpr.t != AccountType then
      errors.add(s"Bad type in member access expression ${expr.contractExpr.t} at ${expr.contractExpr.pos}")

  // TODO match parameters and check whether the function is side-effect-free
  private def checkFunCallExpr(e: FunCallExpr): Unit =
    ()

}
