package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.antlr.SCAppParser.*
import edu.kit.kastel.formal.scar.model.*

import scala.jdk.CollectionConverters.*

/**
 * Translates temporal properties from the ANTLR representation to SCAR expressions.
 */
class TempPropTranslator(con: SpecContext) extends ExprTranslator(con: SpecContext):

  override def visitBoxExpr(ctx: BoxExprContext): Expr =
    BoxExpr(visit(ctx.expr), con.pos)

  override def visitWeakUntilExpr(ctx: WeakUntilExprContext): Expr =
    WeakUntilExpr(visit(ctx.l), visit(ctx.r), con.pos)

  override def visitEnabledExpr(ctx: EnabledExprContext): Expr =
    val e = visit(ctx.e)
    EnabledExpr(op(ctx.accExpr), op(ctx.paramCond), op(ctx.amtExpr), e, con.pos)

  override def visitEnabledUntilExpr(ctx: EnabledUntilExprContext): Expr =
    val e = visit(ctx.e)
    EnabledUntilExpr(op(ctx.accExpr), op(ctx.paramCond), op(ctx.amtExpr), e, con.pos)

  override def visitTxExpr(ctx: TxExprContext): Expr =
    val qf = QualifiedFun(ctx.funName.getText, ctx.conName.getText)
    TxExpr(qf, op(ctx.accExpr), op(ctx.paramCond), op(ctx.amtExpr), con.pos)

  override def visitOwnsExpr(ctx: OwnsExprContext): Expr =
    OwnsExpr(visit(ctx.accExpr), visit(ctx.amtExpr), con.pos)

  private def op(ctx: ExprContext): Option[Expr] =
    Option(ctx).map(visit)