package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.antlr.SCAppParser.{CapabilitiesContext, ExprContext, LPropContext, ScappContext}
import edu.kit.kastel.formal.scar.antlr.{SCAppLexer, SCAppParser, ThrowingErrorListener}
import edu.kit.kastel.formal.scar.model.{Capabilities, SCApp, UserDefinedTypes}
import org.antlr.v4.runtime.CharStreams.fromString
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.misc.ParseCancellationException

import java.nio.file.Path
import scala.io.*
import scala.util.{Failure, Success, Try, Using}

/**
 * This object provides methods to translate a string representation of a SCApp model to a SCApp object.
 */
object Text2scapp {

  /**
   * Translates a string representation of a SCApp model to a SCApp object.
   * @param text the string representation of the SCApp model
   * @return a Try object containing the SCApp object if the translation was successful,
   *         or a Failure object containing an exception otherwise
   */
  def translateString(text: String) : Try[SCApp] =
    Try(parseScappString(text)) match
      case Success(ctx) =>

        val scv = new SCAppTranslator()

        //translate contracts, state variables, and function descriptions
        scv.visitScapp(ctx)
        if scv.exceptions.nonEmpty then
          val msg = scv.exceptions.mkString("", "\n", "")
          return Failure(Exception(msg))

        val name = scv.name
        val udts = UserDefinedTypes(scv.enumTypes.toSet, scv.structTypes.toSet, scv.contractTypes)
        val contracts = scv.contracts.toSet
        val capas = Capabilities(scv.roles.toSet, scv.callCapas.toSet, scv.modCapas.toSet, scv.txCapas.toSet)
        val tempProps = scv.tmpProps
        Success(SCApp(name, udts, contracts, capas, tempProps))

      case Failure(exception) =>
        Failure(exception)

  def generateSCApp(scar: Path): SCApp =
    translateFromFile(scar.toString) match
      case Failure(exception) => throw exception
      case Success(value) => value
    
  def translateFromFile(filepath: String) : Try[SCApp] =
    val source = scala.io.Source.fromFile(filepath)
    val modelString = try source.mkString finally source.close()
    translateString(modelString)

//    Using(Source.fromFile(filepath))(_.mkString).flatMap(translateString)

  def parseFile(filepath: String) : Try[ScappContext] =
    Using(Source.fromFile(filepath))(_.mkString) match
      case Success(s) =>
        try
          Success(parseScappString(s))
        catch
          case e: ParseCancellationException => Failure(e)
      case Failure(e) => Failure(e)


  @throws[ParseCancellationException]
  def parseScappString(text: String) : ScappContext =
    val charstream = fromString(text)
    val lexer = new SCAppLexer(charstream)
    lexer.removeErrorListeners()
    lexer.addErrorListener(ThrowingErrorListener.INSTANCE)
    val ts = new CommonTokenStream(lexer)
    val parser = new SCAppParser(ts)
    parser.removeErrorListeners()
    parser.addErrorListener(ThrowingErrorListener.INSTANCE)
    parser.scapp()

  @throws[ParseCancellationException]
  def parseExprString(text: String): ExprContext =
    val charstream = fromString(text)
    val lexer = new SCAppLexer(charstream)
    lexer.removeErrorListeners()
    lexer.addErrorListener(ThrowingErrorListener.INSTANCE)
    val ts = new CommonTokenStream(lexer)
    val parser = new SCAppParser(ts)
    parser.removeErrorListeners()
    parser.addErrorListener(ThrowingErrorListener.INSTANCE)
    parser.expr()

  @throws[ParseCancellationException]
  def parseLPropString(text: String): LPropContext =
    val charstream = fromString(text)
    val lexer = new SCAppLexer(charstream)
    lexer.removeErrorListeners()
    lexer.addErrorListener(ThrowingErrorListener.INSTANCE)
    val ts = new CommonTokenStream(lexer)
    val parser = new SCAppParser(ts)
    parser.removeErrorListeners()
    parser.addErrorListener(ThrowingErrorListener.INSTANCE)
    parser.lProp()

  @throws[ParseCancellationException]
  def parseCapaString(text: String): CapabilitiesContext =
    val charstream = fromString(text)
    val lexer = new SCAppLexer(charstream)
    lexer.removeErrorListeners()
    lexer.addErrorListener(ThrowingErrorListener.INSTANCE)
    val ts = new CommonTokenStream(lexer)
    val parser = new SCAppParser(ts)
    parser.removeErrorListeners()
    parser.addErrorListener(ThrowingErrorListener.INSTANCE)
    parser.capabilities()
}
