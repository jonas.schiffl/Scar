package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.generate.{SolcVerifySolSpecGenerator, SolidityGenerator}
import edu.kit.kastel.formal.scar.model.SCApp

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


private val commentRegex = "\\s*///.*".r

protected def updateAnnotations(model: SCApp, implementation: Seq[String]): Seq[String] =
  val commentMap = getCommentMap(model)
  implementation.filterNot(commentRegex.matches(_)).flatMap { line =>
    val indent = line.takeWhile(_.isWhitespace)
    commentMap.getOrElse(Component(line).orNull, Seq.empty)
      .map(indent + _.stripLeading()) :+ line
  }


private def getCommentMap(model: SCApp): Map[Component, Seq[String]] =
  val result = new ListBuffer[(Component, Seq[String])]
  val comments = new ListBuffer[String]
  val gen = SolidityGenerator(model, Some(SolcVerifySolSpecGenerator))
  gen.getString.result.lines().forEach {
    case comment@commentRegex() => comments += comment
    case line => if comments.nonEmpty then
      Component(line).map(component => {
        result += ((component, comments.toSeq))
        comments.clear()
      })
  }
  result.toMap


private sealed trait Component:
  val name: String


private object Component:
  def apply(line: String): Option[Component] = line.replace("\t", "") match
    case s"$_ contract $name $_" => Some(Contract(name))
    case s"$_ constructor $_" => Some(Function("constructor"))
    case s"$_ function $name $_" => Some(Function(name))
    case s"$_ event $name $_" => Some(Event(name))
    case _ => None


private case class Contract(name: String) extends Component
private case class Function(name: String) extends Component
private case class Event(name: String) extends Component
