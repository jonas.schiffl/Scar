package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.model.SCApp

import java.nio.file.Path


def findErrors(model: SCApp, implementation: Seq[String], docker: Option[Path] = None): Set[SpecError] =
  val errors = parseMessages(produceMessages(implementation, docker))
  printErrors(errors.toSeq)
  if errors.isEmpty then return Set.empty
  val newModel = integrateErrors(model, errors)
  val newImplementation = updateAnnotations(newModel, implementation)
  errors ++ findErrors(newModel, newImplementation, docker)


def integrateErrors(model: SCApp, errors: Set[SpecError]): SCApp =
  val integrator = new SpecErrorIntegrator
  errors.foldLeft(model)((runningModel, error) => error.accept(integrator, runningModel))


private def printErrors(errors: Seq[SpecError]): Unit =
    println("found " + (errors.size match
      case 0 => "no (more) errors"
      case 1 => "error: " + errors.head.toString
      case _ => ("errors:" +: errors.map(_.toString) :+ "").mkString("\n")
    ))
