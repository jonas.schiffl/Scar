package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.model.QualifiedFun

import java.nio.file.{Files, Path}
import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters.SeqHasAsJava
import scala.sys.process.{Process, ProcessBuilder, ProcessLogger, stringSeqToProcess}


def produceMessages(implementation: Seq[String], docker: Option[Path] = None): Map[QualifiedFun, Set[String]] =
  val file = Files.createTempFile("temp", ".sol")
  Files.write(file, implementation.asJava)
  getMessages(file, docker)


protected def getMessages(path: Path, docker: Option[Path] = None): Map[QualifiedFun, Set[String]] =
  parseOutput(runSolcVerify(path, docker))


protected def parseOutput(output: Seq[String]): Map[QualifiedFun, Set[String]] =
  val result = new ListBuffer[(QualifiedFun, Set[String])]
  val errorMessages = new ListBuffer[String]
  output.reverse.foreach {
    case s"$contract::$function: ERROR" =>
      result += ((QualifiedFun(function, contract), errorMessages.toSet))
      errorMessages.clear()
    case s" - $path:$row:$column: $message" => errorMessages += message
    case _ => ()
  }
  result.toMap


protected def runSolcVerify(path: Path, docker: Option[Path] = None): Seq[String] =
  checkPath(path)
  val (exitCode, output) = runProcess(buildProcess(path, docker))
  checkExitCode(exitCode, output)
  output


private def checkPath(path: Path): Unit =
  if !Files.exists(path) || !path.getFileName.toString.endsWith(".sol") then
    throw new IllegalArgumentException("Provide a valid target file")


private def buildProcess(path: Path, docker: Option[Path] = None): ProcessBuilder =
  val internalPath = s"/contracts/${path.getFileName.toString}"
  Process(Seq(
    docker.getOrElse(getDocker).toAbsolutePath.toString, "run", "--rm",
    "--platform", "linux/amd64",
    "-v", s"${path.toAbsolutePath}:$internalPath",
    "scar-solc-verify", "--show-warnings", internalPath
  ))


private def getDocker: Path =
  val osName = System.getProperty("os.name").toLowerCase
  val command = if (osName.contains("win")) "where" else "which"
  val dockerPath = Seq(command, "docker").!!.trim
  if (dockerPath.isEmpty) throw new RuntimeException("Docker not found")
  Path.of(dockerPath)


private def runProcess(process: ProcessBuilder): (Int, Seq[String]) =
  val output = scala.collection.mutable.ListBuffer[String]()
  val logger = ProcessLogger(line => output += line)
  val exitCode = process ! logger
  (exitCode, output.toSeq)


private def checkExitCode(exitCode: Int, output: Seq[String]): Unit =
  // https://github.com/SRI-CSL/solidity/blob/
  // 8ea107cecd90ffff0d6a8f276b434b5ac9aa043b/solc/solc-verify.py#L15
  // successful verification -> 0
  // unsuccessful verification -> -6 = 250
  // partial verification -> -7 = 249
  if !Seq(0, 250, 249).contains(exitCode)
  then throw new RuntimeException(
    (s"Running solc-verify through Docker failed:" +: output).mkString("\n"))
