package edu.kit.kastel.formal.scar.verification
import edu.kit.kastel.formal.scar.generate.SolcVerifySolSpecGenerator
import edu.kit.kastel.formal.scar.model.{Actor, Expr, ModifyCapa, SCApp, VarLoc}


class SpecErrorIntegrator extends SpecErrorVisitor:
  override def visit(error: ModifyError, model: SCApp): SCApp =
    val loc = VarLoc(error.field)
    model.copy(capas = model.capas.copy(modCapas =
      model.capas.modCapas
        ++ model.capas.callCapas
        .filter(_.callee == error.function)
        .map(capa => ModifyCapa(capa.actor, loc))
        + ModifyCapa(Actor.Fun(error.function), loc))
    )

  override def visit(error: ConditionError, model: SCApp): SCApp =
    def removeInvariant(conditions: Set[Expr]): Set[Expr] =
      conditions - conditions.find(condition =>
        SolcVerifySolSpecGenerator.e2s(condition).filterNot(_.isWhitespace)
          == error.invariant.filterNot(_.isWhitespace)
      ).get

    // type of problem doesn't matter, we just remove the condition
    val contract = model.contracts.find(_.name == error.function.contractName).get
    val modelFun = contract.functions.find(_.name == error.function.funName).get
    val newModelFun = modelFun.copy(spec = error.location match
      case FunLocation.FunctionEntry => modelFun.spec.copy(
        pre = removeInvariant(modelFun.spec.pre))
      case FunLocation.FunctionExit => modelFun.spec.copy(
        post = removeInvariant(modelFun.spec.post))
    )
    val newContract = contract.copy(functions = contract.functions - modelFun + newModelFun)
    model.copy(contracts = model.contracts - contract + newContract)
