package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.model.{QualifiedFun, QualifiedStateVar}


def parseMessages(messages: Map[QualifiedFun, Set[String]]): Set[SpecError] =
  messages.flatMap((function, messages) => messages.map(parseMessage(function, _))).toSet


private def parseMessage(function: QualifiedFun, message: String): SpecError =
  message match
    case s"Function might modify '$field' illegally" =>
      ModifyError(function, parseVariable(field, function.contractName))
    case "Function might modify balances illegally" =>
      ModifyBalancesError(function)
    case "Assertion might not hold." =>
      AssertionError(function)
    case "Index may be out of bounds" =>
      IndexOutOfBoundsError(function)
    case "Conversion to enum might be out of range" =>
      EnumConversionOutOfRangeError(function)
    case "An overflow can occur before calling function" =>
      OverflowError(function, FunLocation.FunctionEntry)
    case "Function can terminate with overflow" =>
      OverflowError(function, FunLocation.FunctionExit)
    // invariants
    case s"Invariant '$invariant' might not hold when entering function." =>
      InvariantError(function, invariant, FunLocation.FunctionEntry, Problem.MightNotHold)
    case s"Variables in invariant '$invariant' might be out of range when entering function." =>
      InvariantError(function, invariant, FunLocation.FunctionEntry, Problem.VariableOutOfRange)
    case s"Overflow in computation of invariant '$invariant' when entering function." =>
      InvariantError(function, invariant, FunLocation.FunctionEntry, Problem.ComputationOverflow)
    case s"Invariant '$invariant' might not hold at end of function." =>
      InvariantError(function, invariant, FunLocation.FunctionExit, Problem.MightNotHold)
    case s"Variables in invariant '$invariant' might be out of range at end of function." =>
      InvariantError(function, invariant, FunLocation.FunctionExit, Problem.VariableOutOfRange)
    case s"Overflow in computation of invariant '$invariant' at end of function." =>
      InvariantError(function, invariant, FunLocation.FunctionExit, Problem.ComputationOverflow)
    case s"Invariant '$invariant' might not hold on loop entry" =>
      LoopInvariantError(function, invariant, LoopLocation.LoopEntry)
    case s"Invariant '$invariant' might not hold after first iteration" =>
      LoopInvariantError(function, invariant, LoopLocation.AfterFirstLoopIteration)
    case s"Invariant '$invariant' might not be maintained by the loop" =>
      LoopInvariantError(function, invariant, LoopLocation.LoopMaintain)
    case s"Invariant '$invariant' might not hold before external call." =>
      ExternalCallInvariantError(function, invariant, Problem.MightNotHold)
    case s"Variables for invariant '$invariant' might be out of range before external call." =>
      ExternalCallInvariantError(function, invariant, Problem.VariableOutOfRange)
    case s"State variable initializers might violate invariant '$invariant'." =>
      StateVariableInitializersInvariantError(function, invariant)
    // conditions
    case s"Precondition '$condition' might not hold when entering function." =>
      ConditionError(function, condition, FunLocation.FunctionEntry, Problem.MightNotHold)
    case s"Variables in precondition '$condition' might be out of range when entering function." =>
      ConditionError(function, condition, FunLocation.FunctionEntry, Problem.VariableOutOfRange)
    case s"Overflow in computation of precondition '$condition' when entering function." =>
      ConditionError(function, condition, FunLocation.FunctionEntry, Problem.ComputationOverflow)
    case s"Postcondition '$condition' might not hold at end of function." =>
      ConditionError(function, condition, FunLocation.FunctionExit, Problem.MightNotHold)
    case s"Variables in postcondition '$condition' might be out of range at end of function." =>
      ConditionError(function, condition, FunLocation.FunctionExit, Problem.VariableOutOfRange)
    case s"Overflow in computation of postcondition '$condition' at end of function." =>
      ConditionError(function, condition, FunLocation.FunctionExit, Problem.ComputationOverflow)
    case s"Event precondition '$condition' might not hold before emit of $event." =>
      EventConditionError(function, event, condition, FunLocation.FunctionEntry)
    case s"Event postcondition '$condition' might not hold before emit $event." =>
      EventConditionError(function, event, condition, FunLocation.FunctionExit)
    case s"Function called without triggering event " =>
      EventNotTriggeredError(function, FunLocation.FunctionEntry)
    case s"Function can end without triggering event" =>
      EventNotTriggeredError(function, FunLocation.FunctionExit)
    case s"Event triggered without changes to data" =>
      EventTriggeredWithoutChangesError(function)
    case other => throw new IllegalArgumentException(s"unknown error message '$other'")


private def parseVariable(field: String, contractName: String): QualifiedStateVar =
  field match
    case s"$contract::$fieldName" => QualifiedStateVar(fieldName, contract)
    case fieldName => QualifiedStateVar(fieldName, contractName)
