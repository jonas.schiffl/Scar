package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.model.*
import upickle.default.{ReadWriter, read, write}


sealed trait SpecError derives ReadWriter:
  def function: QualifiedFun
  
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp

  override def toString: String =
    val product = this.asInstanceOf[Product] // all errors are case classes = products
    val zipped = product.productElementNames.zip(product.productIterator)
    val fields = zipped.map { case (name, value) => s"$name = `$value`" }
    fields.mkString(s"${product.productPrefix}(", ", ", ")")


def writeErrorsJson(errors: Set[SpecError]): String = write(errors, indent = 2)

def readErrorsJson(json: String): Set[SpecError] = read(json)


protected case class ModifyError(function: QualifiedFun, field: QualifiedStateVar) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class ModifyBalancesError(function: QualifiedFun) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class AssertionError(function: QualifiedFun) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class IndexOutOfBoundsError(function: QualifiedFun) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class EnumConversionOutOfRangeError(function: QualifiedFun) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class OverflowError(function: QualifiedFun, location: FunLocation) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class InvariantError(
  function: QualifiedFun,
  invariant: String,
  location: FunLocation,
  problem: Problem
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class LoopInvariantError(
  function: QualifiedFun,
  invariant: String,
  location: LoopLocation
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class ExternalCallInvariantError(
  function: QualifiedFun,
  invariant: String,
  problem: Problem // no computation overflow here
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class StateVariableInitializersInvariantError(
  function: QualifiedFun,
  invariant: String
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class ConditionError(
  function: QualifiedFun,
  invariant: String,
  location: FunLocation,
  problem: Problem
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class EventConditionError(
  function: QualifiedFun,
  event: String,
  invariant: String,
  location: FunLocation
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class EventNotTriggeredError(
  function: QualifiedFun,
  location: FunLocation
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected case class EventTriggeredWithoutChangesError(
  function: QualifiedFun
) extends SpecError:
  def accept(visitor: SpecErrorVisitor, model: SCApp): SCApp = visitor.visit(this, model)

protected enum FunLocation derives ReadWriter:
  case FunctionEntry, FunctionExit

protected enum LoopLocation derives ReadWriter:
  case LoopEntry, AfterFirstLoopIteration, LoopMaintain

protected enum Problem derives ReadWriter:
  case MightNotHold, VariableOutOfRange, ComputationOverflow


trait SpecErrorVisitor:
  def visit(error: ModifyError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: ModifyBalancesError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: AssertionError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: IndexOutOfBoundsError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: EnumConversionOutOfRangeError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: OverflowError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: InvariantError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: LoopInvariantError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: ExternalCallInvariantError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: StateVariableInitializersInvariantError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: ConditionError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: EventConditionError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: EventNotTriggeredError, model: SCApp): SCApp = throw new NotImplementedError
  def visit(error: EventTriggeredWithoutChangesError, model: SCApp): SCApp = throw new NotImplementedError
