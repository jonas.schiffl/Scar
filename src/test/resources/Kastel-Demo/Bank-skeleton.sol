// SPDX-License-Identifier: UNLICENSED
pragma solidity >0.6 <0.7;

library UTIL {

  enum Role {customer, owner, any}
  function hasRole(address a, Role r) internal pure returns (bool) { }
}


contract Bank {
    modifier onlyany { 
    require(UTIL.hasRole(msg.sender, UTIL.Role.any));
   _;
  } 

  modifier onlycustomer { 
    require(UTIL.hasRole(msg.sender, UTIL.Role.customer));
   _;
  } 

  modifier onlyowner { 
    require(UTIL.hasRole(msg.sender, UTIL.Role.owner));
   _;
  } 

   mapping(address=>uint) balances;
   uint totBal;
   bool closed;
   constructor () { }

    /// @notice precondition !Bank.closed
    /// @notice postcondition Bank.balances[msg.sender] == __verifier_old_uint(Bank.balances[msg.sender]) + msg.value
    /// @notice postcondition Bank.totBal == __verifier_old_uint(Bank.totBal) + msg.value
    /// @notice modifies balances[msg.sender]
    /// @notice modifies totBal
    /// @notice modifies balances
    /// @notice modifies address(this).balance
  function deposit() onlyany public payable  { }

    /// @notice postcondition Bank.closed
    /// @notice modifies closed
    /// @notice modifies totBal
    /// @notice modifies balances
  function close() onlyowner public   { }

    /// @notice precondition Bank.balances[msg.sender] >= amt
    /// @notice precondition !Bank.closed
    /// @notice postcondition Bank.balances[msg.sender] == __verifier_old_uint(Bank.balances[msg.sender]) - amt
    /// @notice modifies balances[msg.sender]
    /// @notice modifies totBal
    /// @notice modifies address(this).balance
    /// @notice modifies address(msg.sender).balance
  function withdraw(uint amt) onlycustomer public   { }

}

