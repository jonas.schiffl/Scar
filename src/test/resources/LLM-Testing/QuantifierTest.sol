// SPDX-License-Identifier: UNLICENSED
pragma solidity >0.6 <0.7;

library UTIL {

  enum Role {ANY}
  function hasRole(address a, Role r) internal pure returns (bool) { }
}


contract c1 {
     uint[] arr;
   constructor () { }

  /// @notice precondition forall (uint x) arr[x] >= 0
  /// @notice postcondition forall (uint x) arr[x] >= 1
  function f()  public   { }

}

