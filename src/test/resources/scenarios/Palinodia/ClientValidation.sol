pragma solidity >=0.5.0 <0.6.0;

import "./BinaryHashStorage.sol";
import "./Software.sol";

/* This contract represents the process of validating that a downloaded software binary has the
correct hash stored on the blockchain (in the BHS contract), and that the associated BHS is (still)
endorsed by the original software contract. The specification for the validate function is rather
trivial, because it only reproduces the function's if-statements.
An invariant for the application would be
"validate returns true iff the correct hash exists and is endorsed by the original software contract"

Then, the correspondence between "correct hash exists and is endorsed by the original
software contract" and the more abstract notion of an unbroken "trust chain" of authorized function
calls needs to be established. One approach to this is temporal logic, which enables statements of
the form
"if bhs_hash == 0, then the hash was either never published or the revoke() function was called on it"
or
"if sw.isVerifiedSDP(platformID), then the associated function must have been called by someone with the correct credentials"
*/
contract ClientValidation {

    mapping (string => address) local_sw_contracts;

    function validate(uint256 localhash, string memory hashID, BinaryHashStorage bhs,
                    address local_sw, string memory software_id)
        public returns(bool)
    {
      ( , uint256 bhs_hash) = bhs.getBinaryStatement(hashID);
        if (bhs_hash == 0) {
            // hash was revoked, or never published
            return false;
        }
        if (localhash != bhs_hash) {
            // hash in BHS does not match local hash
            return false;
        }
        Software sw = bhs.software_contract();
        string memory platformID = bhs.platformID();
        if (!(sw.isVerifiedSDP(platformID) && sw.getBHSContract(platformID) == address(bhs))) {
            // software contract does not endorse this bhs contract
            return false;
        }
        if (local_sw == address(0)) {
          local_sw_contracts[software_id] = address(sw);
        } else if (local_sw != address(sw)) {
            // not the address of first use. do not trust
            return false;
        }
        return true;
    }
}
