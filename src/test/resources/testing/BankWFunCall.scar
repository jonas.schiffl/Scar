application: bank

// In this example, there are two withdraw functions. In the implementation,
// the customerWithdraw function should call the freeWithdraw function with \caller as the "to" parameter.
contract bank:
    state:
        var owner: account
        var total: int
        var balances: mapping(account=>uint)
    functions:
        fun deposit:
            post: \send(\caller, bank, \amt)
            post: balances[\caller] == \old(balances[\caller]) + \amt
        fun freeWithdraw:
            params: account to, int amount
            pre: balances[to] >= amount
            post: \send(bank, to, amount)
            post: balances[to] == \old(balances[to]) - amount
        fun customerWithdraw:
            params: int amount
            pre: balances[\caller] >= amount
            post: \send(bank, \caller, amount)
            post: balances[\caller] == \old(balances[\caller]) - amount
            calls: bank.freeWithdraw
