// SPDX-License-Identifier: UNLICENSED
pragma solidity >0.6 <0.7;

library UTIL {

  enum Role {ANY}
  function hasRole(address a, Role r) internal pure returns (bool) { }
}


contract Calculator {
     int number;
   constructor () { }

  /// @notice precondition summand > 0
  /// @notice postcondition number == __verifier_old_int(number) + summand
  function add(int summand)  public   { }

  /// @notice precondition subtrahend > 0
  /// @notice postcondition number == __verifier_old_int(number) + subtrahend
  function subtract(int subtrahend)  public   { }

}
