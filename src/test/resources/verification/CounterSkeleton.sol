// SPDX-License-Identifier: UNLICENSED
pragma solidity >0.6 <0.7;

library UTIL {

  enum Role {ANY}
  function hasRole(address a, Role r) internal pure returns (bool) { }
}


contract Counter {
     int count;
   int unrelated;
   constructor () { }

  /// @notice precondition count < 100
  /// @notice postcondition count == __verifier_old_int(count) + 1
  /// @notice modifies count
  function increment()  public   { }

}
