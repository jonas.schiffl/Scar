// SPDX-License-Identifier: UNLICENSED
pragma solidity >0.6 <0.7;

library UTIL {

    enum Role {Curator, Guard, Visitor}
    function hasRole(address a, Role r) internal pure returns (bool) { }
}


contract Museum {
    modifier onlyGuard {
        require(UTIL.hasRole(msg.sender, UTIL.Role.Guard));
        _;
    }

    modifier onlyCurator {
        require(UTIL.hasRole(msg.sender, UTIL.Role.Curator));
        _;
    }

    modifier onlyVisitor {
        require(UTIL.hasRole(msg.sender, UTIL.Role.Visitor));
        _;
    }

    address[] visitorLog;
    uint[] exhibits;
    string[] alarmLog;
    constructor () { }

    /// @notice modifies visitorLog
    function visit() onlyVisitor public   {
        visitorLog.push(msg.sender);
    }

    /// @notice modifies exhibits
    function addExhibit(uint uid) onlyCurator public   {
        exhibits.push(uid);
    }

    /// @notice modifies alarmLog
    function soundAlarm(string calldata reason) onlyGuard public   {
        alarmLog.push(reason);
    }

}
