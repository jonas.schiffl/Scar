package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.generate.ScarGenerator
import edu.kit.kastel.formal.scar.translation.Text2scapp
import org.scalatest.Assertions.succeed
import org.scalatest.flatspec.AnyFlatSpec

import scala.util
import scala.util.{Failure, Success, Try}

class BankCapaTest extends AnyFlatSpec:

  val bankFile: String = "src/test/resources/Kastel-Demo/Bank-correct.scar"
  val bankNegFile: String = "src/test/resources/Kastel-Demo/Bank-incorrect.scar"

  "The call test file" should "not yield any errors" in {
    val scapp = Text2scapp.translateFromFile(bankFile).get
    val res = TxAnalysis(scapp).analyse ++ CallAnalysis(scapp).analyse ++ ModifiesAnalysis(scapp).analyse
    if res.isEmpty then
      succeed
    else
      res.foreach(r => println(r.errorMsg))
      fail(res.map(r => r.errorMsg).mkString("\n"))
  }

  "The call test file" should "yield errors" in {
    val scapp = Text2scapp.translateFromFile(bankNegFile).get
    val res = TxAnalysis(scapp).analyse ++ CallAnalysis(scapp).analyse ++ ModifiesAnalysis(scapp).analyse
    if res.isEmpty then
      fail("No errors found")
    else
      res.foreach(r => println(r.errorMsg))
      succeed
  }