package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.smt.Scapp2z3Translator
import edu.kit.kastel.formal.scar.translation.Text2scapp
import org.scalatest.Assertions.succeed
import org.scalatest.flatspec.AnyFlatSpec
import z3.scala.*

import scala.util
import scala.util.{Failure, Success, Try}

class SMTTest extends AnyFlatSpec:

  val simpleTest = "src/test/resources/testing/SimpleApp.scar"

  "The test from github" should "run fine" in {
    val z3 = new Z3Context("MODEL" -> true)
    val intSort = z3.mkIntSort()
    val boolSort = z3.mkBoolSort()
    val i = z3.mkFreshConst("i", intSort)
    val b = z3.mkFreshConst("b", boolSort)
    val init = z3.mkAnd(z3.mkEq(i, z3.mkInt(0, intSort)), z3.mkEq(b, z3.mkFalse()))
    val pre = z3.mkLE(i, z3.mkInt(42, intSort))

    val solver = z3.mkSolver()
    solver.assertCnstr(z3.mkNot(z3.mkImplies(init, pre)))
    val result = solver.check()
    result match
      case Some(value) => if !value then succeed else fail("wtf")
      case None => fail("didn't work")
  }

  // TODO fix this test
//  "The solver" should "prove that the initial condition implies a function's precondition" in {
//    Text2scapp.translateFromFile(simpleTest) match
//      case Failure(exception) => fail(exception.toString)
//      case Success(scapp) =>
//        val z3 = new Z3Context()
//        val c1 = scapp.contracts.find(c => c.name == "c1").get
//        val f = c1.functions.find(f => f.name == "f").get
//        val trans = new Scapp2z3Translator(z3)
//        val fpre = trans.expr2SMT(f.spec.pre.head)
//
//        val init = z3.mkAnd(c1.initCond(1).map(i => trans.expr2SMT(i)).toIndexedSeq*)
//
//        if trans.errors.nonEmpty then
//          print(trans.errors.mkString("\n"))
//          fail("")
//
//        val imp = z3.mkImplies(init, fpre)
//        val PO = z3.mkNot(imp)
//        val solver = z3.mkSolver()
//        solver.assertCnstr(PO)
//        solver.check() match
//          case Some(value) =>
//            if value then fail("Should be unsat!")
//            else
//              succeed
//          case None => fail("Solver failed")
//
//  }

  "A simple unsat formula" should "be recognized as unsat by the solver" in {
    val z3ctx = new Z3Context()
    val z3Solver = z3ctx.mkSolver()

    val iSort = z3ctx.mkIntSort()
    val i1 = z3ctx.mkConst("i1", iSort)
    val c1 = z3ctx.mkInt(0, iSort)
    val c2 = z3ctx.mkInt(3, iSort)

    val cs1 = z3ctx.mkLE(i1, c1)
    val cs2 = z3ctx.mkGE(i1, c2)

    z3Solver.assertCnstr(z3ctx.mkAnd(cs1, cs2))

    val res = z3Solver.check()

    res match
      case Some(value) =>
        if value then
          println(value)
          fail("z3 says sat but shouldn't")
        else
          succeed
      case None => fail("Call to solver failed")
  }

  "A simple sat formula" should "be recognized as sat by the solver" in {
    val z3ctx = new Z3Context()
    val z3Solver = z3ctx.mkSolver()

    val iSort = z3ctx.mkIntSort()
    val i1 = z3ctx.mkConst("i1", iSort)
    val c1 = z3ctx.mkInt(0, iSort)
    val c2 = z3ctx.mkInt(1, iSort)

    val cs1 = z3ctx.mkLE(i1, c2)
    val cs2 = z3ctx.mkGT(i1, c1)

    z3Solver.assertCnstr(z3ctx.mkAnd(cs1, cs2))

    val res = z3Solver.check()

    res match
      case Some(value) =>
        if value then
          val model = z3Solver.getModel()
          val x = model.evalAs[Int](i1)
          if x.get == 1 then succeed else fail(s"Model was expected to be 1 but was $x")
        else
          fail("z3 says unsat but shouldn't")
      case None => fail("Call to solver failed")
  }


