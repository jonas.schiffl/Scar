package edu.kit.kastel.formal.scar.analysis

import edu.kit.kastel.formal.scar.generate.ScarGenerator
import edu.kit.kastel.formal.scar.translation.Text2scapp
import org.scalatest.Assertions.succeed
import org.scalatest.flatspec.AnyFlatSpec

import scala.util
import scala.util.{Failure, Success, Try}

class TxAnalysisTest extends AnyFlatSpec:

  val testFile: String = "src/test/resources/testing/TxAnalysisTest.scar"
  val negTestFile: String = "src/test/resources/testing/TxAnalysisNegTest.scar"

  "The call test file" should "not yield any errors" in {
    val scapp = Text2scapp.translateFromFile(testFile).get
    val res = TxAnalysis(scapp).analyse
    if res.isEmpty then
      succeed
    else
      res.foreach(r => println(r.errorMsg))
      fail(res.map(r => r.errorMsg).mkString("\n"))
  }

  "The negative test" should "yield an analysis error" in {
    val scapp = Text2scapp.translateFromFile(negTestFile).get
    val res = TxAnalysis(scapp).analyse
    if res.isEmpty then
      ScarGenerator(scapp).generate()
      fail("There should be an error here!")
    else
      res.foreach(r => println(r.errorMsg))
      succeed
  }