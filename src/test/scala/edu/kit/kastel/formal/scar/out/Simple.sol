// SPDX-License-Identifier: UNLICENSED
pragma solidity >0.6;

library UTIL {
  enum Coin {HEAD, TAIL}
  struct Thing {int x; UTIL.Coin c;}

  enum Role {ANY}
  function hasRole(address a, Role r) internal pure returns (bool) { }
}


contract SimpleTest {
     mapping(address=>uint) m;
   UTIL.Thing t;
   int y;
  /// @notice postcondition t.x == y
  /// @notice postcondition t.c == UTIL.Coin.TAIL
   constructor () { }

  /// @notice precondition t.c == UTIL.Coin.HEAD
  /// @notice precondition i <= 15
  /// @notice precondition i <= y && t.x >= y
  /// @notice postcondition t.c != UTIL.Coin.TAIL
  /// @notice postcondition y <= -2
  function f(UTIL.Coin c, int i, bool a)  public   { }

  /// @notice precondition b == true
  /// @notice postcondition y == 0
  function g(bool b)  public   { }

}

