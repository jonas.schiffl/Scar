package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.generate.ScarGenerator
import edu.kit.kastel.formal.scar.model.SCApp
import org.scalatest.Assertions.succeed
import org.scalatest.flatspec.AnyFlatSpec

import java.io.File
import scala.util
import scala.util.{Failure, Success, Try}


class DefaultGeneratorTest extends AnyFlatSpec:

  "The simple bank example" should "be parsed, translated back and printed " in {
    val scar = Text2scapp.translateFromFile("src/test/resources/testing/BankWSpec.scar")
    scar match
      case Failure(exception) =>
        fail(s"Failed to parse the simple bank example: $exception")
      case Success(value) =>
        val generated = ScarGenerator(value).getString
        println(generated)
        succeed
  }
