package edu.kit.kastel.formal.scar.translation

import edu.kit.kastel.formal.scar.model.SCApp
import org.scalatest.Assertions.succeed
import org.scalatest.flatspec.AnyFlatSpec

import java.io.File
import scala.util
import scala.util.{Failure, Success, Try}


class ParseTest extends AnyFlatSpec:

  "All .scar files in the resource folder" should "be successfully parsed" in {
    var failures = Set.empty[File]
    getFiles(new File("src/test/resources"), "scar").foreach(f =>
      val res = Text2scapp.translateFromFile(f.getAbsolutePath)
      res match
        case Failure(exception) =>
          println(s"Failed to parse ${f.getPath}")
          println(exception)
          failures += f
        case Success(value) => ()
    )
    if failures.nonEmpty then
      println("Failed files:")
      failures.foreach(f => println(f.getName))
      fail()
    else
      succeed
  }

  "All .neg files in the resource folder" should "not be successfully parsed" in {
    var failures = Set.empty[File]
    getFiles(new File("src/test/resources"), "neg").foreach(f =>
      val res = Text2scapp.translateFromFile(f.getAbsolutePath)
      res match
        case Failure(_) => ()
        case Success(_) =>
          println(s"Should have failed to parse ${f.getPath}")
          failures += f
    )
    if failures.nonEmpty then
      println("Failed files:")
      failures.foreach(f => println(f.getName))
      fail()
    else
      succeed
  }

  def getFiles(dir: File, fileEx: String): Set[File] =
    var res = dir.listFiles()
    res ++= dir.listFiles().filter(_.isDirectory).flatMap(getFiles(_, fileEx))
    res = res.filter(_.getName.endsWith(s".$fileEx"))
    res.toSet