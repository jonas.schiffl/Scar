package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.generate.{SolcVerifySolSpecGenerator, SolidityGenerator}
import edu.kit.kastel.formal.scar.main.{Main, writeErrors}
import edu.kit.kastel.formal.scar.model.{QualifiedFun, QualifiedStateVar}
import edu.kit.kastel.formal.scar.translation.Text2scapp.generateSCApp
import edu.kit.kastel.formal.scar.verification.*
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Files


val incorrectMuseumErrors: Set[SpecError] = Set(
  ModifyError(QualifiedFun("visit", "Museum"),
    QualifiedStateVar("alarmLog", "Museum")),
  ModifyError(QualifiedFun("addExhibit", "Museum"),
    QualifiedStateVar("alarmLog", "Museum"))
)

val incorrectCalculatorErrors: Set[SpecError] = Set(
  ConditionError(
    QualifiedFun("add", "Calculator"), "number == __verifier_old_int(number) + summand",
    FunLocation.FunctionExit, Problem.MightNotHold),
  ConditionError(
    QualifiedFun("subtract", "Calculator"),
    "number == __verifier_old_int(number) - subtrahend",
    FunLocation.FunctionExit, Problem.MightNotHold)
)

val incorrectCounterErrors: Set[SpecError] = Set(
  ConditionError(
    QualifiedFun("increment", "Counter"),
    "count == __verifier_old_int(count) + 1",
    FunLocation.FunctionExit, Problem.MightNotHold),
  ModifyError(QualifiedFun("increment", "Counter"),
    QualifiedStateVar("unrelated", "Counter"))
)

private val errorFile = Files.createTempFile("errors", ".json")


trait AnalyzerTest extends AnyFlatSpec with Matchers:
  def checkErrors(scarFile: String, solFile: String, expectedErrors: Set[SpecError] = Set()): Unit

  "Museum skeleton" should "not produce errors" in:
    checkErrors("Museum.scar", "MuseumSkeleton.sol")

  "Correct museum implementation" should "not produce errors" in:
    checkErrors("Museum.scar", "MuseumCorrect.sol")

  "Incorrect museum implementation" should "produce errors" in:
    checkErrors("Museum.scar", "MuseumIncorrect.sol", incorrectMuseumErrors)

  "Incorrect calculator implementation" should "produce correct errors" in:
    checkErrors("Calculator.scar", "CalculatorIncorrect.sol", incorrectCalculatorErrors)

  "Incorrect counter implementation" should "produce correct errors" in:
    checkErrors("Counter.scar", "CounterIncorrect.sol", incorrectCounterErrors)


class SimpleAnalyzerTest extends AnalyzerTest:
  override def checkErrors(
      scarFile: String, solFile: String, expectedErrors: Set[SpecError] = Set()): Unit =
    if verFile(scarFile).isEmpty then
      cancel(s"File $scarFile not found")
    if verFile(solFile).isEmpty then
      cancel(s"File $solFile not found")
    val model = generateSCApp(verFile(scarFile).get)
    val implementation = readLines(verFile(solFile).get)
    findErrors(model, implementation) shouldBe expectedErrors

  "SCAR file" should "have its skeleton printed" ignore:
    if verFile("Museum.scar").isEmpty then
      cancel("File Museum.scar not found")
    val museumModel = generateSCApp(verFile("Scar.scar").get)
    val gen = SolidityGenerator(museumModel, Some(SolcVerifySolSpecGenerator))
    println(gen.getString)


class FilesAnalyzerTest extends AnalyzerTest:
  override def checkErrors(
      scarFile: String, solFile: String, expectedErrors: Set[SpecError] = Set()): Unit =
    if verFile(scarFile).isEmpty then
      cancel(s"File $scarFile not found")
    if verFile(solFile).isEmpty then
      cancel(s"File $solFile not found")
    writeErrors(verFile(scarFile).get, verFile(solFile).get, errorFile)
    readErrorsJson(Files.readString(errorFile)) shouldBe expectedErrors


class MainAnalyzerTest extends AnalyzerTest:
  override def checkErrors(
      scarFile: String, solFile: String, expectedErrors: Set[SpecError] = Set()): Unit =
    Main.main(Array("analyze",
      verFile(scarFile).toString, verFile(solFile).toString, errorFile.toString))
    readErrorsJson(Files.readString(errorFile)) shouldBe expectedErrors
