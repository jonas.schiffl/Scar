package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.model.{QualifiedFun, QualifiedStateVar}
import edu.kit.kastel.formal.scar.verification.*
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Files


class JsonErrorsTest extends AnyFlatSpec with Matchers:
  "No errors" should "be written and read correctly" in:
    val errors: Set[SpecError] = Set()
    readErrorsJson(writeErrorsJson(errors)) shouldBe errors

  "Example errors" should "be written and read correctly" in:
    val errors: Set[SpecError] =
      incorrectMuseumErrors ++ incorrectCalculatorErrors ++ incorrectCounterErrors
    readErrorsJson(writeErrorsJson(errors)) shouldBe errors


class ParserErrorsTest extends AnyFlatSpec with Matchers:
  "Modify error message with unqualified field" should "be parsed correctly" in:
    parseMessages(
      Map(QualifiedFun("visit", "Museum") ->
        Set("Function might modify 'alarmLog' illegally"))
    ) shouldEqual
      Set(ModifyError(QualifiedFun("visit", "Museum"),
        QualifiedStateVar("alarmLog", "Museum")))

  "Modify error message with qualified field" should "be parsed correctly" in:
    parseMessages(
      Map(QualifiedFun("visit", "Museum") ->
        Set("Function might modify 'Alarm::log' illegally"))
    ) shouldEqual
      Set(ModifyError(QualifiedFun("visit", "Museum"),
        QualifiedStateVar("log", "Alarm")))

  "Precondition error message" should "be parsed correctly" in:
    parseMessages(
      Map(QualifiedFun("addExhibit", "Museum") ->
        Set("Precondition 'count > 0' might not hold when entering function."))
    ) shouldEqual
      Set(ConditionError(QualifiedFun("addExhibit", "Museum"),
        "count > 0", FunLocation.FunctionEntry, Problem.MightNotHold))

  "Postcondition error message" should "be parsed correctly" in:
    parseMessages(
      Map(QualifiedFun("addExhibit", "Museum") ->
        Set("Postcondition 'count == __verifier_old_int(count) + 1' might not hold at end of function."))
    ) shouldEqual
      Set(ConditionError(QualifiedFun("addExhibit", "Museum"),
        "count == __verifier_old_int(count) + 1",
        FunLocation.FunctionExit, Problem.MightNotHold))

  "All error messages in solc-verify repo" should "be parsed without exception" in:
    Files.walk(scvFile("").get).filter(_.toString.endsWith(".sol.gold")).forEach { file =>
      println(file)
      parseMessages(parseOutput(readLines(file)))
    }
