package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.main.{Main, writeIntegrated}
import edu.kit.kastel.formal.scar.model.*
import edu.kit.kastel.formal.scar.translation.Text2scapp.generateSCApp
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Files


private val errorFile = Files.createTempFile("errors", ".json")
private val integratedScar = Files.createTempFile("integrated", ".scar")

private val museumModel = generateSCApp(verFile("Museum.scar").get)
private val calculatorModel = generateSCApp(verFile("Calculator.scar").get)
private val counterModel = generateSCApp(verFile("Counter.scar").get)


trait IntegratorTest extends AnyFlatSpec with Matchers:
  def checkIntegrated(scarFile: String, errors: Set[SpecError], expectedModel: SCApp): Unit

  "Incorrect museum implementation errors" should "be integrated correctly" in:
    checkIntegrated("Museum.scar", incorrectMuseumErrors, getExpectedMuseumModel(museumModel))

  // fails due to set order... what the hell https://stackoverflow.com/q/29008500
  "Incorrect calculator implementation errors" should "be integrated correctly" ignore:
    checkIntegrated("Calculator.scar", incorrectCalculatorErrors, removePostconditions(calculatorModel))

  "Incorrect counter implementation errors" should "be integrated correctly" in:
    checkIntegrated("Counter.scar", incorrectCounterErrors, getExpectedCounterModel(counterModel))


class SimpleIntegratorTest extends IntegratorTest:
  override def checkIntegrated(scarFile: String, errors: Set[SpecError], expectedModel: SCApp): Unit =
    if verFile(scarFile).isEmpty then
      cancel(s"File $scarFile not found")
    val model = generateSCApp(verFile(scarFile).get)
    integrateErrors(model, errors) shouldEqual expectedModel


class FilesIntegratorTest extends IntegratorTest:
  override def checkIntegrated(scarFile: String, errors: Set[SpecError], expectedModel: SCApp): Unit =
    Files.write(errorFile, writeErrorsJson(errors).getBytes)
    if verFile(scarFile).isEmpty then
      cancel(s"File $scarFile not found")
    writeIntegrated(verFile(scarFile).get, errorFile, integratedScar)
    generateSCApp(integratedScar) shouldEqual expectedModel


class MainIntegratorTest extends IntegratorTest:
  override def checkIntegrated(scarFile: String, errors: Set[SpecError], expectedModel: SCApp): Unit =
    Files.write(errorFile, writeErrorsJson(errors).getBytes)
    Main.main(Array("integrate", verFile(scarFile).toString, errorFile.toString, integratedScar.toString))
    generateSCApp(integratedScar) shouldEqual expectedModel


private def removePostconditions(original: SCApp): SCApp =
  original.copy(contracts = Set(
    original.contracts.head.copy(
      functions = original.contracts.head.functions.map(fun =>
        fun.copy(spec = fun.spec.copy(post = Set()))))))


private def addModCapas(original: SCApp, newCapas: Set[ModifyCapa]): SCApp =
  original.copy(capas = original.capas.copy(
      modCapas = original.capas.modCapas ++ newCapas))


private def getExpectedMuseumModel(original: SCApp): SCApp =
  addModCapas(original, Set(
    ModifyCapa(Actor.Ro(Role("Visitor")),
      VarLoc(QualifiedStateVar("alarmLog", "Museum"))),
    ModifyCapa(Actor.Fun(QualifiedFun("visit", "Museum")),
      VarLoc(QualifiedStateVar("alarmLog", "Museum"))),
    ModifyCapa(Actor.Ro(Role("Curator")),
      VarLoc(QualifiedStateVar("alarmLog", "Museum"))),
    ModifyCapa(Actor.Fun(QualifiedFun("addExhibit", "Museum")),
      VarLoc(QualifiedStateVar("alarmLog", "Museum")))
  ))


private def getExpectedCounterModel(original: SCApp): SCApp =
  addModCapas(removePostconditions(original), Set(
    ModifyCapa(Actor.Ro(Role("Counting")),
      VarLoc(QualifiedStateVar("unrelated", "Counter"))),
    ModifyCapa(Actor.Fun(QualifiedFun("increment", "Counter")),
      VarLoc(QualifiedStateVar("unrelated", "Counter")))
  ))
