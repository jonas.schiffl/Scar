package edu.kit.kastel.formal.scar.verification

import java.nio.file.{Files, Path}
import scala.jdk.CollectionConverters.ListHasAsScala

def file(path: String): Option[Path] =
  Option(getClass.getResource("/" + path)).map(url => Path.of(url.getPath))

def scvFile(path: String): Option[Path] = file(s"solc-verify/$path")

def verFile(path: String): Option[Path] = file(s"verification/$path")

def readLines(path: Path): Seq[String] =
  Files.readAllLines(path).asScala.map(
    line => line.replace("test/solc-verify/specs/", "/contracts/")
  ).toSeq
