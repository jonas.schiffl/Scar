package edu.kit.kastel.formal.scar.verification

import edu.kit.kastel.formal.scar.model.QualifiedFun
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class RawWrapperTest extends AnyFlatSpec with Matchers:
  "Annotations example" should "produce correct raw output" in:
    runSolcVerify(scvFile("examples/annotations.sol").get) shouldEqual
      readLines(scvFile("examples/annotations.sol.gold").get)

  "ModifiesIdx example" should "produce correct raw output" in:
    runSolcVerify(scvFile("specs/ModifiesIdx.sol").get) shouldEqual
      readLines(scvFile("specs/ModifiesIdx.sol.gold").get)


class MessagesWrapperTest extends AnyFlatSpec with Matchers:
  "Annotations example" should "produce correct error messages" in:
    parseOutput("""
      solc-verify warning: Balance modifications due to gas consumption or miner rewards are not modeled
      C::add: OK
      C::[implicit_constructor]: OK
      C::[receive_ether_selfdestruct]: OK
      No errors found.
      """.stripIndent().linesIterator.toSeq
    ) shouldEqual Map()

  "ModifiesIdx example" should "produce correct error messages" in:
    parseOutput("""
      solc-verify warning: Balance modifications due to gas consumption or miner rewards are not modeled
      ModifiesIdx::f: OK
      ModifiesIdx::g: OK
      ModifiesIdx::g_incorrect: ERROR
       - /contracts/ModifiesIdx.sol:35:5: Function might modify 'xs' illegally
      ModifiesIdx::[implicit_constructor]: OK
      Errors were found by the verifier.
      """.stripIndent().linesIterator.toSeq
    ) shouldEqual Map(
      QualifiedFun("g_incorrect", "ModifiesIdx") ->
        Set("Function might modify 'xs' illegally")
    )
